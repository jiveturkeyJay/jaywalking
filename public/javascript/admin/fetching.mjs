import * as utils from "../helpers/utils.mjs";
import {
	radioType,
	buttonGetByType,
	buttonGetBySubType,
	fieldSetArticleParam,
	fieldSetType
} from "../helpers/selectors.mjs";

const { byId, createFrag, createElement, removeClassesToEle, qs, addClassesToEle } = utils;

const urls = {
	testData: {
		baseUrl: "/admin/testdata/",
		options: {
			method: "GET"
		}
	},
	articleData: {
		baseUrl: "/admin/articledata/",
		options: {
			method: "GET"
		}
	}
};

const fetcher = async (url, options) => {
	let data;
	try {
		const res = await fetch(url, options);
		if (res.ok) {
			data = await res.json();
		}
	} catch (err) {
		console.log(err);
	}

	return data;
};

const createOptions = (data, valueParam, textParam) => {
	const frag = createFrag();

	data.forEach((item) => {
		const opt = createElement("option");
		opt.value = item[valueParam];
		opt.textContent = item[textParam];
		frag.appendChild(opt);
	});

	return frag;
};

let getSubTypesButton;
const getArticlesByParam = async (evt) => {
	let options = urls.articleData;
	const select = qs("select", evt.target.parentNode);
	const { selectedIndex } = select;
	const value = select.options[selectedIndex].value;
	options.url = `${options.baseUrl}${value}`;

	try {
		const data = await fetcher(options.url, options.options);
		const articleOuter = byId(fieldSetArticleParam);
		const frag = createOptions(data, "articleParam", "article_title");
		articleOuter.querySelector("select").appendChild(frag);
		removeClassesToEle(articleOuter, ["is-hidden"]);
	} catch (err) {
		if (err) {
			console.log(err);
		}
	}
};

const getTypes = async (evt) => {
	evt.preventDefault();

	let options = urls.testData;
	const item = qs(radioType, evt.target.parentNode);
	const { value } = item;
	options.url = `${options.baseUrl}${value}`;

	try {
		const data = await fetcher(options.url, options.options);
		if (data && data.length) {

			const subTypeParamOuter = byId(fieldSetType);
			const frag = createOptions(data, "sub_type_param", "title");

			subTypeParamOuter.querySelector("select").appendChild(frag);
			removeClassesToEle(subTypeParamOuter, ["is-hidden"]);

			const selected = document.querySelector('[name="updateOrAddNew"]:checked').value;
			// need to set up some
			// events only for the update as we don't
			// need to submit this if a new article is being created
			if (!getSubTypesButton && selected === 'update') {
				getSubTypesButton = qs(buttonGetBySubType);
				getSubTypesButton.addEventListener("click", getArticlesByParam);
			} else {
				const button = document.querySelector('[data-button="get-by-sub-type"]');
				addClassesToEle(button, ["is-hidden"]);
			}
		}
	} catch (err) {
		console.log("err");
	}
};

const getByTypeButton = qs(buttonGetByType);

getByTypeButton.addEventListener("click", getTypes);
