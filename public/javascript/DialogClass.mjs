/**
 * opens and adds content to an image
 * https://css-tricks.com/dialog-components-roll-your-own/
 *
 */
class DialogClass {
	/**
	 * we supply an outer and within that we use the openDataStr to open a modal dialog
	 * we use the closeDataStr to close it.
	 * Requires some link up between the JS and HTML
	 * we can really only ever have one dialog open
	 *
	 * todo check whether the unopened dialog is read by screen readers
	 *
	 * @param {element} outer - an html element
	 * @param {string} openDataStr -  an html dataset property
	 *
	 * we could add in modalId/closeDataStr as props but not really worth it now
	 */
	/**
	 *
	 * @param outer
	 * @param openDataStr
	 */
	constructor(outer, openDataStr) {
		this.outer = outer;
		this.openDataStr = openDataStr;
		this.closeDataStr = "closeModal";
		this.modalId = "modal-dialog";
		this.dialog = document.getElementById(this.modalId);

		//https://www.freecodecamp.org/news/this-is-why-we-need-to-bind-event-handlers-in-class-components-in-react-f7ea1a6f93eb/
		if (this.outer && this.openDataStr && this.dialog) {
			this.closeModal = this.closeModal.bind(this);
			this.openModal = this.openModal.bind(this);
			this.keyListener = this.keyListener.bind(this);

			this.init();
		}
	}

	/**
	 *
	 * @returns {HTMLImageElement}
	 */
	getDialogImage() {
		return this.dialog.querySelector("img");
	}

	/**
	 * keylistener not need if using a native dialog
	 * as it closes when using escqpe/space if opened with dialog.openModal
	 * and also it autofocuses and tabs around
	 *
	 * will need this if we revert to not using a dialog element
	 * @param evt
	 */
	keyListener(evt) {
		const { code } = evt;
		// allows users to close using escape key
		if (code && code === "Escape") {
			this.closeModalAction();
		}

		if (code === "Tab") {
			// else get all the focusable elements within the content
			const focusableModalElements = this.dialog.querySelectorAll(
				"a[href], button, textarea, input[type=\"text\"], input[type=\"radio\"], input[type=\"checkbox\"], select"
			);

			const firstElement = focusableModalElements[0];
			const lastElement =
				focusableModalElements[focusableModalElements.length - 1];
			// if we only have one focusable element don't do anything
			if (focusableModalElements.length === 1) {
				firstElement.focus();
				evt.preventDefault();
				return;
			}

			if (!evt.shiftKey && document.activeElement !== firstElement) {
				firstElement.focus();
				evt.preventDefault();
				return;
			}

			if (evt.shiftKey && document.activeElement !== lastElement) {
				lastElement.focus();
				evt.preventDefault();
			}
		}
	}

	closeModalAction() {
		//put into function
		const image = this.getDialogImage();
		image.src = "";
		image.alt = "";
		// hack for tests - close does not work
		// https://www.freecodecamp.org/news/javascript-keycode-list-keypress-event-key-codes/
		document.body.removeEventListener("keydown", this.keyListener);
		document.body.removeEventListener("click", this.closeModal);
		this.dialog.classList.add("is-hidden");
	}

	/**
	 *
	 * @param evt
	 * @returns {boolean}
	 */
	closeModal(evt) {
		evt.preventDefault();
		const { target } = evt;
		const { dataset } = target;
		if (dataset.closeModal) {
			this.closeModalAction();

			return true;
		}
		return false;
	}

	/**
	 * opens the modal and also adds in the correct img
	 * we need to test that the event target has the correct
	 * data-attr
	 * this is because the event is delegated on the outer
	 *
	 * @param evt
	 * @returns {boolean}
	 */
	openModal(evt) {
		const { target } = evt;
		const { dataset } = target;

		if (dataset.openModal) {
			evt.preventDefault();
			evt.stopPropagation();
			const image = this.getDialogImage();
			this.dialog.classList.remove("is-hidden");

			image.src = `/images/${dataset.imageName}_large.jpg`;
			image.alt = dataset.imageAlt;
			// needs test
			document.body.addEventListener("keydown", this.keyListener, false);
			document.body.addEventListener("click", this.closeModal, false);
			return true;
		}
		return false;
	}

	setUpEvents() {
		this.outer.addEventListener("click", this.openModal, false);
	}

	init() {
		this.setUpEvents();
	}
}

export {
	DialogClass
};
