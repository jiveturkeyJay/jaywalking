import { DialogClass } from "./DialogClass.mjs";

document.addEventListener("DOMContentLoaded", () => {
	const articlePage = document.getElementById("article-cont");
	const imageButtons = document.querySelectorAll(".btn__dialog");
	// we will use a event delegation on the article
	if (articlePage && imageButtons.length) {
		const dialog = new DialogClass(articlePage, "openModal");
	}

});
