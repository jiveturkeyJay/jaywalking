import {CreateTextArea} from './createTextArea.mjs';
import {CreatePicture} from './createPicture.mjs';

export {
    CreatePicture,
    CreateTextArea
}
