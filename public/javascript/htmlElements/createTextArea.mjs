import {addClassesToEle, createElement, createEleWithAttr, createFrag} from '../helpers/utils.mjs';

const divClassList = ['input-wrap-text', 'm-t-vmed'];
const labelClassList = ['label', 'label-small'];
const textareaClassList = ['input', 'textarea'];

// we can get a ref using a data attr
/**
 * creates a text area to use in the intro para or anywhere
 *
 * with the follow dom elements
 * we need to supply an xx to the object
 *
 <div class="input-wrap-text m-t-vmed" data-text-ref="textarea-xx">
 <label class="label label-small" for="textarea-xx">Text 0</label>
 <textarea id="textarea-xx" name="textarea-xx" class="input textarea"></textarea>
 </div>
 *
 * need to be able to update the xx
 *
 * to update/delete them then we'd need to loop through an object
 */
class CreateTextArea {
    constructor(suffix, abbr) {
        if (suffix) {
            // need the _ otherwise has a fit
            this._suffix = suffix;
            this.domElements = null;
        }
    }

    get suffix() {
        return this._suffix;
    }

    set suffix(suffix) {
        this._suffix = suffix;
    }

    /**
     *  *
     <div class="input-wrap-text m-t-vmed"  data-text-ref="textarea-xx">
     <label class="label label-small" for="textarea-xx">Text 0</label>
     <textarea id="textarea-xx" name="textarea-xx" class="input textarea"></textarea>
     </div>
     *
     * create the DOM elements but don't insert unless specified
     */
    createDomElements() {
        const suffix = `textarea-${this._suffix}`;
        const frag = createFrag();
        const div = createElement('div');
        div.dataset.textRef = suffix;
        addClassesToEle(div, divClassList);

        // create label
        const label = createEleWithAttr('label', {
            for: suffix
        });

        label.textContent = `text ${this._suffix}`;
        addClassesToEle(label, labelClassList);
        div.appendChild(label);

        const textarea = createEleWithAttr('textarea', {
            id: suffix,
            name: suffix
        });

        addClassesToEle(textarea, textareaClassList);
        div.appendChild(textarea);

        frag.appendChild(div);
        this.domElements = frag;
    }
}

export {
    CreateTextArea,
    divClassList,
    labelClassList,
    textareaClassList
};
