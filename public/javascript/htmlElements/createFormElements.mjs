import {addClassesToEle, createEleWithAttr, createFrag} from '../helpers/utils.mjs';

const inputClassNames = ['input'];
const labelClassNames = ['label', 'label-small'];

// todo refactor these so use an options object
// will make it more re-useable
// defaulting to classnames is a pain
const createInput = (suffix, classNames = inputClassNames) => {
    const input = createEleWithAttr('input', {
        id: suffix,
        name: suffix,
        type: 'text'
    });
    addClassesToEle(input, classNames);
    return input;
};

const createLabel = (suffix, labelText, classNames = labelClassNames) => {
    const label = createEleWithAttr('label', {
        for: suffix
    });
    addClassesToEle(label, classNames);
    label.textContent = labelText;
    return label;
};

/**
 * creates an input and label
 * <label></label><input>
 * options can be {
 *     labelClassNames: [],
 *     inputClassNames: []
 * }
 *
 * @param {string} suffix
 * @param {string} labelText
 * @param {object} options - additional options that can be used
 * @return {DocumentFragment}
 */
const createInputAndLabel = (suffix, labelText, options = {}) => {
    const frag = document.createDocumentFragment();
    const {
        labelClassNames,
        inputClassNames
    } = options;
    const input = inputClassNames ? createInput(suffix, inputClassNames) : createInput(suffix);
    const label = labelClassNames ? createLabel(suffix, labelText, labelClassNames) : createLabel(suffix, labelText);
    frag.appendChild(label);
    frag.appendChild(input);

    return frag;
};

// https://developer.mozilla.org/en-US/docs/Web/API/ValidityState

/**
 * todo test for different name
 * todo test for adding classes
 * more adaptable can create any input.
 * if want to control layout then we should use css rather than
 * moving dom elements
 *
 * @param {string} suffix - required for ids, names
 * @param {string} labelText - label text
 * @param {array} inputClasses - defaults to inputClassName
 * @param {array} labelClasses - defaults to labelClassNames
 * @param {object} inputAttributes - object of attributes to apply to input
 * @param {object} labelAttributes - object of attributes to apply to label
 * @return {DocumentFragment}
 */
const createInputAndLabel2 = ({
    suffix,
    labelText,
    inputClasses = inputClassNames,
    labelClasses = labelClassNames,
    inputAttributes = {
        id: suffix,
        name: suffix,
        type: 'text'
    },
    labelAttributes = {
        for: suffix
    }
}) => {
    const frag = createFrag();
    const label = createEleWithAttr('label', labelAttributes);
    addClassesToEle(label, labelClasses);
    label.textContent = labelText;
    const input = createEleWithAttr('input', inputAttributes);
    addClassesToEle(input, inputClasses);

    frag.appendChild(label);
    frag.appendChild(input);

    return frag;
};

export {
    createInput,
    createLabel,
    createInputAndLabel,
    createInputAndLabel2,
    labelClassNames
};
