import {
    CreateTextArea,
    divClassList,
    labelClassList,
    textareaClassList
} from '../createTextArea.mjs';

describe('CreateTextArea', () => {
    let createTextArea;
    beforeEach(() => {
        createTextArea = new CreateTextArea('abc');
    });

    afterEach(() => {
        createTextArea = null;
    });
    describe('set up', () => {
        it('should return an instance of CreateTextArea', () => {
            expect(createTextArea instanceof CreateTextArea).to.be.true;
        });

        // it('should call init function when outer is supplied', () => {
        //     const protoSpy = sinon.spy(CreateTextArea.prototype, 'init');
        //     createTextArea = new CreateTextArea('abc');
        //     expect(protoSpy.callCount).to.equal(1);
        //     protoSpy.restore();
        // });
    });

    describe('Prototype methods', () => {
        describe('createDomElements', () => {
            let divElement;
            let label;
            let textarea;
            beforeEach(() => {
                createTextArea.createDomElements();
                divElement = createTextArea.domElements.querySelector('div');
                label = divElement.querySelector('label');
                textarea = divElement.querySelector('textarea');
            });

            afterEach(() => {
                divElement = null;
            });


            it('should set the domElements property', () => {
                expect(createTextArea.domElements).not.to.be.null;
            });

            it('should have correct domElements', () => {
                expect(divElement).not.to.be.null;
            });

            it('should have a div with the correct data reference', () => {
                expect(divElement.dataset.textRef).
                to.
                equal('textarea-abc');
            });

            it('should have a div with the correct classes', () => {
                divClassList.forEach((className) => {
                    expect(divElement.classList.contains(className)).to.be.true;
                });
            });

            it('should have a label element', () => {
                expect(label).not.to.be.null;
            });

            it('should have a label with the correct classes', () => {
                labelClassList.forEach((className) => {
                    expect(label.classList.contains(className)).to.be.true;
                });
            });

            it('should have a label with the correct for', () => {
                expect(label.getAttribute('for')).
                to.
                equal('textarea-abc');
            });

            it('should have a label with correct text', () => {
                expect(label.textContent).
                to.
                equal('text abc');
            });

            it('should have a textarea', () => {
                expect(textarea).not.to.be.null;
            });

            it('should have a textarea with the correct attributes', () => {
                expect(textarea.id).
                to.
                equal('textarea-abc');
                expect(textarea.name).
                to.
                equal('textarea-abc');
            });

            it('should have a textarea with the correct classes', () => {
                textareaClassList.forEach((className) => {
                    expect(textarea.classList.contains(className)).to.be.true;
                });
            });
        });
    });
});
