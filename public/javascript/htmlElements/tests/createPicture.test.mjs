import {CreatePicture} from '../createPicture.mjs';

describe('CreatePicture', () => {
    let createPicture;
    beforeEach(() => {
        createPicture = new CreatePicture('abc');
    });

    afterEach(() => {
        createPicture = null;
    });


    describe('set up', () => {
        it('should return an instance of CreatePicture', () => {
            expect(createPicture instanceof CreatePicture).to.be.true;
        });
    });

    describe('Prototype methods', () => {
        describe('createDomElements', () => {
            let divElements;

            beforeEach(() => {
                createPicture.createDomElements();
                divElements = createPicture.domElements.querySelectorAll('div');
            });

            afterEach(() => {
                divElements = null;
            });

            it('should have div elements', () => {
                expect(divElements).not.to.be.null;
            });

            it('should have  with 2 divs', () => {
                expect(divElements.length).
                to.
                equal(2);
            });

            it('should have divs with the correct className', () => {
                divElements.forEach((div) => {
                    expect(div.classList.contains('input-wrap-inner-aside')).to.be.true;
                });
            });

            it('should have divs with the correct child nodes', () => {
                divElements.forEach((div) => {
                    const inputs = div.querySelectorAll('input');
                    const label = div.querySelectorAll('label');

                    expect(inputs.length).
                    to.
                    equal(3);
                    expect(label.length).
                    to.
                    equal(3);
                });
            });

            describe('Child nodes', () => {
                let childInputs;
                let childLabels;

                beforeEach(() => {
                    childInputs = createPicture.domElements.querySelectorAll('input');
                    childLabels = createPicture.domElements.querySelectorAll('label');
                });

                it('should have the correct amount of child elements', () => {
                    expect(childInputs.length).
                    to.
                    equal(6);
                    expect(childLabels.length).
                    to.
                    equal(6);
                });

                // needs text
                it('should have correct attributes on first labels', () => {
                    expect(childLabels[0].getAttribute('for')).
                    to.
                    equal('img-abc-0');
                    expect(childLabels[3].getAttribute('for')).
                    to.
                    equal('img-abc-1');
                });

                it('should have correct attributes on first labels', () => {
                    expect(childLabels[0].textContent).
                    to.
                    equal('img-0');
                    expect(childLabels[3].textContent).
                    to.
                    equal('img-1');
                });

                // this is the first and forth of the inputs
                it('should have correct attributes on first inputs', () => {
                    expect(childInputs[0].name).
                    to.
                    equal('img-abc-0');
                    expect(childInputs[0].id).
                    to.
                    equal('img-abc-0');
                    expect(childInputs[3].name).
                    to.
                    equal('img-abc-1');
                    expect(childInputs[3].id).
                    to.
                    equal('img-abc-1');
                });

                // this is the second and fifth of the inputs
                it('should have correct attributes on second labels', () => {
                    expect(childLabels[1].getAttribute('for')).
                    to.
                    equal('alt-abc-0');
                    expect(childLabels[4].getAttribute('for')).
                    to.
                    equal('alt-abc-1');
                });

                it('should have correct text on second labels', () => {
                    expect(childLabels[1].textContent).
                    to.
                    equal('alt-0');
                    expect(childLabels[4].textContent).
                    to.
                    equal('alt-1');
                });

                it('should have correct attributes on second inputs', () => {
                    expect(childInputs[1].name).
                    to.
                    equal('alt-abc-0');
                    expect(childInputs[1].id).
                    to.
                    equal('alt-abc-0');
                    expect(childInputs[4].name).
                    to.
                    equal('alt-abc-1');
                    expect(childInputs[4].id).
                    to.
                    equal('alt-abc-1');
                });

                // this is the third and sixth of the inputs
                it('should have correct attributes on third labels', () => {
                    expect(childLabels[2].getAttribute('for')).
                    to.
                    equal('cap-abc-0');
                    expect(childLabels[5].getAttribute('for')).
                    to.
                    equal('cap-abc-1');
                });

                it('should have correct attributes on third labels', () => {
                    expect(childLabels[2].textContent).
                    to.
                    equal('cap-0');
                    expect(childLabels[5].textContent).
                    to.
                    equal('cap-1');
                });

                it('should have correct attributes on third inputs', () => {
                    expect(childInputs[2].name).
                    to.
                    equal('cap-abc-0');
                    expect(childInputs[2].id).
                    to.
                    equal('cap-abc-0');
                    expect(childInputs[5].name).
                    to.
                    equal('cap-abc-1');
                    expect(childInputs[5].id).
                    to.
                    equal('cap-abc-1');
                });
            });
        });
    });
});
