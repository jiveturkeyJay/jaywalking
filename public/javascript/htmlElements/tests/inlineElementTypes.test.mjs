import {inlineElementTypes} from '../inlineElementTypes.mjs';

describe('inlineElementTypes', () => {
    describe('Abbr', () => {
        let abbr;
        beforeEach(() => {
            abbr = inlineElementTypes('abbr', 'abc-1');
        });
        afterEach(() => {
            abbr = null;
        });

        it('should return a document fragment', () => {
            expect(abbr.nodeType).to.equal(11);
            expect(abbr.nodeName.toLowerCase()).to.equal('#document-fragment');
        });
        it('should return a document fragment with correct amount of children', () => {
            expect(abbr.children.length).to.equal(5);
        });

        it('should have the correct amount of elements', () => {
            expect(abbr.querySelectorAll('input').length).to.equal(3);
            expect(abbr.querySelectorAll('label').length).to.equal(2);
        });

        it('should have the correct attributes on first label', () => {
            const firstLabel = abbr.children[0];
            expect(firstLabel.getAttribute('for')).to.equal('Text-abbr-abc-1');
            expect(firstLabel.textContent).to.equal('Text');
        });

        // this is really tested in the createLabelAndInput
        it('should have the correct attributes on first input', () => {
            const firstInput = abbr.children[1];
            expect(firstInput.getAttribute('id')).to.equal('Text-abbr-abc-1');
            expect(firstInput.getAttribute('name')).to.equal('Text-abbr-abc-1');
            expect(firstInput.getAttribute('required')).to.equal('required');
        });

        it('should have the correct attributes on second label', () => {
            const secondLabel = abbr.children[2];
            expect(secondLabel.getAttribute('for')).to.equal('Title-abbr-abc-1');
            expect(secondLabel.textContent).to.equal('Title');
        });

        it('should have the correct attributes on first input', () => {
            const firstInput = abbr.children[3];
            expect(firstInput.getAttribute('id')).to.equal('Title-abbr-abc-1');
            expect(firstInput.getAttribute('name')).to.equal('Title-abbr-abc-1');
            expect(firstInput.getAttribute('required')).to.equal('required');
        });

        it('should have the correct attributes on the hidden input', () => {
            const hiddenInput = abbr.children[4];
            expect(hiddenInput.type).to.equal('hidden');
            expect(hiddenInput.value).to.equal('abbr');
        });
    });

    describe('Link', () => {
        let link;
        beforeEach(() => {
            link = inlineElementTypes('link', 'abc-1');
        });
        afterEach(() => {
            link = null;
        });

        it('should return a document fragment', () => {
            expect(link.nodeType).to.equal(11);
            expect(link.nodeName.toLowerCase()).to.equal('#document-fragment');
        });
        it('should return a document fragment with correct amount of children', () => {
            expect(link.children.length).to.equal(5);
        });

        it('should have the correct amount of elements', () => {
            expect(link.querySelectorAll('input').length).to.equal(3);
            expect(link.querySelectorAll('label').length).to.equal(2);
        });

        it('should have the correct attributes on first label', () => {
            const firstLabel = link.children[0];
            expect(firstLabel.getAttribute('for')).to.equal('Url-link-abc-1');
            expect(firstLabel.textContent).to.equal('Url');
        });

        it('should have the correct attributes on first input', () => {
            const firstInput = link.children[1];
            expect(firstInput.getAttribute('id')).to.equal('Url-link-abc-1');
            expect(firstInput.getAttribute('name')).to.equal('Url-link-abc-1');
            expect(firstInput.getAttribute('required')).to.equal('required');
        });

        it('should have the correct attributes on second label', () => {
            const secondLabel = link.children[2];
            expect(secondLabel.getAttribute('for')).to.equal('Text-link-abc-1');
            expect(secondLabel.textContent).to.equal('Text');
        });

        it('should have the correct attributes on first input', () => {
            const firstInput = link.children[3];
            expect(firstInput.getAttribute('id')).to.equal('Text-link-abc-1');
            expect(firstInput.getAttribute('name')).to.equal('Text-link-abc-1');
            expect(firstInput.getAttribute('required')).to.equal('required');
        });

        it('should have the correct attributes on the hidden input', () => {
            const hiddenInput = link.children[4];
            expect(hiddenInput.type).to.equal('hidden');
            expect(hiddenInput.value).to.equal('link');
        });
    });
});
