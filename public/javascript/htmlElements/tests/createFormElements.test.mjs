import {
    createInput,
    createInputAndLabel,
    createInputAndLabel2,
    createLabel,
    labelClassNames
} from '../createFormElements.mjs';

describe('createLabel', () => {
    let label = createLabel('abc', 'label name');
    it('should return a label element', () => {
        expect(label.nodeName.toLowerCase()).to.equal('label');
    });

    it('should create a label with the correct text', () => {
        expect(label.textContent).to.equal('label name');
    });

    it('should create a label with the correct attributes', () => {
        expect(label.getAttribute('for')).to.equal('abc');
    });

    it('should create a label with default classNames', () => {
        labelClassNames.forEach((className) => {
            expect(label.classList.contains(className)).to.be.true;
        });
    });

    it('should create a label with supplied classNames', () => {
        const newLabel = createLabel('abc', 'some text', ['someclass']);
        expect(newLabel.classList.contains('someclass')).to.be.true;
    });
});

describe('createInput', () => {
    const input = createInput('abc');
    it('should create an input with the correct attributes', () => {
        expect(input.id).to.equal('abc');
        expect(input.name).to.equal('abc');
    });

    it('should have default classNames', () => {
        expect(input.classList.contains('input')).to.be.true;
    });

    it('should have classNames that are supplied', () => {
        const newInput = createInput('abc', ['someclass']);
        expect(newInput.classList.contains('someclass')).to.be.true;
    });
});

describe('createInputAndLabel', () => {
    const inputAndLabel = createInputAndLabel('abc', 'some string');
    it('should be a document fragment', () => {
        expect(inputAndLabel.nodeType).to.equal(11);
        expect(inputAndLabel.nodeName.toLowerCase()).to.equal('#document-fragment');
    });

    it('should have 2 children', () => {
        expect(inputAndLabel.children.length).to.equal(2);
    });

    it('should have the correct children', () => {
        expect(inputAndLabel.children[0].nodeName.toLowerCase()).to.equal('label');
        expect(inputAndLabel.children[1].nodeName.toLowerCase()).to.equal('input');
    });

    it('should allow optional overrides of classNames', () => {
        const inputAndLabelNew = createInputAndLabel('abc', 'some text', {
            labelClassNames: ['label-class'],
            inputClassNames: ['input-class'],
        });

        expect(inputAndLabelNew.children[0].classList.contains('label-class')).to.be.true;
        expect(inputAndLabelNew.children[1].classList.contains('input-class')).to.be.true;
    });

    describe('createInputAndLabel2', () => {
        let inputAndLabel;
        beforeEach(() => {
            inputAndLabel = createInputAndLabel2({
                suffix: 'abc',
                labelText: 'label text'
            });
        });

        afterEach(() => {
            inputAndLabel = null;
        });
        it('should be a document fragment', () => {
            expect(inputAndLabel.nodeType).to.equal(11);
            expect(inputAndLabel.nodeName.toLowerCase()).to.equal('#document-fragment');
        });

        it('should have two children', () => {
            expect(inputAndLabel.children.length).to.equal(2);
        });

        it('should have the correct children', () => {
            expect(inputAndLabel.children[0].nodeName.toLowerCase()).to.equal('label');
            expect(inputAndLabel.children[1].nodeName.toLowerCase()).to.equal('input');
        });

        describe('Defaults: nothing supplied except suffix or labelText', () => {
            let label;
            let input;

            beforeEach(() => {
                label = inputAndLabel.children[0];
                input = inputAndLabel.children[1];
            });

            it('should have a label with the correct attributes and text', () => {
                expect(label.textContent).to.equal('label text');
                expect(label.getAttribute('for')).to.equal('abc');
            });

            it('should have the default classNames', () => {
                expect(label.classList.contains('label')).to.be.true;
                expect(label.classList.contains('label-small')).to.be.true;
            });

            it('should have an input with correct attributes', () => {
                expect(input.type).to.equal('text');
                expect(input.id).to.equal('abc');
                expect(input.name).to.equal('abc');
            });

            it('should have the default classNames', () => {
                expect(input.classList.contains('input')).to.be.true;
            });
        });

        describe('Creates Inputs that over-ride default options', () => {
            it('should create different types of input', () => {
                //note this is not every possible type
                //also note that submit reset wont fit this as they don't have labels
                const potentialTypes = ['radio', 'checkbox', 'number', 'password', 'tel'];
                let newInputAndChildren;
                potentialTypes.forEach((type) => {
                    newInputAndChildren = createInputAndLabel2({
                        suffix: 'abc',
                        labelText: 'label text',
                        inputAttributes: {
                            type
                        }
                    });

                    expect(newInputAndChildren.children[1].nodeName.toLowerCase()).to.equal('input');
                    expect(newInputAndChildren.children[1].type).to.equal(type);
                });
            });

            it('should be able to create inputs with additional attributes', () => {
                const base = {
                    id: 'abc',
                    name: 'abc',
                    type: 'text'
                };

                const newAttr = {
                    maxlength: '100',
                    minlength: '10',
                    placeholder: 'bob',
                    pattern: '[a-z]{1,15}'
                };

                const newInputAndChildren = createInputAndLabel2({
                    suffix: 'abc',
                    labelText: 'label text',
                    inputAttributes: {
                        ...base,
                        ...newAttr
                    }
                });

                const input = newInputAndChildren.children[1];
                Object.keys(newAttr).forEach((attr) => {
                    expect(input.getAttribute(attr)).to.equal(newAttr[attr]);
                });
            });
        });
    });
});
