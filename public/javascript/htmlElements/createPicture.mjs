import {addClassesToEle, createElement, createFrag} from '../helpers/utils.mjs';
import {createInput, createLabel} from './createFormElements.mjs';


const divClassList = ['input-wrap-inner-aside'];

// we can get a ref using a data attr

/**
 * creates a text area to use in the intro para or anywhere
 *
 * with the follow dom elements
 * we need to supply an xx to the object
 *
 <div class="input-wrap-inner-aside">
 <label class="label label-small" for="image-xxx.0">Name 999</label>
 <input type="text" id="image-xxx.0" name="image-xxx.0" class="input" />

 <label class="label label-small" for="alt-xxx.0">Alt 999</label>
 <input type="text" id="alt-xxx.0" name="alt-xxx.0" class="input" />

 <label class="label label-small" for="caption-xxx.0">Cap 999</label>
 <input type="text" id="caption-xxx.0" name="caption-xxx.0" class="input" />
 </div>
 *
 * need to be able to update the xx
 *
 * to update/delete them then we'd need to loop through an object
 */
class CreatePicture {
    /**
     *
     * @param suffix
     */
    constructor(suffix) {
        if (suffix) {
            // need the _ otherwise has a fit
            this._suffix = suffix;
            this.domElements = null;
            this.labelsAndInputs = ['img', 'alt', 'cap'];
        }
    }

    get suffix() {
        return this._suffix;
    }

    set suffix(suffix) {
        this._suffix = suffix;
    }

    /**
     *
     * @param name
     * @param i
     * @returns {DocumentFragment}
     */
    createInputAndLabel(name, i) {
        const localSuffix = `${name}-${this._suffix}-${i}`;
        const text = `${name}-${i}`;
        const innerFrag = createFrag();
        const label = createLabel(localSuffix, text);
        const input = createInput(localSuffix);
        innerFrag.appendChild(label);
        innerFrag.appendChild(input);
        return innerFrag;
    }

    createDomElements() {
        const frag = createFrag();
        ['div', 'div'].forEach((eleName, i) => {
            const div = createElement(eleName);
            addClassesToEle(div, divClassList);
            let innerFrag;
            this.labelsAndInputs.forEach((name) => {
                innerFrag = this.createInputAndLabel(name, i);
                div.appendChild(innerFrag);
            });

            frag.appendChild(div);
        });


        this.domElements = frag;
    }
}

export {CreatePicture};
