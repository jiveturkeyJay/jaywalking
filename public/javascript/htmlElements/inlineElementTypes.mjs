import {createEleWithAttr, createFrag} from '../helpers/utils.mjs';
import {createInputAndLabel} from './createFormElements.mjs';

/**
 * supplies a type and returns a series of html elements
 * wrapped in a frag.
 * returned frag is inserted into DOM by whatever called it
 * @type {{link: string, abbr: string}}
 */
/**
 * abbr searchs for some text eg GPS and wraps in an abbr tag
 * with a title attribute
 * eg <abbr title="Global Positioning System">GPS</a>
 * "abbr": [{
 "type": "abbr",
 "text": "GPS",
 "title": "Global Positioning System"
 }]
 */

/**
 * link searches for a string within a para eg bobs cakes
 * and wraps that string within an a tag
 * eg <a href="something">bobs cakes</a>
 *  link: [{
 text: 'Capital Ring',
 path: 'https://tfl.gov.uk/modes/walking/capital-ring'
 }
 */

const typesContent = {
    abbr: {
        textInput: {labelText: 'Text'},
        titleInput: {labelText: 'Title'}
    },
    link: {
        urlInput: {labelText: 'Url'},
        textInput: {labelText: 'Text'}
    }
};

/**
 *
 * @param type
 * @param suffix
 * @return {DocumentFragment}
 */
    // todo add hidden element
const inlineElementTypes = (type, suffix) => {
        const linkContent = typesContent[type];
        const frag = createFrag();
        const baseSuffix = `${type}-${suffix}`;
        Object.values(linkContent).forEach((contentType) => {
            const {labelText} = contentType;
            // need to make sure that the labels are unique
            const suffixToUser = `${labelText}-${baseSuffix}`;
            const inputAndLabel = createInputAndLabel(suffixToUser, labelText);
            inputAndLabel.children[1].setAttribute('required', 'required');
            frag.appendChild(inputAndLabel);
        });

        // add a hidden input to the frag
        const hidden = createEleWithAttr('input', {
            type: 'hidden',
            value: type,
            id: `hidden-${type}-${suffix}`
        });
        frag.appendChild(hidden);

        return frag;
    };

export {
    inlineElementTypes
};
