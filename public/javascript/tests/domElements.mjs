const domStrFigure = "<main class=\"grid-post-main\" id=\"main\"><article id=\"article-page\"><figure class=\"fig\"><picture class=\"fig__pic\"><button  aria-expanded=\"false\" aria-controls=\"modal-dialog\" data-open-modal=\"true\" data-image-name=\"image-name\" data-image-alt=\"Alt text\" class=\"btn btn__dialog no-js-hide-item\">Click me</button></picture><figcaption class=\"fig__text\">Some figure caption 0</figcaption></figure></article></main>";
const dialogStr = "<div id=\"modal-dialog\" class=\"dialog-outer is-hidden\" data-close-modal=\"true\"><div class=\"dialog\" aria-modal=\"true\" role=\"dialog\" aria-labelledby=\"dialog2_label\"><div class=\"dialog__cont\"><h2 id=\"dialog2_label\" class=\"dialog_label\" class=\"is-accessible-text\">Large image</h2><img src=\"\" alt=\"\" /><button class=\"btn btn__dialog btn__dialog--close\" data-close-modal=\"true\">Close</button></div></div></div>";

export {
	domStrFigure,
	dialogStr
};
