describe("Articles.mjs", () => {
	const domStringStr = "<main class=\"grid-post-main\" id=\"main\"><article id=\"article-page\">";
	const domStrNoFigures = "<p>Some text</p>";
	const domStrFigure = "<figure class=\"fig\"><picture class=\"fig__pic\"><button data-image-name=\"{{ xxx }}\" class=\"btn btn__dialog no-js-hide-item\"><svg data-image-name=\"ndw2_path{{ xxx }}\" aria-hidden=\"true\" class=\"svg__magnify\" width=\"45\" height=\"45\" viewBox =\"0 0 750 750\"><use href=\"#svg-magnify\"></use></svg></button></picture><figcaption class=\"fig__text\">Some figure caption {{ xxx }}</figcaption></figure>";
	const domStringEnd = "</article></main>";

	let domString;
	let outer;

	function createDomFigureStr(length = 3) {
		let str = "";
		for (let i = 0; i < length; i++) {
			str += domStrFigure.replace(/\{\{ xxx }}/g, i);
		}

		return str;
	}

	describe("No images on article page", () => {
		beforeEach(() => {
			domString = `${domStringStr}${domStrNoFigures}${domStringEnd}`;
			document.body.innerHTML = domString;
			outer = document.getElementById("article-page");
		});

		afterEach(() => {
			document.body.innerHTML = "";
			domString = null;
			outer = null;
		});

		it("should have an outer element", () => {
			expect(outer).not.to.be.null;
		});


	});

	describe("Images on article page", () => {
		it("should have correct DOM elements [sanity check]", () => {
			domString = `${domStringStr}${createDomFigureStr()}${domStringEnd}`;
			document.body.innerHTML = domString;
			outer = document.getElementById("article-page");
			expect(document.querySelectorAll(".fig__pic").length).to.equal(3);
		});
	});
});
