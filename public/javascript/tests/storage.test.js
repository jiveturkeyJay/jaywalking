import {Storage} from '../storage.mjs';

const fakeWindowLocalStorage = {
    data: {},
    length: 0,
    setLength() {
        this.length = Object.keys(this.data).length;
    },
    setItem(name, value) {
        this.data[name] = value.toString();
        this.setLength();
    },
    getItem(name) {
        return this.data[name];
    },
    removeItem(name) {
        delete this.data[name];
        this.setLength();
    },
    clear() {
        this.data = {};
        this.setLength();
    }
};


describe('Storage', () => {

    let storage;
    const name = 'name';
    const value = 'Hello';
    let dateStubber;

    beforeEach(() => {

        const dateValue1 = new Date('2020-05-14T11:01:58.135Z').valueOf();
        const dateValue2 = new Date('2020-05-14T11:01:58.135Z').valueOf();
        const dateValue3 =new Date('2020-05-14T11:04:58.135Z').valueOf();

        dateStubber = sinon.stub(global.Date, 'now')
          .onFirstCall().returns(dateValue1)
          .onSecondCall().returns(dateValue2)
          .onThirdCall().returns(dateValue3)

        Object.defineProperty(window, "sessionStorage", {
            value: fakeWindowLocalStorage,
            writable: true
        });

        storage = new Storage();
        storage.set(name, {
            value
        });
    });

    // need to remove
    afterEach(() => {
        storage = null;
        dateStubber.restore()
    });

    describe('set method', () => {
        it('should set object with correct keys', () => {
            const setValue = JSON.parse(window.sessionStorage.getItem(name));
            expect(Object.keys(setValue)).to.eql(['setTime', 'cacheLimit', 'value']);
        });

        it('should set object with correct values', () => {
            const setValue = JSON.parse(window.sessionStorage.getItem(name));
            expect(Object.values(setValue)).to.eql([1589454118135, 1589454418135, {
                value: 'Hello'
            }]);
        });
    });

    describe('remove method', () => {
        it('should remove named value', () => {
            expect(window.sessionStorage.length).to.equal(1);
            storage.remove(name);
            expect(window.sessionStorage.length).to.equal(0);
        });
    });

    describe('has method', () => {
        it('should return false if named key does not exist', () => {
            expect(storage.has('bob')).to.equal(false);
        });

        it('should return true if named key exists', () => {
            expect(storage.has(name)).to.equal(true);
        });
    });

    describe('get method', () => {
        it('should return the value if it exists', () => {
            expect(storage.get(name)).not.to.be.undefined;
        });

        it('should return undefined if value does not exist', () => {
            expect(storage.get('bob')).to.be.undefined;
        });
    });

    describe('clear method and length method', () => {
        it('should clear all the data', () => {
            expect(window.sessionStorage.length).to.equal(1);
            storage.clear();
            expect(window.sessionStorage.length).to.equal(0);
        });
    });

    /**
     * note this method test relies on the order of the mocked Date.now (above)
     */
    describe('shouldUpdateCache method', () => {
       it('should return true if utc is expired', () => {
           // use -1 as it will set the cache to a millisecond before Date.now()
           storage.set('expired', {
               value
           },  -1
           );

            expect(storage.shouldUpdateCache('expired')).to.be.true;
       });

       it('should return true if key does not exist', () => {
           expect(storage.shouldUpdateCache('bob')).to.be.true;
       });

        it('should return false if the utc is not expired', () => {
            expect(storage.shouldUpdateCache(name)).to.be.false;
        });
    });
});
