import { DialogClass } from "../DialogClass.mjs";
import { dialogStr, domStrFigure } from "./domElements.mjs";

describe("DialogClass", () => {
	let dialog;
	const openDataStr = "openModal";
	let outer;
	beforeEach(() => {
		document.body.innerHTML = `${domStrFigure}${dialogStr}`;
		outer = document.getElementById("article-page");
		dialog = new DialogClass(outer, openDataStr);
	});

	afterEach(() => {
		document.body.innerHTML = "";
		dialog.closeModalAction();
		outer = null;
		dialog = null;
		// this removes all events on the body
		document.body.outerHTML = document.body.outerHTML;
	});

	describe("Set up", () => {
		it("should create an instance correctly", () => {
			expect(dialog).to.be.an.instanceof(DialogClass);
		});

		it("should have the correct properties on instantiation", () => {
			dialog = new DialogClass(outer, "openModal");
			expect(dialog.outer.nodeName).to.equal("ARTICLE");
			expect(dialog.openDataStr).to.equal("openModal");
			expect(dialog.closeDataStr).to.equal("closeModal");
			expect(dialog.dialog.nodeName).to.equal("DIV");
		});

		it("should NOT call init function if outer and openDataStr are NOT supplied", () => {
			const protoSpy = sinon.spy(DialogClass.prototype, "init");
			dialog = new DialogClass();
			expect(protoSpy.callCount).to.equal(0);
			protoSpy.restore();
		});

		it("should NOT call init function if outer is NOT supplied", () => {
			const protoSpy = sinon.spy(DialogClass.prototype, "init");
			dialog = new DialogClass(null, ".abc");
			expect(protoSpy.callCount).to.equal(0);
			protoSpy.restore();
		});

		it("should NOT call init function if openDataStr is NOT supplied", () => {
			const protoSpy = sinon.spy(DialogClass.prototype, "init");
			dialog = new DialogClass(outer);
			expect(protoSpy.callCount).to.equal(0);
			protoSpy.restore();
		});

		// it("should NOT call init function if dialog element does not exist in the DOM", () => {
		// 	const protoSpy = sinon.spy(DialogClass.prototype, "init");
		// 	dialog = new DialogClass(outer, "openModal", "closeModal", "modal-dialog2");
		// 	expect(protoSpy.callCount).to.equal(0);
		// 	protoSpy.restore();
		// });

		it("should call init function if outer and openDataStr are supplied", () => {
			const protoSpy = sinon.spy(DialogClass.prototype, "init");
			dialog = new DialogClass(outer, "openModal");
			expect(protoSpy.callCount).to.equal(1);
			protoSpy.restore();
		});
	});

	describe("Prototype methods", () => {
		describe("Init", () => {
			it("should call setUpEvents method from inti", () => {
				const protoSpy = sinon.spy(DialogClass.prototype, "setUpEvents");
				dialog = new DialogClass(outer, openDataStr);
				expect(protoSpy.callCount).to.equal(1);
				protoSpy.restore();
			});
		});

		describe("SetUpEvents", () => {
			it("should call openModal when button is clicked", () => {
				const protoSpy = sinon.spy(DialogClass.prototype, "openModal");
				dialog = new DialogClass(outer, openDataStr);
				const button = document.querySelector("button");
				button.click();
				expect(protoSpy.callCount).to.equal(1);
				protoSpy.restore();
			});

			it("should NOT call openModal when area outside delegation location is clicked is clicked", () => {
				const protoSpy = sinon.spy(DialogClass.prototype, "openModal");
				dialog = new DialogClass(outer, openDataStr);
				const main = document.querySelector("main");
				main.click();
				expect(protoSpy.callCount).to.equal(0);
				protoSpy.restore();
			});

			it("should call closeModal when body or elsewhere is clicked", () => {
				const protoSpy = sinon.spy(DialogClass.prototype, "closeModal");
				dialog = new DialogClass(outer, openDataStr);

				// open the modal to allow the event listeners on the body
				const target = document.querySelector("button");
				const evt = {
					preventDefault: () => {
					},
					stopPropagation: () => {
					},
					target
				};

				dialog.openModal(evt);

				const main = document.querySelector("main");
				main.click();
				expect(protoSpy.callCount).to.equal(1);
				protoSpy.restore();
			});
		});

		//todo - add tests to show that after closing modal the events are removed
		// call event directly
		describe("openModal", () => {
			let preventDefault;
			let evt;
			let target;
			let stopPropagation;
			beforeEach(() => {
				preventDefault = sinon.spy();
				stopPropagation = sinon.spy();
				target = document.querySelector("button");
				evt = {
					stopPropagation,
					preventDefault,
					target
				};
			});

			afterEach(() => {
				preventDefault = null;
				stopPropagation = null;
				evt = null;
				target = null;
			});

			it("should call preventDefault", () => {
				expect(preventDefault.callCount).to.equal(0);
				dialog.openModal(evt);
				expect(preventDefault.callCount).to.equal(1);
			});

			it("should NOT open modal when target does NOT contain correct openDataStr prop", () => {
				target = document.createElement("button");
				expect(dialog.openModal({
					target,
					preventDefault,
					stopPropagation
				})).to.be.false;
			});

			it("should remove is-hidden class", () => {
				dialog.openModal(evt);
				expect(dialog.dialog.classList.contains("is-hidden")).to.be.false;
			});

			it("should open modal when target DOES contain correct openDataStr prop", () => {
				expect(dialog.openModal(evt)).to.be.true;
			});

			it("should set alt and src on the dialog image", () => {
				dialog.openModal(evt);
				const dialogImage = document.querySelector("#modal-dialog img");
				expect(dialogImage.src).to.equal("/images/image-name_large.jpg");
				expect(dialogImage.alt).to.equal("Alt text");
			});

			// not working
			// it("should focus on the dialog automatically", () => {
			// 	dialog.openModal(evt);
			// 	const dialogEle = document.querySelector("dialog");
			// 	expect(document.activeElement).to.equal(dialogEle);
			// });
		});

		describe("closeModal", () => {
			let preventDefault;
			let closeTarget;
			let closeEvent;

			beforeEach(() => {
				preventDefault = sinon.spy();
				closeTarget = document.querySelector("[data-close-modal=\"true\"]");
				closeEvent = {
					preventDefault,
					target: closeTarget
				};
			});

			afterEach(() => {
				preventDefault = null;
				closeEvent = null;
			});

			it("should call preventDefault", () => {
				expect(preventDefault.callCount).to.equal(0);
				dialog.closeModal(closeEvent);
				expect(preventDefault.callCount).to.equal(1);
			});

			it("should NOT close modal when target does NOT contain correct closeDataStr prop", () => {
				const target = document.createElement("button");
				expect(dialog.closeModal({
					target,
					preventDefault
				})).to.be.false;
			});

			it("should close modal when target DOES contain correct closeDataStr prop", () => {
				expect(dialog.closeModal(closeEvent)).to.be.true;
			});

			it("should call closeModalAction when target DOES contain correct closeDataStr prop", () => {
				const protoSpy = sinon.spy(DialogClass.prototype, "closeModalAction");
				dialog.closeModal(closeEvent);
				expect(protoSpy.callCount).to.equal(1);
				protoSpy.restore();
			});
		});

		describe("closeModalAction", () => {
			let dialogDom;

			beforeEach(() => {
				dialogDom = document.querySelector("#modal-dialog");
				dialogDom.classList.remove("is-hidden");
				const img = dialogDom.querySelector("img");
				img.src = "/images/image-name_large.jpg";
				img.alt = "Alt text";

			});

			it("should add is-hidden class to the dialog", () => {
				dialogDom = document.querySelector("#modal-dialog");
				dialog.closeModalAction();
				expect(dialogDom.classList.contains("is-hidden")).to.be.true;

			});

			// we need to use openModal first to get up the image src and alt
			it("should reset the alt and src attributes", () => {
				const dialogImage = document.querySelector("#modal-dialog img");
				expect(dialogImage.src).to.equal("/images/image-name_large.jpg");
				expect(dialogImage.alt).to.equal("Alt text");

				dialog.closeModalAction();

				expect(dialogImage.src).to.equal("");
				expect(dialogImage.alt).to.equal("");
			});
		});

		describe("keyListener", () => {
			it("should call closeModalAction when key code is \"Escape\"", () => {
				const protoSpy = sinon.spy(DialogClass.prototype, "closeModalAction");
				const evt = {
					code: "Escape",
					preventDefault: sinon.spy()
				};

				dialog.keyListener(evt);

				expect(evt.preventDefault.callCount).to.equal(0);
				expect(protoSpy.callCount).to.equal(1);
				protoSpy.restore();
			});


			it("should do nothing when key code is not \"Escape\" or \"Tab\"", () => {
				const protoSpy = sinon.spy(DialogClass.prototype, "closeModalAction");
				const evt = {
					code: "Blah",
					preventDefault: sinon.spy()
				};

				dialog.keyListener(evt);

				expect(evt.preventDefault.callCount).to.equal(0);
				expect(protoSpy.callCount).to.equal(0);
				protoSpy.restore();
			});

			it("should focus on the close button and prevent default when key code is \"Tab\"", () => {
				const protoSpy = sinon.spy(DialogClass.prototype, "closeModalAction");
				const evt = {
					code: "Tab",
					preventDefault: sinon.spy()
				};

				dialog.keyListener(evt);

				expect(evt.preventDefault.callCount).to.equal(1);
				// check it is focused on the correct element
				expect(document.activeElement.dataset.closeModal).to.equal("true");
				expect(protoSpy.callCount).to.equal(0);
				protoSpy.restore();

			});
		});


	});

	// additional tests to check that the events are working: some duplication
	describe("Events", () => {
		let preventDefault;
		//let openEvent;
		//let openTarget;
		//let closeEvent;
		//let closeTarget;


		beforeEach(() => {
			preventDefault = sinon.spy();
			//openTarget = document.querySelector("button");
			// openEvent = {
			// 	preventDefault,
			// 	target: openTarget
			// };

			//closeTarget = document.querySelector("[data-close-modal=\"true\"]");
			// closeEvent = {
			// 	preventDefault,
			// 	target: closeTarget
			// };
		});

		afterEach(() => {
			preventDefault = null;
			//openEvent = null;
			//openTarget = null;
			//closeEvent = null;
		});

		// problem with the events as some are not being removed.
		// we probably need to remove the ones from setUpEvents
		// ie we need to clear events
		it("should call openModal when button is clicked 2", () => {
			const openModalSpy = sinon.spy(DialogClass.prototype, "openModal");
			const closeModalSpy = sinon.spy(DialogClass.prototype, "closeModal");
			const closeModalActionSpy = sinon.spy(DialogClass.prototype, "closeModalAction");

			dialog = new DialogClass(outer, openDataStr);
			const button = document.querySelector("button");
			button.click();

			expect(openModalSpy.callCount).to.equal(1);
			// close modal is called as it is delegated
			expect(closeModalSpy.callCount).to.equal(0);
			expect(closeModalActionSpy.callCount).to.equal(0);

			// click on main
			document.getElementById("main").click();
			expect(closeModalSpy.callCount).to.equal(1);
			expect(closeModalActionSpy.callCount).to.equal(0);
			//
			// click on close button
			// something is odd here
			document.querySelector("[data-close-modal=\"true\"]").click();
			expect(closeModalSpy.callCount).to.equal(2);
			// this should only be 1?
			expect(closeModalActionSpy.callCount).to.equal(2);

			openModalSpy.restore();
			closeModalSpy.restore();
			closeModalActionSpy.restore();
		});
	});

});
