/**
 *
 * @param {HTMLElement} ele - an element to add
 * @param {array} classNames - an array of classnames to add
 */
const addClassesToEle = (ele, classNames) => {
	return ele.classList.add(...classNames);
};

const removeClassesToEle = (ele, classNames) => {
	return ele.classList.remove(...classNames);
};

const keys = (obj) => Reflect.ownKeys(obj);
const has = (obj, prop) => Reflect.has(obj, prop);
const set = (obj, prop, value) => Reflect.set(obj, prop, value);
const getKeys = obj => Reflect.ownKeys(obj);
const reflectSlice = (items) => Reflect.apply(Array.prototype.slice, items, []);

/**
 * what happens if the element is not correct
 * @return {HTMLElement}
 * @param {string} name - name of a valid dom element
 */
const createElement = name => name && document.createElement(name);

/**
 * use setAttribute as some attr such as for can't be set
 * using element.[attrname] = xxx
 * @param {string} name - ie the dom element to create
 * @param {object} attr - object to add as attributes
 */
const createEleWithAttr = (name, attr = {}, dataSet = {}) => {
	const ele = createElement(name);
	const keys = getKeys(attr);
	const dataKeys = getKeys(dataSet);

	if (keys.length) {
		keys.forEach((key) => {
			ele.setAttribute(key, attr[key]);
		});
	}

	if (dataKeys) {
		dataKeys.forEach((key) => {
			ele.dataset[key] = dataSet[key];
		});
	}

	return ele;
};

const createFrag = () => document.createDocumentFragment();

const getFromDataSet = (element, dataItem) => {
	return element.dataset[dataItem];
};

/**
 *
 * @param node
 * @param attr
 * @return {string} - the attribute of the
 */
const getAttr = (node, attr) => {
	return node.getAttribute(attr);
};

/**
 *
 * @param node
 * @param {array} - attrArr : an array of strings
 */
const getMultipleAttr = (node, attrArr) => {
	return attrArr.reduce((acc, attrName) => {
		if (node.hasAttribute(attrName)) {
			acc[attrName] = node.getAttribute(attrName);
		}
		return acc;

	}, {});
};

/**
 * queries dom and returns an array from the result
 * @param selector
 * @param outer
 * @return {array|null}
 */
const qsa = (selector, outer = undefined) => {
	const items = (outer || document).querySelectorAll(selector);
	return items.length ? reflectSlice(items) : null;
};

const qs = (selector, outer = document) => {
	return outer.querySelector(selector);
};


/**
 * todo test
 * queries dom and gets first item form the series
 * eg first radio button
 * @param selector - name
 * @param outer
 */
const gsInitialGroupEle = (name, outer = undefined) => {
	let context = document;
	if (outer) {
		context = outer;
	}

	return context.querySelector(`[name="${name}"]:first-of-type`);
};

const replaceDataOnNode = (node, value, attr) => {
	node.dataset[attr] = getFromDataSet(node, attr).replace(/\d+/g, value);
	return node;
};

/**
 * replace xxx-1 with xxx-12
 * used for text content
 * @param node
 * @param attr
 * @param value
 */
const updateNumericTextOnNode = (node, value) => {
	const currText = node.textContent;
	node.textContent = currText.replace(/\d+/g, value);
	return node;
};

/**
 * just used for replacing number at end of ids,
 * for attributes eg something like xxxx-1
 * @param node
 * @param attr
 * @param value
 */
const updateNumericAttrOnNode = (node, value, attr) => {
	const current = getAttr(node, attr).replace(/\d+/g, value);
	node.setAttribute(attr, current);
	return node;
};

const hideShow = (node) => {
	node.previousElementSibling.addEventListener("click", (evt) => {
		node.classList.toggle("is-hidden-block");
	}, false);
};

const byId = (id) => document.getElementById(id);

export {
	addClassesToEle,
	byId,
	createElement,
	createEleWithAttr,
	createFrag,
	getAttr,
	getFromDataSet,
	getMultipleAttr,
	gsInitialGroupEle,
	has,
	hideShow,
	keys,
	qs,
	qsa,
	reflectSlice,
	removeClassesToEle,
	replaceDataOnNode,
	set,
	updateNumericAttrOnNode,
	updateNumericTextOnNode
};
