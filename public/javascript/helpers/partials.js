import {getFromDataSet, updateNumericAttrOnNode} from './utils.mjs';

// tested outside
const compose = (...fns) => fns.reduce((f, g) => (...args) => f(g(...args)));
const partialRight = (fn, ...partiallyAppliedArgs) => (...remainingArgs) => fn.apply(this, remainingArgs.concat(partiallyAppliedArgs));
const replaceName = partialRight(updateNumericAttrOnNode, 'name');
const getDataIndex = partialRight(getFromDataSet, 'index');
const getAndParseDataIndex = compose((data) => parseInt(data, 10), getDataIndex);
const replaceFor = partialRight(updateNumericAttrOnNode, 'for');
const replaceId = partialRight(updateNumericAttrOnNode, 'id');


export {
    partialRight,
    replaceId,
    replaceFor,
    getAndParseDataIndex,
    replaceName
};
