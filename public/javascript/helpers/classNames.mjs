const errorListItemClass = 'error-list-item';
const inputError = 'input__error';
const inputGroupError = 'input-group-error';
const errorHolderClass = 'error-holder';

const classNames = {
    formValidation: {
        errorListItemClass,
        inputError,
        inputGroupError,
        errorHolderClass
    }
};

export {
    classNames
};
