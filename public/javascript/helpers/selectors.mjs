
const radioType = "[name=\"type\"]:checked";

const buttonGetByType = "[data-button=\"get-by-type\"]";
const buttonGetBySubType = "[data-button=\"get-by-sub-type\"]";

const fieldSetType = "subTypeParamOuter";
const fieldSetArticleParam = "subTypeArticleParamOuter";

export {
	buttonGetByType,
	buttonGetBySubType,
	fieldSetArticleParam,
	fieldSetType,
	radioType
};
