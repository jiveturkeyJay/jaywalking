import * as utils from '../utils.mjs';

const {
    addClassesToEle,
    createElement,
    createEleWithAttr,
    getMultipleAttr,
    createFrag,
    getAttr,
    getFromDataSet,
    replaceDataOnNode,
    getAndParseDataIndex,
    //partialRight,
    hideShow,
    qsa,
    reflectSlice,
    updateNumericAttrOnNode,
    updateNumericTextOnNode,
    replaceId,
    replaceFor,
    replaceName
} = utils;
describe('Utils', () => {
    beforeEach(() => {
    });

    afterEach(() => {
        document.body.innerHTML = '';
    });

    describe('createElement', () => {
        it('should return undefined if not an element', () => {
            const ele = createElement();
            expect(ele).to.be.undefined;
        });

        it('should create an element of the correct type', () => {
            ['div', 'span', 'main', 'dt'].forEach((eleName) => {
                const ele = createElement(eleName);
                expect(ele.nodeName.toLowerCase()).to.equal(eleName);
            });
        });
    });

    describe('createElementWithAttr', () => {
        it('should return undefined if nothing supplied', () => {
            expect(createEleWithAttr()).to.be.undefined;
        });

        it('should return an element', () => {
            expect(createEleWithAttr('div').nodeName.toLowerCase()).to.equal('div');
        });

        it('should add attributes correct using supplied object', () => {
            const attr = {
                type: 'text',
                name: 'name',
                id: 'name',
                required: 'required'
            };
            const ele = createEleWithAttr('input', attr);
            Object.keys(attr).forEach((key) => {
                expect(ele.getAttribute(key)).to.equal(attr[key]);
            });
        });

        it('should add dataset info if added', () => {
            const ele = createEleWithAttr('input', {}, {
                index: '2',
                name: 'name'
            });

            expect(ele.dataset.index).to.equal('2');
            expect(ele.dataset.name).to.equal('name');
        });
    });

    describe('addClassesToEle', () => {
        it('should add classes correctly to elements via an array', () => {
            const ele = createElement('div');
            const classNameArr = ['one', 'two', 'three'];
            addClassesToEle(ele, classNameArr);
            classNameArr.forEach((name) => {
                expect(ele.classList.contains(name)).to.be.true;
            });
        });
    });

    describe('getMultipleAttr', () => {
        let ele;
        const arrayOfAttr = ['name', 'type', 'id'];
        beforeEach(() => {
            ele = createElement('input');
            ele.setAttribute('name', 'bob');
            ele.setAttribute('type', 'text');
            ele.setAttribute('id', 'someid');
        });

        afterEach(() => {
            ele = null;
        });

        it('should return object of name and values', () => {
            const returnedValues = getMultipleAttr(ele, arrayOfAttr);
            expect(returnedValues).to.have.all.keys('name', 'type', 'id');
        });

        it('should return object with correct values', () => {
            const returnedValues = getMultipleAttr(ele, arrayOfAttr);

            expect(returnedValues.id).to.equal('someid');
            expect(returnedValues.name).to.equal('bob');
            expect(returnedValues.type).to.equal('text');
        });

        // just test to check
        it('should deconstruct object', () => {
            const {
                id,
                name,
                type
            } = getMultipleAttr(ele, arrayOfAttr);

            expect(id).to.equal('someid');
            expect(name).to.equal('bob');
            expect(type).to.equal('text');
        });


        it('should not return values which do not exist', () => {
            const returnedValues = getMultipleAttr(ele, ['name', 'required']);
            expect(returnedValues).to.have.all.keys('name');
            expect(Object.keys(returnedValues).length).to.equal(1);
        });

        it('should return empty object if no attributes set', () => {
            const returnedValues = getMultipleAttr(ele, ['required']);
            expect(Object.keys(returnedValues).length).to.equal(0);
        });
    });

    describe('updateNumericAttrOnNode', () => {
        it('should replace attr with updated value', () => {
            const attr = {
                type: 'text',
                name: 'name-0',
                id: 'name',
                required: 'required'
            };
            let ele = createEleWithAttr('input', attr);
            ele = updateNumericAttrOnNode(ele, '1', 'name');
            expect(ele.getAttribute('name')).to.equal('name-1');
        });

        it('should handle 2 digit updates', () => {
            const attr = {
                type: 'text',
                name: 'name-0',
                id: 'name',
                required: 'required'
            };
            let ele = createEleWithAttr('input', attr);
            ele = updateNumericAttrOnNode(ele, '12', 'name');
            expect(ele.getAttribute('name')).to.equal('name-12');
        });
    });
});
