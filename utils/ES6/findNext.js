/* eslint-disable no-nested-ternary */
/* eslint-disable max-len */
import getContextAndIndex from './getContextAndIndex.js';

/**
 * Using an array or object returns the next element or if on the last element returns the first
 * @param {array|string|object} oiteratee - pretty much anything that can be put into made into an array
 * @param {string} value string to find
 * @returns {Undefined|number} undefined if no next index or an array of length 1
 */
const findNext = (oiteratee, value) => {
	//clone this
	const {group, length, index} = getContextAndIndex(oiteratee, value);
	//is value there? then return false or only length of 1 return false
	return (index !== -1 && length >= 2) ? (index === length - 1) ? group[0] : group[index + 1] : undefined;
};

export default findNext;
