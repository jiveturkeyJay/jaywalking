import * as monads from './monads.js';

const {Maybe} = monads;

/**
 * checks if a number is an integer
 * @param {x} x - any value
 * @returns {boolean} - bool
 */
const isInt = (x) => Maybe.of(x).map(Number.isInteger).join().__value;

export default isInt;
