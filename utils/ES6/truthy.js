import existy from './existy.js';

/**
 * for occasions when we might need 0, '' as a valid  value this
 * returns false for really false values eg undefined, null, false
 * @param {*} x value
 * @returns {boolean} - a boolean
 */
const truthy = (x) => (x !== false) && existy(x);

export default truthy;
