// no test
const reduce = (arr, fn, acc) => arr.reduce(fn, acc);

export default reduce;
