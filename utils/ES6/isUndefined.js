import isType from './isType.js';

const isUndefined = (elem) => isType(elem, 'Undefined');

export default isUndefined;
