import isEmpty from './isEmpty.js';

/**
 * alias to prevent !isEmpty
 * @param {object|array|string} x - any value
 * @returns {boolean} - bool
 */
const isNotEmpty = (x) => !isEmpty(x);

export default isNotEmpty;
