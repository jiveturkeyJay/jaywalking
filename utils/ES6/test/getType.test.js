//can getType an array or object
import getType from '../getType.js';

describe('getType', () => {
	it('should return a string with the correct type', () => {
		expect(getType({})).to.equal('Object');
		expect(getType([])).to.equal('Array');
		expect(getType('string')).to.equal('String');
		expect(getType(1)).to.equal('Number');
		expect(getType(x => x)).to.equal('Function');
		expect(getType(null)).to.equal('Null');
		expect(getType(Symbol('bob'))).to.equal('Symbol');
		expect(getType(undefined)).to.equal('Undefined');
		//trouble with these 2 for some reason
		//problem is with JS dom and it's implementation
		//expect(getType(div)).to.equal('NodeList');
		//expect(getType(classList)).to.equal('DOMTokenList');
	});
});
