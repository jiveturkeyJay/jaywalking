import findNext from '../findNext.js';

describe('findNext', () => {
	//test object and array
	//but set context should sort most
	const valuesArray = ['a', 'b', 'c', 'd', 'e', 'f'];
	const singleArray = ['a'];
	const doubleArray = ['a', 'b'];

	const valuesObj = {
		a: 'a', b: 'b', c: 'c', d: 'd', e: 'e', f: 'f'
	};
	const singleObj = {a: 'a'};
	const doubleObj = {a: 'a', b: 'b'};

	it('should return undefined if no oiteree is supplied', () => {
		expect(findNext()).to.be.undefined;
	});

	it('should return undefined if there is only one element to be indexed', () => {
		expect(findNext(singleArray)).to.be.undefined;
		expect(findNext(singleObj)).to.be.undefined;
	});

	it('should return the next index if there are two elements to be indexed', () => {
		expect(findNext(doubleArray, 'a')).to.equal('b');
		expect(findNext(doubleObj, 'a')).to.equal('b');
	});

	it('should return the third index if the second index is supplied', () => {
		expect(findNext(valuesArray, 'b')).to.equal('c');
		expect(findNext(valuesObj, 'b')).to.equal('c');
	});

	it('should return the first index if the last index is supplied', () => {
		expect(findNext(valuesArray, 'f')).to.equal('a');
		expect(findNext(valuesObj, 'f')).to.equal('a');
	});
});
