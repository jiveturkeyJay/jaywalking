/* eslint-disable no-param-reassign */
import compose from '../compose.js';

describe('compose', () => {
	const o = {
		upper: str => str.toUpperCase(),
		reverse: str => [...str].reverse().join(''),
		add: str => str += 'extra'
	};

	it('should compose properly', () => {
		const composed = compose(o.upper, o.reverse, o.add);
		expect(composed('one')).to.equal('ARTXEENO');
	});

	it('should work with a curried function', () => {
		//this is really a partially applicated function
		const curryA = (pre) => (str) => pre + str;
		const composed = compose(curryA('blah'), o.upper, o.reverse, o.add);

		expect(composed('one')).to.equal('blahARTXEENO');
	});

	it('should work with a composed argument', () => {
		const composedArg = compose((str) => `pre${str}`, (str) => `dee${str}`);
		const composed = compose(composedArg, o.upper, o.reverse, o.add);

		expect(composed('one')).to.equal('predeeARTXEENO');
	});
});
