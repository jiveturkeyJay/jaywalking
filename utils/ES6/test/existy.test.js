import existy from '../existy.js';

describe('exists', () => {
	it('should return true result when arg exists', () => {
		expect(existy('')).to.be.true;
		expect(existy(0)).to.be.true;
		expect(existy({})).to.be.true;
		expect(existy([])).to.be.true;
		expect(existy(false)).to.be.true;
		expect(existy(true)).to.be.true;
		expect(existy('a')).to.be.true;
		expect(existy(1)).to.be.true;
		expect(existy(new Map())).to.be.true;
	});

	it('should return false when the arg is null, undefined', () => {
		[null, undefined].forEach((val) => {
			expect(existy(val)).to.be.false;
		});
	});
});
