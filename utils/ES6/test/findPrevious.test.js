import findPrevious from '../findPrevious.js';

describe('findPrevious', () => {
	//test object and array
	//but set context should sort most
	const valuesArray = ['a', 'b', 'c', 'd', 'e', 'f'];
	const singleArray = ['a'];
	const doubleArray = ['a', 'b'];

	const valuesObj = {
		a: 'a', b: 'b', c: 'c', d: 'd', e: 'e', f: 'f'
	};
	const singleObj = {a: 'a'};
	const doubleObj = {a: 'a', b: 'b'};

	it('should return undefined if no oiteree is supplied', () => {
		expect(findPrevious()).to.be.undefined;
	});

	it('should return undefined if there is only one element to be indexed', () => {
		expect(findPrevious(singleArray)).to.be.undefined;
		expect(findPrevious(singleObj)).to.be.undefined;
	});

	it('should return the prev index if there are two elements to be indexed', () => {
		expect(findPrevious(doubleArray, 'a')).to.equal('b');
		expect(findPrevious(doubleObj, 'a')).to.equal('b');
	});

	it('should return the first index if the second index is supplied', () => {
		expect(findPrevious(valuesArray, 'b')).to.equal('a');
		expect(findPrevious(valuesObj, 'b')).to.equal('a');
	});

	it('should return the last index if the first index is supplied', () => {
		expect(findPrevious(valuesArray, 'a')).to.equal('f');
		expect(findPrevious(valuesObj, 'a')).to.equal('f');
	});
});
