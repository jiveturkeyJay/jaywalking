import omit from '../omit.js';

describe('Omit', () => {
	let obj;
	beforeEach(() => {
		obj = {
			a: 'a',
			b: 'b',
			c: 'c',
			d: {
				a: 'da',
				da: 'da-a'
			},
			e: 'e'
		};
	});

	afterEach(() => {
		obj = null;
	});

	it('should return undefined if no object is supplied', () => {
		expect(omit()).to.be.undefined;
	});

	it('should return undefined if no oiteratee is supplied', () => {
		expect(omit({})).to.be.undefined;
	});

	it('should return an object with the specified keys omitted', () => {
		const result = {
			b: 'b',
			c: 'c',
			e: 'e'
		};

		expect(omit(obj, ['a', 'd'])).to.eql(result);
	});

	it('should return an empty object when all keys are removed', () => {
		expect(omit(obj, ['a', 'b', 'c', 'd', 'e'])).to.eql({});
	});
});
