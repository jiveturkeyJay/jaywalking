import first from '../first.js';

describe('first', () => {
	it('should return the first element in the array', () => {
		const arr = [1, 2, 3, 4, 5];
		expect(first(arr)).to.eql(1);
	});

	it('returns undefined if nothing supplied', () => {
		expect(first([])).to.be.undefined;
		expect(first({})).to.be.undefined;
		expect(first()).to.be.undefined;
		expect(first(123)).to.be.undefined;
	});

	it('should return the first character of string', () => {
		expect(first('abcd')).to.equal('a');
	});

	it('should return the first character of an empty', () => {
		expect(first('')).to.equal('');
	});
});
