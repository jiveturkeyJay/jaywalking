import isString from '../isString.js';

describe('isString', () => {
	it('should return false if not a string', () => {
		expect(isString([])).to.be.false;
		expect(isString({})).to.be.false;
		expect(isString(1)).to.be.false;
	});

	it('should return true if is a string', () => {
		expect(isString('a')).to.be.true;
	});
});
