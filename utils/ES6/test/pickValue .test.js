import pickValue from '../pickValue.js';

describe('pickValue', () => {
	let obj;
	beforeEach(() => {
		obj = {
			a: 'a',
			b: 'b',
			c: 'c',
			d: {
				a: 'da',
				da: 'da-a'
			},
			e: 'e'
		};
	});

	afterEach(() => {
		obj = null;
	});

	it('should return undefined if no obj if not defined', () => {
		expect(pickValue()).to.be.undefined;
	});

	it('should return false if no picker', () => {
		expect(pickValue(obj)).to.be.undefined;
	});

	it('should return prop value', () => {
		expect(pickValue(obj, 'd')).to.equal(obj.d);
	});
});
