import last from '../last.js';

describe('Last', () => {
	let numberArr;
	let compactArr;
	let primitives;
	let falsey;
	let combined;

	beforeEach(() => {
		numberArr = [1, 2, 3, 4, 5];
		compactArr = [1, 0, false, '', 2, undefined];
		primitives = [true, false, undefined, null, 'abc', 1];
		// we add these together so we can test a lot of things
		falsey = [false, undefined, null, '', 0];
		combined = [].concat(compactArr, primitives, falsey);
	});

	it('should return undefined if an array is not supplied', () => {
		// loops through the values so various things supplied
		combined.forEach((item) => {
			expect(last(item)).to.be.undefined;
		});
	});

	it('should return undefined if an empty array', () => {
		expect(last([])).to.be.undefined;
	});

	it('should return the last element in the array', () => {
		expect(last(numberArr)).to.equal(5);
	});

	it('should return the only element if only one', () => {
		expect(last([1])).to.equal(1);
	});
});
