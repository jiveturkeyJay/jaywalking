import partial from '../partial.js';

describe('partial', () => {
	const fn1 = (x, y) => x + y;
	const fn2 = (a, b, c) => a + b + c;
	let p;

	beforeEach(() => {
		p = partial(fn1, 3);
	});

	it('should return a function', () => {
		expect(typeof p).to.equal('function');
	});

	it('should preload first argument', () => {
		expect(p(4)).to.equal(7);
	});

	it('should work with more than one argument', () => {
		p = partial(fn2, 1, 2);
		expect(p(3)).to.equal(6);
	});
});
