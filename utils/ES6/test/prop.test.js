import prop from '../prop.js';
import compose from '../compose.js';
import isFunction from '../isFunction.js';

describe('prop', () => {
	const user = {
		email: 'james@example.com',
		accountDetails: {
			address: {
				street: '123 Fake St',
				city: 'Exampleville',
				province: 'NS',
				postcode: '1234'
			}
		},
		preferences: {}
	};

	it('should return a function', () => {
		expect(isFunction(prop('bob'))).to.be.true;
	});

	it('should return undefined if the supplied object does not have the prop', () => {
		expect(prop('bob')({})).to.be.undefined;
	});

	it('should return undefined if the an object is not supplied', () => {
		expect(prop('bob')([])).to.be.undefined;
	});

	it('should return object with object has prop', () => {
		expect(prop('email')(user)).to.equal('james@example.com');
	});

	describe('composing prop', () => {
		it('should compose and work properly', () => {
			const composed = compose(prop('postcode'), prop('address'), prop('accountDetails'));
			const result = composed(user);
			expect(result).to.equal('1234');
		});
	});
});
