import reduceObj from '../reduceObj.js';

describe('reduceObj', () => {
	let supplier;
	let returned;

	beforeEach(() => {
		supplier = {
			one: 'one',
			two: 'two',
			three: 'three'
		};
	});

	afterEach(() => {
		supplier = null;
		returned = null;
	});

	it('should return a new object with just the required keys add', () => {
		returned = reduceObj(supplier, 'one', 'three');
		expect(returned).to.eql({
			one: 'one',
			three: 'three'
		});
	});

	it('should add a prop of false when the key does not exist on the supplier object', () => {
		returned = reduceObj(supplier, 'one', 'three', 'four');
		expect(returned).to.eql({
			one: 'one',
			three: 'three',
			four: false
		});
	});

	it('should not modify the original object', () => {
		expect(supplier).to.eql(supplier);
	});
});
