import initial from '../initial.js';

describe('Initial', () => {
	let numberArr;
	let compactArr;
	let primitives;
	let falsey;
	let combined;

	beforeEach(() => {
		numberArr = [1, 2, 3, 4, 5];
		compactArr = [1, 0, false, '', 2, undefined];
		primitives = [true, false, undefined, null, 'abc', 1];
		//we add these together so we can test a lot of things
		falsey = [false, undefined, null, '', 0];
		combined = [].concat(compactArr, primitives, falsey);
	});

	it('should return undefined if an array is not supplied', () => {
		combined.forEach((item) => {
			expect(initial(item)).to.be.undefined;
		});
	});

	it('should return an empty array if an empty array is supplied', () => {
		expect(initial([])).to.be.undefined;
	});

	it('should remove the last element in the array', () => {
		expect(initial(numberArr)).to.eql([1, 2, 3, 4]);
	});

	it('should return an empty array if one only element in the array', () => {
		expect(initial([1])).to.eql([]);
	});
});
