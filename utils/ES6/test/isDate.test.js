import isDate from '../isDate.js';

describe('isDate', () => {
  it('should return false when not a date', () => {
    ['a', 1, () => {}, new Set(), null, false].forEach((x) => {
      expect(isDate(x)).to.be.false;
    });
  });

  it('should return true when a date', () => {
    const date = new Date();
    expect(isDate(date)).to.be.true;
  });
});
