import isNumber from '../isNumber.js';

describe('isNumber', () => {
	it('should return false if not a number', () => {
		expect(isNumber([])).to.be.false;
		expect(isNumber({})).to.be.false;
		expect(isNumber('a')).to.be.false;
	});

	it('should return true if is a number', () => {
		expect(isNumber(1)).to.be.true;
		expect(isNumber(NaN)).to.be.true;
	});
});
