import flattenObj from '../flattenObj.js';

describe('flattenObj', () => {
	//test directly and the array one
	//do this to a depth of 3,4,5
	//when the one of the nested objects is empty
	//when the outer one is empty
	describe('flattenObj', () => {
		let nested;
		let result;

		beforeEach(() => {
			nested = {
				a: 'a',
				b: 'b',
				c: {ca: 'ca'},
				d: 'd'
			};
		});

		afterEach(() => {
			nested = null;
		});

		it('should return an empty object if an empty object is supplied', () => {
			expect(flattenObj({})).to.eql({});
		});

		it('should flatten an object with a nested object', () => {
			nested.c = {ca: 'ca'};

			result = {
				a: 'a', b: 'b', ca: 'ca', d: 'd'
			};
			expect(flattenObj(nested)).to.eql(result);
		});

		it('should flatten an double nested object', () => {
			nested.c = {
				ca: 'ca',
				cb: {
					cba: 'cba',
					cbb: 'cbb'
				}
			};

			result = {
				a: 'a', b: 'b', ca: 'ca', cba: 'cba', cbb: 'cbb', d: 'd'
			};
			expect(flattenObj(nested)).to.eql(result);
		});

		it('should flatten a triple nested object', () => {
			nested.c = {
				ca: 'ca',
				cb: {
					cba: 'cba',
					cbb: 'cbb',
					cbc: {
						cbca: 'cbca',
						cbcb: 'cbcb'
					}
				}
			};

			result = {
				a: 'a', b: 'b', ca: 'ca', cba: 'cba', cbb: 'cbb', cbca: 'cbca', cbcb: 'cbcb', d: 'd'
			};
			expect(flattenObj(nested)).to.eql(result);
		});

		it('should not have a prop when a nested object is empty', () => {
			nested.c = {
				ca: 'ca',
				cb: {
					cba: 'cba',
					cbb: 'cbb',
					cbc: {}
				}
			};

			result = {
				a: 'a', b: 'b', ca: 'ca', cba: 'cba', cbb: 'cbb', d: 'd'
			};
			expect(flattenObj(nested)).to.eql(result);
		});
	});
});
