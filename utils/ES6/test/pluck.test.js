import pluck from '../pluck.js';
import map from '../map.js';

describe('Pluck - get named key from object', () => {
	let simple;

	beforeEach(() => {
		simple = [{
			name: 'bob'
		}, {
			name: 'bob1'
		}, {
			name: 'bob2'
		}];
	});

	const expected = ['bob', 'bob1', 'bob2'];
	it('should get a named key from an object', () => {
		expect(pluck('name', simple[0])).to.equal('bob');
	});

	it('should work fine on an array', () => {
		map(simple, (item, i) => {
			expect(pluck('name', item)).to.equal(expected[i]);
		});
	});

	it('should return undefined if a key does not exist', () => {
		expect(pluck('blah', simple[0])).to.be.undefined;
	});

	it('should return undefined is an object is not supplied', () => {
		expect(pluck('blah')).to.be.undefined;
	});
});
