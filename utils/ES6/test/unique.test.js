import unique from '../unique.js';

describe('Unique', () => {
	let duplicates;
	let numberArr;
	beforeEach(() => {
		duplicates = [1, 2, 1, 3, 4, 3, 5, 6];
		numberArr = [1, 2, 3, 4, 5];
	});

	it('should remove all duplicates and return and array', () => {
		const expected = [1, 2, 3, 4, 5, 6];
		expect(unique(duplicates)).to.eql(expected);
	});

	it('should return a copy of the original array if none are duplicates', () => {
		const returned = unique(numberArr);
		expect(returned).to.eql(numberArr);
		expect(returned === numberArr).to.be.false;
	});
});
