import pick from '../pick.js';

describe('Pick', () => {
	let obj;
	beforeEach(() => {
		obj = {
			a: 'a',
			b: 'b',
			c: 'c',
			d: {
				a: 'da',
				da: 'da-a'
			},
			e: 'e'
		};
	});

	afterEach(() => {
		obj = null;
	});

	it('should return an empty object if no object is supplied', () => {
		expect(Object.keys(pick())).to.have.lengthOf(0);
	});

	it('should return an empty object if no oiteratee is supplied', () => {
		expect(Object.keys(pick({}))).to.have.lengthOf(0);
	});

	it('should add keys to an existing object using an object for the oitereee', () => {
		const odds = [null, false, undefined, NaN, '', 0, new RegExp(), new Date()];
		odds.forEach((odd) => {
			expect(Object.keys(pick({}, odd))).to.have.lengthOf(0);
		});
	});

	it('should return a object with the correct keys', () => {
		const picked = pick(obj, {a: 'a', b: 'a'});
		const result = {
			a: 'a',
			b: 'b'
		};

		expect(picked).to.eql(result);
	});

	it('should add keys to an existing object using an array is used as oitereee', () => {
		const picked = pick(obj, ['a', 'b']);
		const result = {
			a: 'a',
			b: 'b'
		};

		expect(picked).to.eql(result);
	});

	it('should add keys to existing object using an array and object is used for the oitereee', () => {
		const picked = pick(obj, ['a', 'b', 'c'], {z: 'z'});
		const result = {
			a: 'a',
			b: 'b',
			c: 'c',
			z: 'z'
		};

		expect(picked).to.eql(result);
	});
});
