import rest from '../rest.js';

describe('Rest', () => {
	let numberArr;

	beforeEach(() => {
		numberArr = [1, 2, 3, 4, 5];
	});

	it('should return a copy of the array if 0 index', () => {
		expect(rest(numberArr, 0)).to.eql([1, 2, 3, 4, 5]);
	});

	it('should return a shallow copy if 0 index', () => {
		const slicedArr = rest(numberArr, 0);
		expect(slicedArr).to.eql([1, 2, 3, 4, 5]);
	});

	it('should default to remove first ', () => {
		//expect(_.rest(numberArr)).to.eql([2, 3, 4, 5]);
		expect(rest(numberArr)).to.eql([2, 3, 4, 5]);
	});

	it('should convert arguments to an array', () => {
		//arrow func has a fit
		const arr = rest(1, 2, 3);
		expect(Array.isArray(arr)).to.be.true;
	});
});
