import isDeep from '../isDeep.js';

describe('isDeep', () => {
	it('should return false when it is not deep ie . separated', () => {
		expect(isDeep('abc')).to.be.false;
	});

	it('should return true when it is not deep ie . separated', () => {
		expect(isDeep('a.b.c')).to.be.true;
	});
});
