import partialRight from '../partialRight.js';

describe('partialRight', () => {
	const fn1 = (x, y) => x + y;
	const fn2 = (a, b, c) => a + b + c;
	let pRight;
	beforeEach(() => {
		pRight = partialRight(fn1, 3);
	});

	it('should return a function', () => {
		expect(typeof pRight).to.equal('function');
	});

	it('should preload first argument', () => {
		expect(pRight(4)).to.equal(7);
	});

	it('should work with more than one argument', () => {
		pRight = partialRight(fn2, 1, 2);
		expect(pRight(3)).to.equal(6);
	});
});
