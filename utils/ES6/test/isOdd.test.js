import isOdd from '../isOdd.js';

describe('isOdd', () => {
	it('should return false for non-numbers', () => {
		const nonNumbers = [[], {}, false, true, undefined, ''];

		nonNumbers.forEach((i) => {
			expect(isOdd(i)).to.be.false;
		});
	});

	it('should return true for all floats', () => {
		const floats = [2.2, 0.22, 0.0022];

		floats.forEach((i) => {
			expect(isOdd(i)).to.be.true;
		});
	});

	it('should return true for odd numbers', () => {
		const odds = [1, 3, 13, 213, -3];

		odds.forEach((i) => {
			expect(isOdd(i)).to.be.true;
		});
	});

	it('should return true for even numbers', () => {
		const evens = [0, 2, 4, 10, 112, -2];

		evens.forEach((i) => {
			expect(isOdd(i)).to.be.false;
		});
	});
});
