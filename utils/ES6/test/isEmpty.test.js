import isEmpty from '../isEmpty.js';

//set map weakset
describe('isEmpty', () => {
  it('should return true if arguments has length', () => {
    let args;

    function setArgs() {
      args = arguments;
    }

    args = setArgs('1', '2');
    expect(isEmpty(args)).to.be.false;
    args = setArgs();
    // args are undefined in this case
    expect(isEmpty(args)).to.be.false;
  });

  it('should return true if object has keys', () => {
    expect(isEmpty({ prop: 'yes' })).to.be.false;
  });

  it('should return false if array has no elements', () => {
    expect(isEmpty([1])).to.be.false;
  });

  it('should return false if a string has length', () => {
    expect(isEmpty('a')).to.be.false;
  });

  it('should return false if x is a number', () => {
    expect(isEmpty(0)).to.be.false;
  });

  it('should return false if object is null', () => {
    expect(isEmpty(null)).to.be.false;
  });

  it('should return false if object is undefined', () => {
    expect(isEmpty(undefined)).to.be.false;
  });

  it('should return false when Map has entries', () => {
    const map = new Map();
    map.set('bob', 'arrr');
    expect(isEmpty(map)).to.be.false;
  });

  it('should return false when Set has entries', () => {
    const set = new Set();
    set.add('bob', 'arrr');

    expect(isEmpty(set)).to.be.false;
  });

  it('should return true if object has no keys', () => {
    expect(isEmpty({})).to.be.true;
  });

  it('should return true if array has no elements', () => {
    expect(isEmpty([])).to.be.true;
  });

  it('should return true if a string has no length', () => {
    expect(isEmpty('')).to.be.true;
  });

  it('should return true when Map has no entries', () => {
    const map = new Map();
    expect(isEmpty(map)).to.be.true;
  });

  it('should return true when Set has no entries', () => {
    const set = new Set();
    expect(isEmpty(set)).to.be.true;
  });
});
