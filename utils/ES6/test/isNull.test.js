import isNull from '../isNull.js';

describe('isNull', () => {
  it('should return false when not null', () => {
    ['a', 1, () => {}, new Set(), new Date(), false].forEach((x) => {
      expect(isNull(x)).to.be.false;
    });
  });

  it('should return true when it is null', () => {
    expect(isNull(null)).to.be.true;
  });
});
