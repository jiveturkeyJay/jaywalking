/* eslint-disable no-shadow */
import objEntries from '../objEntries.js';

describe('objEntries', () => {
	const data = {
		1: 'a',
		2: 'b',
		3: 'c'
	};

	const keys = Object.keys(data);
	const values = keys.map((key) => data[key]);

	it('should return undefined if no obj supplied', () => {
		expect(objEntries()).to.be.undefined;
	});

	it('should return [] if supplied arg is an empty array', () => {
		expect(objEntries([])).to.eql([]);
	});

	it('should return [] if supplied arg is not an empty array', () => {
		expect(objEntries([1, 2, 3])).to.eql([['0', 1], ['1', 2], ['2', 3]]);
	});

	it('should return the information correctly', () => {
		const result = objEntries(data);

		keys.forEach((key, i) => {
			const curr = result[i];
			expect(Array.isArray(curr)).to.be.true;
			expect(curr[0]).to.equal(`${i + 1}`);
			expect(curr[1]).to.equal(values[i]);
		});
	});
});
