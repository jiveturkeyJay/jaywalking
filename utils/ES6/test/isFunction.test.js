import isFunction from '../isFunction.js';

describe('isFunction', () => {
	//non objects
	let fn;
	const objects = [[], 1, 'string', null, false, undefined, new Date(), {}, new RegExp()];

	it('should return true if it is a named function', () => {
		expect(isFunction(() => {})).to.be.true;
	});

	it('should return true if it is a function expression', () => {
		fn = function () {};
		expect(isFunction(fn)).to.be.true;
	});

	it('should return true if it is an arrow function', () => {
		expect(isFunction(() => {})).to.be.true;
	});

	it('should return false if not a function', () => {
		objects.forEach((item) => {
			expect(isFunction(item)).to.be.false;
		});
	});
});
