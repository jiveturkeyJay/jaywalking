import always from '../always.js';

describe('always', () => {
	it('should return a function', () => {
		expect(typeof always('Same')).to.equal('function');
	});

	it('should always return the same result', () => {
		const alwaysfn = always('Same');

		expect(alwaysfn()).to.equal('Same');
		expect(alwaysfn()).to.equal('Same');
	});
});
