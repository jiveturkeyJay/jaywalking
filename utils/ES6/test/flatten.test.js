// //can flatten an array or object
//
// import flatten from '../flatten.js';
//
// xdescribe('Flatten method', () => {
//     let data;
//     let arr;
//     let flattend;
//     const primitives = [true, false, undefined, null, 'abc', 1];
//     let copiedPhotoData;
//
//     beforeEach(() => {
//         data = {
//             b: 'b',
//             a: 'a',
//             bob: {sex1: 'm', age1: '0', a: 'nestedBlah'},
//             bob1: {sex2: 'm', age1: '1'},
//             bob2: {sex3: 'm'}
//         };
//
//         arr = [1, 2, 3, [4, 5, 6, 7, [8, 9, 10]], 11, 12];
//         copiedPhotoData = JSON.parse(JSON.stringify(photoData));
//     });
//
//     afterEach(() => {
//         data = null;
//         arr = null;
//         flattend = null;
//         copiedPhotoData = null;
//     });
//
//     it('should return the original object if not an array or object', () => {
//         primitives.forEach((prim) => {
//             expect(flatten(prim)).to.eql(prim);
//         });
//     });
//
//     it('should flatten the data correctly', () => {
//         flattend = flatten(data);
//         expect(flattend).to.eql({
//             sex1: 'm', a: 'nestedBlah', sex2: 'm', age1: '1', sex3: 'm', b: 'b'
//         });
//     });
//
//     it('should flatten an array correctly', () => {
//         flattend = flatten(arr);
//         expect(flattend).to.eql([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]);
//     });
//
//     it('should flatten nested objects correctly', () => {
//         const keys = ['id',
//             'owner',
//             'secret',
//             'server',
//             'farm',
//             'title',
//             'ispublic',
//             '_content',
//             'isfriend',
//             'isfamily',
//             'datetaken',
//             'datetakengranularity',
//             'datetakenunknown',
//             'tags',
//             'url_sq',
//             'height_sq',
//             'width_sq',
//             'url_s',
//             'height_s',
//             'width_s', 'url_m', 'height_m', 'width_m', 'url_n', 'height_n', 'width_n', 'url_z', 'height_z', 'width_z', 'url_c', 'height_c', 'width_c', 'url'];
//         flattend = flatten(copiedPhotoData[0]);
//
//         keys.forEach((key) => {
//             expect(flattend[key]).not.to.be.undefined;
//         });
//
//         expect(flattend.description).to.be.undefined;
//     });
// });
