import isUndefined from '../isUndefined.js';

describe('isUndefined', () => {
  it('should return false when not undefined', () => {
    ['a', 1, () => {}, new Set(), new Date(), false, null].forEach((x) => {
      expect(isUndefined(x)).to.be.false;
    });
  });

  it('should return true when it is undefined', () => {
    expect(isUndefined(undefined)).to.be.true;
  });
});
