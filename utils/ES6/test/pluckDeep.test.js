import pluckDeep from '../pluckDeep.js';

/**
 * todo test for using array
 */
describe('Monad/Pluckdeep', () => {
	//a way of getting a deep prop using a monad
	const obj = {
		prop1: 'prop1Value',
		prop2: {
			prop1Level2: 'prop1Level2Value',
			prop2Level2: 'prop2Level2Value',
			prop3Level2: 'prop3Level2Value',
			prop4Level2: {
				prop1Level3: 'prop1Level3Value'
			}
		}
	};

	const deepPath = 'prop2.prop4Level2.prop1Level3';
	const deepPathError = 'prop6.prop4Level2.prop1Level3';
	//should be able to handle an array or a string
	// it('should split it correctly when called with a string', () => {
	//     const splitSpy = sinon.spy(_ES6, 'split');
	//     pluckDeep(deepPath, obj);
	//     expect(splitSpy.calledOnce).to.be.true;
	//     splitSpy.resetHistory();
	// });

	it('should return undefined if the prop does not exist', () => {
		expect(pluckDeep(deepPathError, obj)).to.be.undefined;
	});

	it('should return the value if it exists', () => {
		expect(pluckDeep(deepPath, obj)).to.equal('prop1Level3Value');
	});
});
