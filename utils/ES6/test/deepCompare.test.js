// import { deepCompare } from '../deepCompare';
//
// describe('deepCompare', () => {
//     // not exhaustive list of types checkable using Reflect
//     const types = [
//         'string',
//         8,
//         new Set(),
//         Symbol(),
//         new Date(),
//         new Map(),
//         new RegExp(/w/),
//         null,
//         (x) => x,
//     ];
//
//     it('should return false when objects are not the same type', () => {
//         const obj1 = { a: 1 };
//         types.forEach((type) => {
//             expect(deepCompare(obj1, type)).to.equal(false);
//         });
//     });
//
//     it('should return false when arguments are not valid types', () => {
//         types.forEach((type) => {
//             expect(deepCompare(type, type)).to.equal(false);
//         });
//     });
//
//     // simple case array
//     it('should return false when two arrays are NOT the same', () => {
//         expect(deepCompare([1, 2, 3], [1, 2, 4])).to.equal(false);
//     });
//
//     it('should return true when two arrays are the same', () => {
//         expect(deepCompare([1, 2, 3], [1, 2, 3])).to.equal(true);
//     });
//
//     // simple object
//     it('should return false when two simple objects are NOT the same', () => {
//         expect(deepCompare({a: 1, b: 2}, {a: 1, b: 3})).to.equal(false);
//     });
//
//     it('should return true when two simple objects are the same', () => {
//         expect(deepCompare({a: 1, b: 2}, {a: 1, b: 2})).to.equal(true);
//     });
//
//     // array of objects
// });
