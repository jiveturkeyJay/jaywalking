import indexBy from '../indexBy.js';

describe('IndexBy', () => {
	let indexed;
	let complex;
	beforeEach(() => {
		complex = [{
			name: 'bob',
			age: '0',
			sex: 'm'
		}, {
			name: 'bob1',
			age: '1',
			sex: 'm'
		}, {
			name: 'bob2',
			sex: 'm'
		}];
		indexed = indexBy(complex, 'name');
	});

	it('returns undefined if no arguments', () => {
		expect(indexBy()).to.be.undefined;
	});

	it('returns undefined if an empty object is supplied', () => {
		expect(indexBy({})).to.be.undefined;
	});

	it('should return an object instead of an array', () => {
		expect(typeof indexed).to.equal('object');
	});

	it('should have the correct amount of keys', () => {
		expect(Object.keys(indexed)).to.have.lengthOf(3);
	});

	it('should have the correct keys', () => {
		expect(Object.keys(indexed)).to.eql(['bob', 'bob1', 'bob2']);
	});

	it('should have the correct created object', () => {
		const expected = {
			bob: {name: 'bob', age: '0', sex: 'm'},
			bob1: {name: 'bob1', age: '1', sex: 'm'},
			bob2: {name: 'bob2', sex: 'm'}
		};

		expect(indexed).to.eql(expected);
	});

	it('should not add key to new object if the key is not present in the original', () => {
		indexed = indexBy(complex, 'age');
		expect(Object.keys(indexed)).to.eql(['0', '1']);
	});

	it('should use a supplied object if supplied', () => {
		indexed = indexBy(complex, 'name', {blah: {}});
		expect(Object.keys(indexed)).to.eql(['blah', 'bob', 'bob1', 'bob2']);
	});
});
