import isNotEmpty from '../isNotEmpty.js';

describe('isNotEmpty', () => {
	it('should return true if object has keys', () => {
		expect(isNotEmpty({
			prop: 'yes'
		})).to.be.true;
	});

	it('should return true if array has no elements', () => {
		expect(isNotEmpty([1])).to.be.true;
	});

	it('should return true if a string has no length', () => {
		expect(isNotEmpty('a')).to.be.true;
	});

	it('should return true if x is a number', () => {
		expect(isNotEmpty(0)).to.be.true;
	});

	it('should return true if object is null', () => {
		expect(isNotEmpty(null)).to.be.true;
	});

	it('should return true if object is undefined', () => {
		expect(isNotEmpty(undefined)).to.be.true;
	});

	it('should return true when Map has entries', () => {
		const map = new Map();
		map.set('bob', 'arrr');

		expect(isNotEmpty(map)).to.be.true;
	});

	it('should return false if object has no keys', () => {
		expect(isNotEmpty({})).to.be.false;
	});

	it('should return false if array has no elements', () => {
		expect(isNotEmpty([])).to.be.false;
	});

	it('should return false if a string has no length', () => {
		expect(isNotEmpty('')).to.be.false;
	});

	it('should return false when Map has no entries', () => {
		const map = new Map();
		expect(isNotEmpty(map)).to.be.false;
	});
});
