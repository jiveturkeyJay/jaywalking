import truthy from '../truthy.js';

describe('truthy', () => {
	const valid = [1, 'a', {}, [], 0, true, ''];
	const invalid = [undefined, false, null];

	it('should return true for values that are valid', () => {
		valid.forEach((v) => {
			expect(truthy(v)).to.be.true;
		});
	});

	it('should return false for values that are invalid', () => {
		invalid.forEach((v) => {
			expect(truthy(v)).to.be.false;
		});
	});
});
