import setContext from '../setContext.js';

/*
* cannot make a nodelist or classlist DomTokenList
* so cannot test them
*
* */
describe('setContext', () => {
	//internal function to turn an obj or array into a iterable
	//ie an array
	//can deal with
	it('should return undefined if nothing is supplied', () => {
		expect(setContext()).to.eql(undefined);
	});

	describe('Falsey values', () => {
		it('should return an array with an empty string when called with an empty string', () => {
			expect(setContext('')).to.eql(['']);
		});

		it('should return an array with false when called with false', () => {
			expect(setContext(null)).to.eql([null]);
		});

		it('should return an array with undefined when called with undefined', () => {
			expect(setContext(undefined)).to.eql([undefined]);
		});
	});

	describe('Boolean', () => {
		it('should return an array with false when called with false', () => {
			expect(setContext(false)).to.eql([false]);
		});

		it('should return an array with true when called with true', () => {
			expect(setContext(true)).to.eql([true]);
		});
	});


	describe('Numbers', () => {
		it('should return an array with a sinole item when called with NaN', () => {
			expect(setContext(NaN)).to.eql([NaN]);
		});

		it('should return an array with a sinole item when called with a number', () => {
			expect(setContext(0)).to.eql([0]);
			expect(setContext(10)).to.eql([10]);
		});
	});

	describe('Strings', () => {
		//note empty string is done in falsey values
		it('should return an array with a sinole item when called with a string', () => {
			expect(setContext('abc')).to.eql(['abc']);
		});
	});

	describe('Sets, maps and objects', () => {
		it('should convert a Set into an array', () => {
			expect(setContext(new Set(['1', 2, 3]))).to.eql(['1', 2, 3]);
		});


		it('should convert a Map into an array', () => {
			const map = new Map();
			map.set(1, 1);
			map.set(2, 2);
			map.set(3, 3);
			expect(setContext(map)).to.eql([1, 2, 3]);
		});

		//or should it be an indexBy?? check with underscore
		it('should return an array of the objects keys', () => {
			const obj = {
				a: 'a',
				b: 'b',
				c: 'c'
			};

			expect(setContext(obj)).to.eql(Object.keys(obj));
		});
	});

	describe('Date', () => {
		it('should return an array with single date item', () => {
			const date = new Date();
			const returned = setContext(date);
			expect(Array.isArray(returned)).to.be.true;
			expect(toString.call(returned[0])).to.equal('[object Date]');
		});
	});

	describe('Array', () => {
		it('should return the array', () => {
			expect(setContext([1, 2, 3])).to.eql([1, 2, 3]);
		});
	});
});
