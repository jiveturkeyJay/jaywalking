import renameProp from '../renameProp.js';

describe('renameProp', () => {
	const obj = {
		a: 'a',
		b: 'b',
		c: 'c',
		d: {
			aa: 'aa',
			bb: 'bb'
		}
	};

	it('should rename prop on a a shallow level', () => {
		const result = renameProp(obj, 'a', 'e');
		expect(result.e).to.equal('a');
	});

	it('should work on a deep object and set it on the top level', () => {
		const result = renameProp(obj, 'd.bb', 'e');
		expect(result.e).to.equal('bb');
	});

	it('does not change the original object/returns a new object', () => {
		renameProp(obj, 'd.bb', 'e');
		expect(obj).to.eql(obj);
	});
});
