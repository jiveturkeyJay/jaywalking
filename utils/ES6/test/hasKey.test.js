import hasKey from '../hasKey.js';

describe('hasKey', () => {
	const obj = {
		a: 'blah'
	};

	it('should return true if object has own property key', () => {
		expect(hasKey(obj, 'a')).to.be.true;
	});

	it('should return false if object has own property key', () => {
		expect(hasKey(obj, 'b')).to.be.false;
	});

	it('should return false if object has default property', () => {
		expect(hasKey(obj, 'toString')).to.be.false;
	});

	//check for bad practised
	it('should return true if a prototype method is over written OOPS', () => {
		obj.toString = 'blah';
		expect(hasKey(obj, 'toString')).to.be.true;
	});
});
