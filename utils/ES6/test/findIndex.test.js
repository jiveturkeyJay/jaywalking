import findIndex from '../findIndex.js';

describe('Find index', () => {
	let complex;
	beforeEach(() => {
		complex = [{
			name: 'bob',
			age: '0',
			sex: 'm'
		}, {
			name: 'bob1',
			age: '1',
			sex: 'm'
		}, {
			name: 'bob2',
			sex: 'm'
		}];
	});

	it('should return index', () => {
		expect(findIndex(complex, {
			name: 'bob2'
		})).to.equal(2);
	});

	it('should return false if no index', () => {
		expect(findIndex(complex, {
			name: 'bladfjdsl'
		})).to.equal(-1);
	});

	it('should return index when multiple props to check', () => {
		expect(findIndex(complex, {
			name: 'bob1',
			sex: 'm'
		})).to.equal(1);
	});
});
