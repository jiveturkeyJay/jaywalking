import evaluator from '../evaluator.js';

describe('Evaluator', () => {
	//tests for nodeList?
	let falsey;
	let prop;
	const fn = (arg) => typeof arg === 'string';
	const bool = (arg) => typeof arg === 'boolean';

	function runEvaluator(arg, cb) {
		return evaluator(arg, cb);
	}

	beforeEach(() => {
		falsey = [false, undefined, null, '', 0];
	});

	afterEach(() => {
		prop = null;
	});

	describe('No callback supplied', () => {
		describe('Errors', () => {
			it('should return false if an object with a false value', () => {
				prop = {a: false};
				expect(runEvaluator(prop)).to.be.false;
			});

			it('should return false if an object with any false values', () => {
				prop = {a: true, b: 'safjklds', c: false};
				expect(runEvaluator(prop)).to.be.false;
			});

			it('should return false if an empty string is passed', () => {
				prop = '';
				expect(runEvaluator(prop)).to.be.false;
			});

			it('should return false if an empty array is passed', () => {
				prop = [];
				expect(runEvaluator(prop)).to.be.false;
			});

			it('should return false if any array with a falsey value is passed', () => {
				prop = [1, 2, ''];
				expect(runEvaluator(prop)).to.be.false;
			});

			it('should return false if any of the values do not pass and an boolean (false) is passed', () => {
				prop = false;
				expect(runEvaluator(prop)).to.be.false;
			});

			it('should return false when a falsey value is passed', () => {
				falsey.forEach((p) => {
					expect(runEvaluator(p)).to.be.false;
				});
			});
		});

		describe('Errors (complex)', () => {
			it('should test nested objects to one level', () => {
				prop = {
					a: true,
					b: 'asdkfljads',
					c: 1,
					d: {
						d: false
					}
				};

				expect(runEvaluator(prop)).to.be.false;
			});

			it('should test nested objects to two levels level', () => {
				prop = {
					a: true,
					b: 'asdkfljads',
					c: 1,
					d: {
						d: true,
						da: {
							daa: false
						}
					}
				};

				expect(runEvaluator(prop)).to.be.false;
			});
		});

		//need more complicated
		describe('Correct (simple)', () => {
			it('should return true  and an object is passed', () => {
				prop = {a: true, b: 'asdkfljads', c: 1};
				expect(runEvaluator(prop)).to.be.true;
			});

			it('should return true if a string is passed', () => {
				prop = 'a string';
				expect(runEvaluator(prop)).to.be.true;
			});

			it('should return true if all the values of an array pass', () => {
				prop = ['one', 'two', 'three'];
				expect(runEvaluator(prop)).to.be.true;
			});

			it('should return true if a boolean (true) is passed', () => {
				prop = true;
				expect(runEvaluator(prop)).to.be.true;
			});

			it('should return true when a symbol is supplied', () => {
				prop = Symbol('arrr');
				expect(runEvaluator(prop)).to.be.true;
			});
		});

		describe('Correct (complicated)', () => {
			it('should test nested objects', () => {
				//need a -ve test for this
				prop = {
					a: true,
					b: 'asdkfljads',
					c: 1,
					d: {
						d: 'a'
					}
				};

				expect(runEvaluator(prop)).to.be.true;
			});

			it('should test nested objects to two levels level', () => {
				prop = {
					a: true,
					b: 'asdkfljads',
					c: 1,
					d: {
						d: true,
						da: {
							daa: true
						}
					}
				};

				expect(runEvaluator(prop)).to.be.true;
			});
		});
	});

	describe('Callback supplied', () => {
		describe('Errors', () => {
			it('should return false if any of the values do not pass and an object is passed', () => {
				prop = {a: false};
				expect(runEvaluator(prop, fn)).to.be.false;
			});

			it('should return false if any of the values do not pass and an array is passed', () => {
				prop = [1, 2, false];
				expect(runEvaluator(prop, fn)).to.be.false;
			});

			it('should return false if any of the values do not pass and an boolean (false) is passed', () => {
				prop = false;
				expect(runEvaluator(prop, fn)).to.be.false;
			});

			it('should return false if any of the values do not pass and a falsey value is passed is passed', () => {
				//cannot use falsey array here as typeof '' is string
				[false, undefined, null, 0].forEach((p) => {
					expect(runEvaluator(p, fn)).to.be.false;
				});
			});

			it('should return false when a symbol is supplied', () => {
				prop = Symbol('arrr');
				expect(runEvaluator(prop, fn)).to.be.false;
			});
		});

		describe('Correct', () => {
			it('should return true when a prop is falsey but the callback returns true', () => {
				prop = false;
				expect(runEvaluator(prop, bool)).to.be.true;
			});

			it('should return true for a string', () => {
				prop = 'a string';
				expect(runEvaluator(prop, fn)).to.be.true;
			});

			it('should return true for a sn array', () => {
				prop = ['one', 'two', 'three'];
				expect(runEvaluator(prop, fn)).to.be.true;
			});

			it('should return true for boolean true', () => {
				prop = true;
				expect(runEvaluator(prop, bool)).to.be.true;
			});

			//sanity check
			it('should return true when a nested object passes', () => {
				prop = {
					a: {
						bb: 'str',
						b: {
							c: 'somestring'
						}
					},
					cc: 'str'
				};

				expect(runEvaluator(prop, fn)).to.be.true;
			});
		});
	});
});
