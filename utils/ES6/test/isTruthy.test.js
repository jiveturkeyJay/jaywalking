import isTruthy from '../isTruthy.js';

const falsey = [false, undefined, null, '', 0];

describe('isTruthy', () => {
	it('should return false when a falsey value supplied', () => {
		falsey.forEach((i) => {
			expect(isTruthy(i)).to.be.false;
		});
	});

	it('should be true when a true value is supplied', () => {
		[1, true, [2]].forEach((i) => {
			expect(isTruthy(i)).to.be.true;
		});
	});
});
