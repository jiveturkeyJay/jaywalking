import isInt from '../isInt.js';

describe('isInt', () => {
	it('should return true when something other than an interger is supplied', () => {
		// null return null
		['a', '1', 1.1, -1.1, {}, [], [1], true, false].forEach((int) => {
			expect(isInt(int)).to.be.false;
		});
	});

	it('should return true when an interger is supplied', () => {
		[1, 100, -2].forEach((int) => {
			expect(isInt(int)).to.be.true;
		});
	});
});
