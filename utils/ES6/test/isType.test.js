import isType from '../isType.js';

describe('isType', () => {
	it('returns false if type matches', () => {
		expect(isType([], 'string')).to.be.false;
		expect(isType({}, 'string')).to.be.false;
		expect(isType('a', 'number')).to.be.false;
		expect(isType(1, 'string')).to.be.false;
		expect(isType(Symbol('bob'), 'string')).to.be.false;
	});

	it('returns true if type matches', () => {
		expect(isType([], 'array')).to.be.true;
		expect(isType({}, 'object')).to.be.true;
		expect(isType('a', 'string')).to.be.true;
		expect(isType(1, 'number')).to.be.true;
		expect(isType(Symbol('bob'), 'symbol')).to.be.true;
	});
});
