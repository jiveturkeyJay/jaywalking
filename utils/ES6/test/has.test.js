/* eslint-disable no-shadow */
/* eslint-disable  no-unused-vars */
import has from '../has.js';

//symbols
//https://hacks.mozilla.org/2015/06/es6-in-depth-symbols/
describe('has', () => {
	const obj = {a: 'blah0', c: 'blah'};

	it('should return false if no prop is supplied', () => {
		expect(has(obj)).to.be.false;
	});

	it('it should return false if the object does not have the property', () => {
		expect(has(obj, 'b')).to.be.false;
	});

	it('should return false if the prop is a falsey value', () => {
		expect(has(obj, false)).to.be.false;
		expect(has(obj, undefined)).to.be.false;
		expect(has(obj, 0)).to.be.false;
		expect(has(obj, '')).to.be.false;
	});

	it('it should return true if the object has the property', () => {
		expect(has(obj, 'a')).to.be.true;
	});
});
