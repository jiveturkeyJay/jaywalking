import compact from '../compact.js';

describe('Compact - remove falsey values from an array', () => {
	let compactArr;
	let numberArr;
	let falsey;

	beforeEach(() => {
		compactArr = [1, 0, false, '', 2, undefined];
		numberArr = [1, 2, 3, 4, 5];
		falsey = [false, undefined, null, '', 0];
	});

	it('should remove all falsey values', () => {
		expect(compact(compactArr)).to.eql([1, 2]);
	});

	it('should return copy of the original array if no falsey values', () => {
		const returned = compact(numberArr);
		expect(returned).to.eql(numberArr);
		expect(returned === numberArr).to.be.false;
	});

	it('should remove all false values', () => {
		const compacted = compact(falsey);
		expect(compacted).to.have.lengthOf(0);
	});
});
