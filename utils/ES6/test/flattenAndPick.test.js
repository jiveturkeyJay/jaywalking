import flattenAndPick from '../flattenAndPick.js';

describe('flattenAndPick', () => {
	let nested;
	let compactArr;
	let primitives;
	let falsey;
	let combined;

	beforeEach(() => {
		compactArr = [1, 0, false, '', 2, undefined];
		primitives = [true, false, undefined, null, 'abc', 1];
		// we add these together so we can test a lot of things
		falsey = [false, undefined, null, '', 0];
		combined = [].concat(compactArr, primitives, falsey);

		nested = {
			a: 'a',
			b: 'b',
			c: {ca: 'ca'},
			d: 'd',
			e: {
				ea: 'ea'
			}
		};
	});

	afterEach(() => {
		nested = null;
	});

	it('should return undefined if the data is not an object', () => {
		combined.forEach((item) => {
			expect(flattenAndPick(item)).to.be.undefined;
		});
	});

	it('should just return a flattend object if no keys to pick', () => {
		const result = {
			a: 'a',
			b: 'b',
			ca: 'ca',
			d: 'd',
			ea: 'ea'
		};

		expect(flattenAndPick(nested)).to.eql(result);
	});

	it('should return an object with an array of keys added', () => {
		const result = {
			a: 'a',
			ca: 'ca'
		};

		expect(flattenAndPick(nested, ['a', 'ca'])).to.eql(result);
	});

	it('should return an object if an object with keys is supplied', () => {
		const result = {
			a: 'a',
			ca: 'ca'
		};
		//pick will check if an array
		expect(flattenAndPick(nested, {a: true, ca: true})).to.eql(result);
	});

	it('should return an object if an str of key is supplied', () => {
		const result = {
			a: 'a'
		};
		//pick will check if an array
		expect(flattenAndPick(nested, 'a')).to.eql(result);
	});
});
