import isEven from '../isEven.js';

describe('isEven', () => {
	it('should return false for non-numbers', () => {
		const nonNumbers = [[], {}, false, true, undefined, ''];

		nonNumbers.forEach((i) => {
			expect(isEven(i)).to.be.false;
		});
	});

	it('should return false for all floats', () => {
		const floats = [-1.1, 2.2, 0.22, 0.0022];

		floats.forEach((i) => {
			expect(isEven(i)).to.be.false;
		});
	});

	it('should return false for odd numbers', () => {
		const odds = [1, 3, 13, 213];

		odds.forEach((i) => {
			expect(isEven(i)).to.be.false;
		});
	});

	it('should return true for even numbers', () => {
		const evens = [0, 2, 4, 10, 112, -2];

		evens.forEach((i) => {
			expect(isEven(i)).to.be.true;
		});
	});
});
