import values from '../values.js';

describe('values', () => {
	// only test the errors as otherwise we are returning
	// object.values() so native method
	it('should return false if no object supplied', () => {
		expect(values()).to.be.undefined;
		expect(values([])).to.be.undefined;
		expect(values(1)).to.be.undefined;
		expect(values('a')).to.be.undefined;
	});
});
