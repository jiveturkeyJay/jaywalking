import isMatch from '../isMatch.js';

describe('isMatch', () => {
	const base = {
		prop1: 'prop1',
		prop2: 'prop2',
		prop3: 'prop3'
	};

	it('should return false if the objects do not have the same key', () => {
		const toCheck = {prop4: 'prop'};
		expect(isMatch(base, toCheck)).to.be.false;
	});

	it('should return false if the objects do not have the same value', () => {
		const toCheck = {prop1: 'prop'};
		expect(isMatch(base, toCheck)).to.be.false;
	});

	it('should return true if objects have same key with same value', () => {
		const toCheck = {prop1: 'prop1'};
		expect(isMatch(base, toCheck)).to.be.true;
	});

	it('should return true if all the props are NOT the same', () => {
		const toCheck = {
			prop1: 'prop1',
			prop2: 'prop1'
		};
		expect(isMatch(base, toCheck)).to.be.false;
	});

	it('should return true if all the props are the same', () => {
		const toCheck = {
			prop1: 'prop1',
			prop2: 'prop2'
		};
		expect(isMatch(base, toCheck)).to.be.true;
	});

	it('returns false when a deep object is checked and no match found', () => {
		const deep = {
			prop1: 'prop1',
			prop2: 'prop2',
			prop3: {
				prop3a: 'prop3a'
			}
		};

		const toCheck = {
			prop1: 'prop1',
			prop3: {
				prop3a: 'prop3a'
			}
		};

		expect(isMatch(deep, toCheck)).to.be.false;
	});

	it('returns true when a deep object is checked and match found', () => {
		const deep = {
			prop1: 'prop1',
			prop2: 'prop2',
			prop3: {
				prop3a: 'prop3a'
			}
		};

		const toCheck = {
			prop1: 'prop1',
			prop2: 'prop2'
		};

		expect(isMatch(deep, toCheck)).to.be.true;
	});
});
