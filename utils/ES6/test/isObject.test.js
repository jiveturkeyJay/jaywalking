import isObject from '../isObject.js';

describe('isObject', () => {
	const objects = [[], 1, 'string', null, false, undefined, new Date(), () => {}, new RegExp()];

	it('should return true if it is an object', () => {
		expect(isObject({})).to.be.true;
	});

	it('should return false if not an object', () => {
		objects.forEach((item) => {
			expect(isObject(item)).to.be.false;
		});
	});
});
