import assign from '../assign.js';

describe('assign', () => {
	const obj1 = {a: '1'};
	const obj2 = {b: '2'};
	const obj3 = {c: '3'};

	it('should work on a single level', () => {
		expect(assign({}, obj1)).to.eql(obj1);
	});

	it('should work with 2 objects to assign', () => {
		expect(assign({}, obj1, obj2)).to.eql({
			a: '1',
			b: '2'
		});
	});

	it('should work with multiple objects', () => {
		expect(assign({}, obj1, obj2, obj3)).to.eql({
			a: '1',
			b: '2',
			c: '3'
		});
	});
});
