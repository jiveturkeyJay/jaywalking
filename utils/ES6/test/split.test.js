/* eslint-disable no-shadow */
/* eslint-disable  no-unused-vars */
import split from '../split.js';

describe('split', () => {
	let baseString;

	beforeEach(() => {
		baseString = 'abc.efg.hij.klm';
	});

	afterEach(() => {
		baseString = null;
	});

	it('should return an array', () => {
		expect(Array.isArray(split(baseString))).to.be.true;
	});

	it('should return an split using the default separator', () => {
		expect(split(baseString)).to.have.lengthOf(4);
	});

	it('should use a sepeerate if supplied', () => {
		expect(split(baseString.replace(/\./ig, '/'), '/')).to.have.lengthOf(4);
	});

	it('should return a string if no match', () => {
		const str = 'abcdef';
		expect(split(str, '/')).to.have.lengthOf(1);
	});

	it('should return what if no string', () => {
		expect(split({})).to.have.lengthOf(1);
	});
});
