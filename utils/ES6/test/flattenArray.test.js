import flattenArray from '../flattenArray.js';

describe('flattenArray', () => {
	it('should return an empty array if an empty array is supplied', () => {
		expect(flattenArray([])).to.eql([]);
	});

	it('should flatten a nested array', () => {
		expect(flattenArray([1, 2, 3, [4], 5])).to.eql([1, 2, 3, 4, 5]);
	});

	it('should flatten an array with more than one nested array', () => {
		expect(flattenArray([1, 2, 3, [4], 5, [6, 7]])).to.eql([1, 2, 3, 4, 5, 6, 7]);
	});

	it('should flatten a double nested array', () => {
		expect(flattenArray([1, 2, 3, [4, ['a', 'b']], 5])).to.eql([1, 2, 3, 4, 'a', 'b', 5]);
	});

	it('should flatten a double nested array with more than one double nested array', () => {
		expect(flattenArray([1, 2, ['z', ['z', 'y']], 3, [4, ['a', 'b']], 5])).to.eql([1, 2, 'z', 'z', 'y', 3, 4, 'a', 'b', 5]);
	});

	it('should flatten a triple nested array', () => {
		expect(flattenArray([1, 2, 3, [4, ['a', ['la', 'ba'], 'b']], 5])).to.eql([1, 2, 3, 4, 'a', 'la', 'ba', 'b', 5]);
	});
});
