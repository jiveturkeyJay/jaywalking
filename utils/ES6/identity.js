/**
 *
 * function that always returns is first argument
 * @param {*} x - anything
 * @returns {*} returns input
 */
const identity = x => x;

export default identity;
