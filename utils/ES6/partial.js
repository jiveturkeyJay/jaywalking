/**
 * @param {function} fn
 * @param {argumenst} args
 * @returns {function(...[*]=)} a partially applicated function
 */
const partial = (fn, ...args) => (...remainingArgs) => fn.apply(this, args.concat(remainingArgs));

export default partial;
