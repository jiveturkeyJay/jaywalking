import isMatch from './isMatch.js';

/**
 * takes an array of object ands returns the index
 * of the object that contains a match
 * if no match then returns false
 * eg var objs = [{id: 1}, {id: 2}, {id: 3}]
 * var match = {id: 2};
 * findIndex(objs, match) => 2
 *
 * @param {array} array
 * @param {string} match
 * @returns {boolean}
 */
const findIndex = (array, match) => array.findIndex((obj) => isMatch(obj, match) === true);

export default findIndex;
