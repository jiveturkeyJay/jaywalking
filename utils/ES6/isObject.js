import isType from './isType.js';

const isObject = (elem) => isType(elem, 'Object');

export default isObject;
