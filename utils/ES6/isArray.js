// no tests
const isArray = (arr) => Array.isArray(arr);

export default isArray;
