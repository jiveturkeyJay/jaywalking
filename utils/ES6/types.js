//not exhaustive
//all html element will return HTML
//Map and Set
const types = {
	'[object Date]': 'Date',
	'[object String]': 'String',
	'[object Number]': 'Number',
	'[object Boolean]': 'Boolean',
	'[object Function]': 'Function',
	'[object Symbol]': 'Symbol',
	'[object Object]': 'Object',
	'[object Null]': 'Null',
	'[object Math]': 'Math',
	'[object Undefined]': 'Undefined',
	'[object Array]': 'Array',
	'[object Window]': 'Window',
	'[object NodeList]': 'NodeList',
	'[object DOMTokenList]': 'DOMTokenList',
	'[object DocumentFragment]': 'DocumentFragment',
	'[object Map]': 'Map',
	'[object Set]': 'Set',
	'[object RegExp]': 'RegExp'
};

export default types;
