import isDeep from './isDeep.js';
import prop from './prop.js';
import split from './split.js';
import compose from './compose.js';
import getType from './getType.js';
import { Maybe } from './monads.js';

const pluckDeep = (path, obj) => {
  const type = getType(path);
  let pathArr;

  // could just return pick here
  if (type === 'String' && isDeep(path)) {
    pathArr = split(path);
  } else {
    pathArr = [path];
  }

  if (type === 'Array') {
    pathArr = path;
  }

  const mapped = pathArr.reverse().map((str) => prop(str));
  // Maybe is over kill
  // return composed(obj) || undefined
  const maybe = Maybe.of(obj).map(compose(...mapped));
  return maybe.__value;
};

export default pluckDeep;
