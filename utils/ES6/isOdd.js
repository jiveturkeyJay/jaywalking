/**
 * need check for .99
 * odd numbers are any number which cannot be divided into 2
 * @param {number} x
 * @returns {boolean} is x a number or not
 */
const isOdd = (x) =>
/*eslint-disable */
     x == parseFloat(x) && !!(x % 2);
    /*eslint-enable */


export default isOdd;
