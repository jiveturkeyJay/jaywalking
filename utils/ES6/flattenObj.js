/* eslint-disable no-param-reassign */
import keys from './keys.js';
import isObject from './isObject.js';

/**
 *
 * @param {object} obj
 * @returns {object} a new object with a flat structure
 */
const flattenObj = (obj) => {
	const flattend = keys(obj).reduce((acc, prop) => {
		const objProp = obj[prop];
		if (isObject(objProp)) {
			const flattedProp = flattenObj(objProp);
			keys(flattedProp)
				.reduce((innerAcc, innerProp) => {
					innerAcc[innerProp] = flattedProp[innerProp];
					return innerAcc;
				}, acc);
		} else {
			acc[prop] = objProp;
		}

		return acc;
	}, {});

	return flattend;
};

export default flattenObj;
