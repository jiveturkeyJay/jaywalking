/* eslint-disable no-param-reassign */
import isArray from './isArray.js';
import existy from './existy.js';

/**
 * function to take an array of objects create an object using a key from
 * the original object
 * eg [{id: 1, ...}, {id: 2}] -> {1: {id: 1, ..., 2: {id: 2, ...}}}
 *  - this is for faster lookup rather than loop through an array
 *
 * @param {array} dataSource - an array of object
 * @param {string} propToSet - prop to set on new object
 * @param {function} cb - a callback function
 * @returns {*|{}}
 *
 * //check were we use this, should be able to compose the function to do the flattenAndPick
 * //plus use indexBy  and a reduce
 */
const normaliseArrayToObject = (data, propToSet, cb) =>
  existy(data) && isArray(data)
    ? data.reduce((ini, item) => {
        //should check that we have the prop??
        //so our callback can pick out the keys required
        ini[item[propToSet]] = cb ? cb(item) : item;
        return ini;
      }, {})
    : {};

export default normaliseArrayToObject;
