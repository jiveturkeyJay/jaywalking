import isObject from './isObject.js';

/**
 * Takes and object and returns value only shallow one depth
 * @param {Object} obj - an object
 * @returns {array} array of obj value
 */
const values = (obj) => {
	return isObject(obj) ? Object.values(obj) : undefined;
};

export default values;
