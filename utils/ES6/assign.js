/**
 * object assign - does not do deep merge
 * @param {object} reciever
 * @param {object} toadd
 * @returns {*} object with new data added
 */
const assign = (reciever, ...toadd) => Object.assign(reciever, ...toadd);

export default assign;
