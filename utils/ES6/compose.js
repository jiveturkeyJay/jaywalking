/**
 * remember compose runs all the functions one after another
 * @param {array} an array of functions
 * @returns {function} a compomsed function
 */
const compose = (...fns) => fns.reduce((f, g) => (...args) => f(g(...args)));

export default compose;
