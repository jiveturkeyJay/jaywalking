import setContext from './setContext.js';
import reduce from './reduce.js';
import has from './has.js';
import isObject from './isObject.js';

/**
 * could have flatPick or normal pick, pick and place
 * @param {Object} obj original object
 * @param {array|object} oiteratee - array of keys or an obj ::could be a str or a number??
 * @param {Object} o - optional object to add results to
 * @returns {Object} - create or supplied object is returned (might be empty)
 */
const pick = (obj, oiteratee, o) => {
	//simple versions only
	const r = (o && isObject(o)) ? o : {};
	if (!obj || !oiteratee) {
		return r;
	}

	const keys = setContext(oiteratee);

	return reduce(keys, (acc, key) => {
		if (has(obj, key)) {
			Reflect.set(acc, key, Reflect.get(obj, key));
		}
		return acc;
	}, r);
};

export default pick;
