//https://github.com/cht8687/You-Dont-Need-Lodash-Underscore
//https://www.reindex.io/blog/you-might-not-need-underscore/

import always from './always.js';
import assign from './assign.js';
import compact from './compact.js';
import compose from './compose.js';
import curry from './curry.js';
import evaluator from './evaluator.js';
import existy from './existy.js';
import filter from './filter.js';
import first from './first.js';
import findIndex from './findIndex.js';
import findPrevious from './findPrevious.js';
import findNext from './findNext.js';
import flatten from './flatten.js';
import flattenAndPick from './flattenAndPick.js';
import flattenArray from './flattenArray.js';
import flattenObj from './flattenObj.js';
import getType from './getType.js';
import has from './has.js';
import hasKey from './hasKey.js';
import indexBy from './indexBy.js';
import initial from './initial.js';
import isArray from './isArray.js';
import isDeep from './isDeep.js';
import identity from './identity.js';
import isEmpty from './isEmpty.js';
import isEven from './isEven.js';
import isFunction from './isFunction.js';
import isInt from './isInt.js';
import isMatch from './isMatch.js';
import isNotEmpty from './isNotEmpty.js';
import isNumber from './isNumber.js';
import isOdd from './isOdd.js';
import isObject from './isObject.js';
import isString from './isString.js';
import isTruthy from './isTruthy.js';
import isType from './isType.js';
import keys from './keys.js';
import last from './last.js';
import map from './map.js';
import normaliseArrayToObject from './normaliseArrayToObject.js';
import omit from './omit.js';
import parseStrToInt from './parseStrToInt.js';
import partial from './partial.js';
import partialRight from './partialRight.js';
import pick from './pick.js';
import pickValue from './pickValue.js';
import prop from './prop.js';
import pluck from './pluck.js';
import pluckDeep from './pluckDeep.js';
import reduce from './reduce.js';
import renameProp from './renameProp.js';
import reduceObj from './reduceObj.js';
import rest from './rest.js';
import split from './split.js';
import setContext from './setContext.js';
import truthy from './truthy.js';
import unique from './unique.js';
import values from './values.js';

export {
	always,
	assign,
	compose,
	compact,
	curry,
	evaluator,
	existy,
	filter,
	first,
	findIndex,
	findNext,
	findPrevious,
	flatten,
	flattenAndPick,
	flattenArray,
	flattenObj,
	getType,
	has,
	hasKey,
	indexBy,
	initial,
	isArray,
	isDeep,
	identity,
	isEven,
	isEmpty,
	isFunction,
	isType,
	isInt,
	isMatch,
	isNotEmpty,
	isNumber,
	isObject,
	isOdd,
	isString,
	isTruthy,
	keys,
	last,
	map,
	normaliseArrayToObject,
	omit,
	parseStrToInt,
	partialRight,
	partial,
	pick,
	pluckDeep,
	pickValue,
	pluck,
	prop,
	reduce,
	reduceObj,
	renameProp,
	rest,
	setContext,
	split,
	truthy,
	values,
	unique
};
