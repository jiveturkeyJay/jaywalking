/* eslint-disable max-len */
//taken from https://gist.github.com/nathggns/fb97cd9018454bfa87c1
const curry = (fn, ...args) => (args.length === fn.length ? fn(...args) : curry.bind(this, fn, ...args));

export default curry;
