import isNumber from './isNumber.js';

/**
 *  -2 etc will return -0 which is 0
 * checks if a number is a number and is even
 * @param {number} x
 * @returns {boolean} is x a number or not
 */
const isEven = (x) => (isNumber(x) ? x % 2 === 0 : false);

export default isEven;
