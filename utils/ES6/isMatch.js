/* eslint-disable max-len */
import keys from './keys.js';
import hasKey from './hasKey.js';

/**
 * in _
 * Tells if the keys and values in properties are contain in object
 * @param {obj} obj
 * @param {obj} match
 * @returns {boolean} - whether the object contains the key and value is the same
 */
const isMatch = (obj, match) => keys(match).every((key) => (hasKey(obj, key) && obj[key] === match[key]));

export default isMatch;
