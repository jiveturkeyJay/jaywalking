import isType from './isType.js';

const isString = (elem) => isType(elem, 'String');

export default isString;
