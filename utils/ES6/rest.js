//returns an array
//can be used to turn args to an
//return the rest of the elements in an array
//should be able to return to array from
//not much bettr than slice
//still by reference for objects
import {slice} from './es6helpers.js';

/**
 *
 * @param {array} arr
 * @param {number} n
 * @returns {array} - returns an array
 */
const rest = (arr, n = 1) => [].concat(Reflect.apply(slice, arr, [n]));

export default rest;
