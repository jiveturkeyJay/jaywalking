import has from './has.js';

/**
 * simple function to return an objects prop value
 * eg var obj = {
 *  a: [],
 *  b: 'blah'
 * }
 *  getPick(obj, 'b') -> 'blah'
 *
 * @param {Object } obj
 * @param {String} toPick - a string name of prop to pick from object
 * @returns {undefined|Object} - undefined or an object
 */
const pickValue = (obj, toPick) => {
	if (!obj || !toPick) {
		return;
	}

	return has(obj, toPick) ? Reflect.get(obj, toPick) : obj;
};

export default pickValue;
