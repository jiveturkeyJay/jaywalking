/*
    NOT tested
 use this instead of Reflect.ownKeys
 ownKeys is like Object.getOwnPropertyNames and returns enumerable or not
 */
const keys = (obj) => Object.keys(obj);
export default keys;
