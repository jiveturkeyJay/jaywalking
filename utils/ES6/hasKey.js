import {hasOwnProp} from './es6helpers.js';

/**
 * NOTE ...key is required as using apply
 * @param {object} obj
 * @param {string} key a string representation of the key
 * @returns {boolean} whether contains the key or not
 */
const hasKey = (obj, ...key) => Reflect.apply(hasOwnProp, obj, key);

export default hasKey;
