/**
    returns a function that always returns same value
    also known as identity in some places
 */
const always = (x) => () => x;

export default always;
