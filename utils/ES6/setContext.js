/* eslint-disable no-undef */
import getType from './getType.js';
import keys from './keys.js';

/**
 * takes an oiteratee and changes it into an array based on type
 * don't typecast or anything, as we only need to make the return an array
 * so it can work in a loop - eg so a callback and be invoked on the returned value
 * @param {Object|array|string|symbol} oiteratee - anything really
 * @returns {Array|undefined} - an array or bool
 */
const setContext = function (oiteratee) {
  //convert object etc to a string
  //NAN
  //check for arguments otherwise we get [undefined] returned
  // DO NOT GET SUCKERED BY LINTING - WE NEED TO CHECK ARGUMENTS
  if (!arguments.length) {
    return;
  }

  //todo check what happens when an nothing is passed in
  // currently returns [undefined] but not sure how
  // might be something to do with the tests
  //this needs to be beefed up as an empty string will be put into an array as ['']
  //we don't want to do with window
  // new Number() when toStringed is Number
  // new String() when toStringed is a String
  //possible problems with this as we returning the same object
  //might mutate
  //or should that be the responsiblity of the next step;
  const type = getType(oiteratee);
  let returnValue;
  switch (type) {
    case 'Array': {
      returnValue = oiteratee;
      break;
    }
    //this is problematic
    case 'Object': {
      returnValue = keys(oiteratee);
      break;
    }
    case 'String':
    case 'Symbol':
    case 'Boolean':
    case 'Null':
    case 'Function':
    case 'Number':
    case 'Undefined':
    case 'RegExp':
    case 'Date': {
      returnValue = [oiteratee];
      break;
    }
    case 'Set':
    case 'NodeList':
    case 'DomTokenList': {
      returnValue = [...oiteratee];
      break;
    }
    case 'Map': {
      returnValue = [...oiteratee.keys()];
      break;
    }
    default: {
      returnValue = undefined;
      break;
    }
  }

  return returnValue;
};

export default setContext;
