import pluckDeep from './pluckDeep.js';

/**
 * function that takes a prop from an object and re-adds it on the same object
 * @param obj
 * @param prop
 * @param newName
 */
const renameProp = (obj, prop, newName) => ({
  ...obj,
  [newName]: pluckDeep(prop, obj)
});

export default renameProp;
