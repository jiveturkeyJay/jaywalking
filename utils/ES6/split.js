/**
 * Split a string(native) and return an array
 *
 * @param {string} str - a string
 * @param {string} separator - to split on OPTIONAL
 * @returns {Array} Str
 */
const split = (str, separator = '.') => {
	//check if string?
	//clone str?
	const Str = `${str}`;
	return Str.split(separator);
};

export default split;
