/* eslint-disable max-len */
/**
 * @param {function} fn
 * @param {argument} partiallyAppliedArgs
 * @returns {function(...[*]=)} a partially applicated function
 */
const partialRight = (fn, ...partiallyAppliedArgs) => (...remainingArgs) => fn.apply(this, remainingArgs.concat(partiallyAppliedArgs));

export default partialRight;
