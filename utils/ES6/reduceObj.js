import reduce from './reduce.js';
import has from './has.js';

/**
 *  takes an object and create a new one with the required keys
 *  if no value adds in as false??
 * @param {object} obj object to operate on
 * @param {array} keys array of keys to retain
 * @returns {object} a new object with only the required keys
 */
const reduceObj = (obj, ...keys) => reduce(keys, (acc, key) => {
	acc[key] = has(obj, key) ? obj[key] : false;
	return acc;
}, {});

export default reduceObj;
