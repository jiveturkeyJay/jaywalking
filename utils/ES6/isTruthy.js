const isTruthy = (o) => !!o;

export default isTruthy;
