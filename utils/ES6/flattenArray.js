import isArray from './isArray.js';
import flatten from './flatten.js';

const flattenArray = (arr) => (isArray(arr) ? [].concat(...arr.map(flatten.bind(this))) : arr);

export default flattenArray;
