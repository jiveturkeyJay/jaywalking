import isType from './isType.js';

const isNull = (elem) => isType(elem, 'Null');

export default isNull;
