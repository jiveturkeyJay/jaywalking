import keys from './keys.js';
import setContext from './setContext.js';

/**
 * takes an object and array/object and returns object with those keys omitted
 * eg ({1:1, 2:2, 3:3}, ['1']) =>  {2:2, 3:3}
 * @param {Object} obj original object
 * @param {array|object} oiteratee - array of keys or an obj ::could be a str or a number??
 * @param {Object} obj - optional object to add results to
 * @returns {undefined|Object} - create or supplied object is returned (might be empty)
 */
const omit = (obj, oiteratee) => {
	if (!obj || !oiteratee) {
		return;
	}

	const context = setContext(oiteratee);

	return keys(obj)
		.filter((key) => context.indexOf(key) < 0)
		.reduce((acc, key) => {
			acc[key] = obj[key];
			return acc;
		}, {});
};

export default omit;
