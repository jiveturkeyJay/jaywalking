/**
 * function to cast a string to a number if it is a proper (Int)
 * but only return that cast value as a number if it is a string
 * so 1.1 will return 1.1
 * and '1' will return 1
 *
 * - using in a compose function which checks for a number
 *
 * @param {string|number} x
 * @returns {number} a number
 */
const parseStrToInt = (x) => {
	const parsed = parseInt(x, 10);

	if (parsed === x * 1) {
		return parsed;
	}

	return x;
};

export default parseStrToInt;
