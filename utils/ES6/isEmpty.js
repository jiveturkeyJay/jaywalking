import existy from './existy.js';
import keys from './keys.js';
import getType from './getType.js';

/**
 * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/arguments
 *  numbers return false
 *  _ and ramda check for callee but no good with ES6 so need to use object
 *
 *  isEmpty([1, 2, 3]);   //=> false
 *  isEmpty([]);          //=> true
 *  isEmpty('');          //=> true
 *  isEmpty(null);        //=> false
 *  isEmpty({});          //=> true
 *  isEmpty({length: 0}); //=> false
 * @param {object|array|string} x - any value
 * @returns {boolean} - bool
 */
const isEmpty = (x) => {
	if (!existy(x)) { return false; }

	const type = getType(x);
	switch (type) {
	case 'Array':
	case 'String':
		return x.length === 0;
	case 'Object':
		return keys(x).length === 0;
	case 'Set':
	case 'Map':
		return x.size === 0;
	default:
		return x.length === 0;
	}
};


export default isEmpty;
