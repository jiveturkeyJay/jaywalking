import setContext from './setContext.js';

const getContextAndIndex = (oiteratee, value) => {
	const group = setContext(oiteratee);
	const length = group.length;
	const index = group.indexOf(value);

	return {group, length, index};
};

export default getContextAndIndex;
