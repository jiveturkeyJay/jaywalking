import isDeep from './isDeep.js';
import split from './split.js';
// no tests
const baseGet = (obj, key) => {
	//check if index of . -> split string
	//else create an array - might need to improve to check if object
	//baseGet just returns an object to work on
	const path = isDeep(key) ? split(key) : [key];
	let index = 0;
	const {length} = path;
	/*eslint-disable */
    while (obj != null && index < length) {
        obj = obj[path[index++]];
    }
    /*eslint-enable */
	return (index && index === length) ? obj : undefined;
};

export default baseGet;
