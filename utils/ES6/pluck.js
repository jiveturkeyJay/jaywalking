import has from './has.js';

/**
 * tests if object has given key and returns it
 * eg {a: 1, b: 2, c: 3} => pluck('a', obj, ) => 1
 *
 * @param {string} key
 * @param {obj} obj
 * @returns {*|undefined} - what ever the value of the key is
 */
const pluck = (key, obj) => (has(obj, key) ? obj[key] : undefined);

export default pluck;
