import getType from './getType.js';

/**
 * @param {*} elem - an elem
 * @param {string}type - to check against
 * @returns {boolean} - a bool
 */
const isType = (elem, type) => getType(elem).toLowerCase() === type.toLowerCase();

export default isType;
