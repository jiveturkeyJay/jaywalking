import hasKey from './hasKey.js';
import isTruthy from './isTruthy.js';

/**
 * can use Refect has ownKeys - not returns symbols
 * checks if the obj has own prop of key
 * @param {object} obj js object
 * @param {string }key to search for
 * @returns {Boolean} - a boolean
 */
const has = (obj, key) => isTruthy(obj) && hasKey(obj, key);

export default has;
