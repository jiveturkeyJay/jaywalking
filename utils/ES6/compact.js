import filter from './filter.js';

/**
 * removes all falsey values
 * @param {array} arr
 * @returns {array} - an array
 */
const compact = (arr) => filter(arr, (ele) => !!ele);

export default compact;
