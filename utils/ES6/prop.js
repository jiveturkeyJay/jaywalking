import curry from './curry.js';
import pluck from './pluck.js';

/**
 * returns a function which when supplied with an object
 * returns the indicated property
 * //note not correct
 * fn(a) -> fn(b) -> b[a] : undefined
 *
 * @returns {function} - curried function
 */
const prop = curry((p, obj) => pluck(p, obj));

export default prop;
