import isType from './isType.js';

const isDate = (elem) => isType(elem, 'Date');

export default isDate;
