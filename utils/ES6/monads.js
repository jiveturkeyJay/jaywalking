/*eslint-disable */
export class Maybe {
  constructor(value) {
    this.__value = value;
  }

  static of(value) {
    return new Maybe(value);
  }

  isNothing() {
    return this.__value === null || this.__value === undefined;
  }

  map(fn) {
    return this.isNothing() ? Maybe.of(null) : Maybe.of(fn(this.__value));
  }

  join() {
    if (!(this.__value instanceof Maybe)) {
      return this;
    }

    return this.__value.join();
  }

  chain(fn) {
    return this.map(fn).join();
  }

  // this is different from the orElse
  // in that it returns a value
  getOrElse(def) {
    return this.isNothing() ? def : this.__value;
  }

  orElse(def) {
    return this.isNothing() ? Maybe.of(def) : this;
  }

  /**
        to use ap the __value must be a curried function
	 */
  ap(someOtherMaybe) {
    return someOtherMaybe.map(this.__value);
  }

  // we cannot call this toString
  // toString() {
  //     const value = this.__value;
  //     return `The value is ${value}`;
  // }
}

/*eslint-enable */
const monadFactory = (value) => Maybe.of(value);

export default monadFactory;
