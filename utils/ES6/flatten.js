/* eslint-disable no-nested-ternary */
import flattenArray from './flattenArray.js';
import flattenObj from './flattenObj.js';
import isArray from './isArray.js';
import isObject from './isObject.js';

const flatten = obj => (isArray(obj) ? flattenArray(obj) : isObject(obj) ? flattenObj(obj) : obj);

export default flatten;
