import setContext from './setContext.js';
import isObject from './isObject.js';
import values from './values.js';

const getContext = (oiteratee) => (isObject(oiteratee) ? values(oiteratee) : setContext(oiteratee));

export default getContext;
