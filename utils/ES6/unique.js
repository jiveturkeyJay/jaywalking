/**
 * @param {array} arr
 * @returns {array} - removes duplicates from an array
 */
const unique = (arr) => [...new Set(arr)];

export default unique;
