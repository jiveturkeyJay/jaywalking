import isType from './isType.js';

const isFunction = (elem) => isType(elem, 'Function');

export default isFunction;
