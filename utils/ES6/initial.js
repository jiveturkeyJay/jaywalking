import isArray from './isArray.js';

/**
 * returns all the elements in an array except last
 * undefined if not an array
 * @param {array} arr
 * @returns {array|undefined} - undefined if nothing
 */
const initial = (arr) => {
	if (!isArray(arr) || !arr.length) {
		return;
	}
	return arr.slice(0, -1);
};

export default initial;
