import {reIsDeepProp} from './es6helpers.js';


/**
 * checks to see if an object path is deeper than one level
 * eg a.b.c => true a => false
 * @param {string} str
 * @returns {boolean} - is it deep
 */
const isDeep = (str) => reIsDeepProp.test(str);

export default isDeep;
