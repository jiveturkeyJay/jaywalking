/* eslint-disable no-nested-ternary */
import isString from './isString.js';

/**
 *
 * @param {string|array} source
 * @returns {string|number|object} will return what is first in an array or string
 */
const first = (source) => (isString(source) ? source.charAt(0)
	: (source && source[0]) ? source[0] : undefined);

export default first;
