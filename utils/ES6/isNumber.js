import isType from './isType.js';

const isNumber = (elem) => isType(elem, 'Number');

export default isNumber;
