import flatten from './flatten.js';
import isObject from './isObject.js';
import pick from './pick.js';

/**
 * is this useful?
 * takes an object flattens it and returns the keys required
 *
 * @param {Object} obj - array of objects to pick from || an object
 * @param  {array} keysToPick - an array of keys to pick OPTIONAL
 * @returns {Object|undefined} - object
 * //could use ...args on keys to pick to turn to an array
 */
const flattenAndPick = (obj, keysToPick) => {
	if (!isObject(obj)) {
		return;
	}

	if (!keysToPick) {
		return flatten(obj);
	}

	return pick(flatten(obj), keysToPick);
};

export default flattenAndPick;
