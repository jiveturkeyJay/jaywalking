import isArray from './isArray.js';

/**
 * returns the last element in an array
 * @param {array} arr - an array
 * @returns {*} - what ever type the last element is
 */
const last = (arr) => {
	if (!isArray(arr)) {
		return;
	}
	return arr.pop();
};

export default last;
