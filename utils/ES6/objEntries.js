/**
 * returns the object entries
 * eg {
 *  1: '1',
 *  2: '2'
 * } => [['1', '1'], ['2', '2']]
 * @param {object} obj
 * @returns {*} object with new data added
 */
const objEntries = (obj) => obj && Object.entries(obj);

export default objEntries;
