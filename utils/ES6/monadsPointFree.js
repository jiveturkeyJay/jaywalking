/* eslint-disable camelcase */
import {curry} from './index';

/**
 * map :: Not sure how to do this
 * curried function to make a pointfree map
 * takes a function and a functor -> uses the functors.map
 *
 */
export const map = curry((f, any_functor_at_all) => any_functor_at_all.map(f));

/**
 * join :: Monad m => m (m a) -> m a
 * a point free independent Monad join
 * @param {function} f a monad (functor)
 * @returns {object} monad
 */
export const join = (f) => f.join();

//  chain :: Monad m => (a -> m b) -> m a -> m b
// or compose(join, map(f))(m)
/**
 * chain ::
 * map (as above)  and then joins
 */
export const chain = curry((f, m) => m.map(f).join());

export const orElse = curry((def, functor) => functor.join().orElse(def));

export const getValue = (x) => x.__value;
