import isNull from './isNull.js';
import isUndefined from './isUndefined.js';

/**
 * checks if something is not null or undefined
 * so can be useful for falsey values
 * null, undefined => false
 * false, true, '' => true
 *
 * @param {*} x any value
 * @returns {boolean}
 */
const existy = (x) => !(isNull(x) || isUndefined(x));

export default existy;
