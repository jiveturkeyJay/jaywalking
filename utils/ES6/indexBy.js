import reduce from './reduce.js';
import isEmpty from './isEmpty.js';
import has from './has.js';

/**
 * takes an array of objects and returns an object indexed by the key supplied
 * eg [{'id:, 1, name: 'bob'}] => {1 : {'id', 1, name: 'bob'}}
 * why do we loop we should be able to just check if the prop exists
 * @param {array} obj array of objects
 * @param {string} key/prop
 * @param {object} r an object to add to
 * @returns {object|undefined} = a new object with the indexed keys
 */
const indexBy = (obj, key, r = {}) => {
	if (!obj || isEmpty(obj) || !key) {
		return;
	}

	return reduce(obj, (ini, currentObj) => {
		if (has(currentObj, key)) {
			Reflect.set(r, Reflect.get(currentObj, key), currentObj);
		}
		return r;
	}, r);
};

export default indexBy;
