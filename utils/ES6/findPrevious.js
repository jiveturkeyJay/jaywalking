/* eslint-disable max-len */
/* eslint-disable no-nested-ternary */
import getContextAndIndex from './getContextAndIndex.js';

/**
 *
 * @param {array|string|object} oiteratee - pretty much anything that can be put into made into an array
 * @param {string} value string to find
 * @returns {Undefined|number} undefined if no next index or an array of length 1
 */
const findPrevious = (oiteratee, value) => {
	const {group, length, index} = getContextAndIndex(oiteratee, value);
	return (index !== -1 && length >= 2) ? (index === 0) ? group[length - 1] : group[index - 1] : undefined;
};

export default findPrevious;
