const ArrayProto = Array.prototype;
const ObjectProto = Object.prototype;
//returns a shallow copy of the portion
const {slice} = ArrayProto;
const {toString} = Object.prototype;
const hasOwnProp = ObjectProto.hasOwnProperty;
//const split = String.prototype.split;
const strSlice = String.prototype.slice;

/* regex for checking object props */
const reIsDeepProp = /\.|\[(?:[^[\]]*|(["'])(?:(?!\1)[^\\]|\\.)*?\1)\]/;
const reIsPlainProp = /^\w*$/;
const reLeadingDot = /^\./;
const rePropName = /[^.[\]]+|\[(?:(-?\d+(?:\.\d+)?)|(["'])((?:(?!\2)[^\\]|\\.)*?)\2)\]|(?=(?:\.|\[\])(?:\.|\[\]|$))/g;

export {
	ArrayProto,
	ObjectProto,
	hasOwnProp,
	reIsDeepProp,
	reIsPlainProp,
	reLeadingDot,
	rePropName,
	slice,
	strSlice,
	toString
};
