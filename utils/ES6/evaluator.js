import compact from './compact.js';
import isArray from './isArray.js';
import getContext from './getContext.js';
import truthy from './truthy.js';
import isObject from './isObject.js';

/**
 * takes a oiteratee and run though the callback to see if they all pass
 * @param {string|array|obj} oiteratee - anything that can be made into an array
 * @param {function} cb optional callback to pass
 * @returns {Boolean} - true or false
 */
const evaluator = (oiteratee, cb) => {
	let compacted;
	const arr = getContext(oiteratee);
	//if no callback we can quickly compact and check length
	//if does not match then we can return false
	// this is wrong => should produce same result if removed
	if (!cb) {
		compacted = compact(arr);
		if (!compacted.length || compacted.length !== arr.length) {
			return false;
		}
	}

	const method = cb || truthy;
	//need to deal with deep structures
	return arr.every((prop) => {
		if (isObject(prop) || isArray(prop)) {
			return evaluator(prop, method);
		}

		return method(prop);
	});
};

export default evaluator;
