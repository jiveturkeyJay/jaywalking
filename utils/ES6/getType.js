import types from './types.js';

/**
 * applies toString on the elem and checks it against the types object eg so '[object Date]'
 * could have the opposite where we slice and get the end portion
 * @param {*} elem - an elem
 * @returns {string|boolean} a boolean
 */
const getType = (elem) => {
	return types[Reflect.apply(toString, elem, [0, 0])];
};

export default getType;
