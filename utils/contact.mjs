import { unallowedWords, unallowedEmailBits, unallowedNames } from '../constants/contact.mjs';

const checkEmails = (emailAddress) => {
	return unallowedEmailBits.some((bit) => {
		return emailAddress.includes(bit);
	});
};

const checkMessage = (message) => {
	//return message
	const reggie = new RegExp(`(^|\\W)${unallowedWords.join('|')}($|\\W)`, 'i');
	return reggie.test(message)
};

const checkMessageBits = (message) => {
	return ['$', '£'].some((bit) => message.includes(bit))
}

const checkNames = (name) => {
	return unallowedNames.some((unallowName) => name === unallowName)
}

export { checkEmails, checkMessage, checkMessageBits, checkNames }
