const mailformat = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
export const required = {
	yourname: {
		validation: (data) => data && data.length > 0
	},
	emailaddress: {
		validation: (data) => data && data.match(mailformat)
	},
	yourmessage: {
		validation: (data) => data && data.length > 0
	}
};

const requiredKeys = ['yourname', 'emailaddress', 'yourmessage'];

const contactFormErrors = (formData) => {
	return requiredKeys.reduce((acc, key) => {
		const { validation } = required[key];
		const value = Reflect.get(formData, key);

		if (!validation(value)) {
			acc.push(key);
		}

		return acc;
	}, []);
};

export {
	contactFormErrors
};
