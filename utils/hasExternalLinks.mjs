const hasExternalLinks = (data, linkProp = 'link', pathProp = 'path') => {
	return data && data.some((d) => {
		let links = Reflect.get(d, linkProp);
		if (links) {
			return links.some((link) => {
				const value = Reflect.get(link, pathProp);

				return /^(http|s)/.test(value);
			});
		}

		return false;

	});
};

export { hasExternalLinks };
