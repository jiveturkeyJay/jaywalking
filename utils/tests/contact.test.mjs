import { checkEmails, checkMessage, checkMessageBits, checkNames } from "../contact.mjs";
import { unallowedWords } from '../../constants/contact.mjs';

describe('CheckEmails', () => {
	it('should return true if the supplied email contains non-allowed characters', () => {
		expect(checkEmails('bob')).to.be.false;
	});

	it('should return true if the supplied email contains non-allowed characters', () => {
		expect(checkEmails('info.')).to.be.true;
		expect(checkEmails('reply.')).to.be.true;
	});
});

describe('checkMessage', () => {
	it('should return true if the message contains non allowed word', () => {
		unallowedWords.forEach((word) => {
			expect(checkMessage(`${word}`)).to.be.true;
		})
	});

	it('should ignore case when checking', () => {
		unallowedWords.forEach((word) => {
			expect(checkMessage(` ${word.toUpperCase()} `)).to.be.true;
			expect(checkMessage(` ${word.toLowerCase()} `)).to.be.true;
		})
	});

	it('should return true if the message contains non allowed word is in there', () => {
		const message = 'A long message showing something that SEO you know';
		expect(checkMessage(message)).to.be.true;
	});

	it('should return true if the message does contain non allowed word', () => {
		unallowedWords.forEach((word) => {
			expect(checkMessage(word)).to.be.true;
		})
	});

	it('should return true if the message does contain non allowed word is in there', () => {
		const message = 'A long message showing something that SEOyou know';
		expect(checkMessage(message)).to.be.true;
	});

	it('should return false if the message does not contain unallowed words', () => {
		const message = 'A long message showing something that you know';
		expect(checkMessage(message)).to.be.false;
	});
});

describe('checkMessageBits', () => {
	it('should return false if the message does not contain unallowed bits', () => {
		const message = 'A long message showing something that you know';
		expect(checkMessageBits(message)).to.be.false;
	});

	it('should return true if the message does  contain unallowed bits', () => {
		const message = 'A long message showing something that you £know';
		expect(checkMessageBits(message)).to.be.true;
	});
});

describe('checkNames', () => {
	it('should return when an unallowed name is ', () => {
		expect(checkNames('zardoz')).to.be.true;
	});

	it('should return when an unallowed name is ', () => {
		expect(checkNames('zardo')).to.be.false;
	});
});
