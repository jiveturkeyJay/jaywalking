import { hasExternalLinks } from './hasExternalLinks.mjs';

describe('hasExternalLInks', () => {
	it('should return true when there are any https links', () => {
		const data = [
			{
				"type": "text",
				"value": "some text",
				"link": [
					{
						"text": "link text",
						"path": "https://www.ldwa.org.uk"
					},
					{
						"text": "link text",
						"path": "https://www.macadder.net/"
					}
				]
			},
			{
				"type": "text",
				"value": "Some text",
				"link": [
					{
						"text": "link text",
						"path": "https://www.visit1066country.com/"
					}
				]
			}
		];

		const result = (hasExternalLinks(data));
		expect(result).to.equal(true);
	});

	it('should return true when there are any http links', () => {
		const data = [
			{
				"type": "text",
				"value": "some text",
				"link": [
					{
						"text": "link text",
						"path": "http://www.ldwa.org.uk"
					},
					{
						"text": "link text",
						"path": "http://www.macadder.net/"
					}
				]
			},
			{
				"type": "text",
				"value": "Some text",
				"link": [
					{
						"text": "link text",
						"path": "http://www.visit1066country.com/"
					}
				]
			}
		];

		const result = (hasExternalLinks(data));
		expect(result).to.equal(true);
	});

	it('should return true when there are any https links', () => {
		const data = [
			{
				"type": "text",
				"value": "some text",
				"link": [
					{
						"text": "link text",
						"path": "https://www.ldwa.org.uk"
					}
				]
			},
			{
				"type": "text",
				"value": "Some text"
			}
		];

		const result = (hasExternalLinks(data));
		expect(result).to.equal(true);
	});

	it('should return false when there are no https or http links', () => {
		const data = [
			{
				"type": "text",
				"value": "some text",
				"link": [
					{
						"text": "link text",
						"path": "/walks/northdownsway"
					}
				]
			}
		];

		const result = (hasExternalLinks(data));
		expect(result).to.equal(false);
	});

	it('should work with other params', () => {
		const data = [
			{
				"type": "text",
				"value": "some text",
				"values": [
					{
						"text": "link text",
						"href": "https://www.ldwa.org.uk"
					}
				]
			},
			{
				"type": "text",
				"href": "Some text"
			}
		];

		const result = (hasExternalLinks(data, 'values', 'href'));
		expect(result).to.equal(true);
	});

});
