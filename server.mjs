const __dirname = path.resolve(path.dirname(""));
// we can remove the getConfig and just load the .env file
// that is local as long as we don't over-write the one on the server
import fs from "fs";
import Koa from "koa";

import session from 'koa-session';
import { koaBody } from "koa-body";
import render from "koa-ejs";
import path from "path";
import serve from "koa-static";
import favicon from "koa-favicon";
import http from "http";
import helmet from 'koa-helmet';
// import logger from "koa-logger";
import morgan from "koa-morgan";

import { adminRouter, imagesRouter, staticRouter, router } from "./routers/index.mjs";
import { cleanContactSessionData } from './session/index.mjs'

// middleware and helpers
import ejsHelpers from "./views/helpers/index.mjs";

const app = new Koa();
app.keys = [process.env.APP_KEYS];
// if (process.env.NODE_ENV === 'development') {
// 	app.use(logger());
// }

const accessLogStream = fs.createWriteStream(__dirname + '/access.log', { flags: 'a' });
app.use(morgan('combined', { stream: accessLogStream }));

// this should use brotli by default
// app.use(compress({
//   filter(content_type) {
//     return /text/i.test(content_type);
//   },
//   threshold: 2048,
//   gzip: {
//     flush: zlib.constants.Z_SYNC_FLUSH
//   },
//   deflate: {
//     flush: zlib.constants.Z_SYNC_FLUSH
//   },
//   br: {
//     flush: zlib.constants.Z_SYNC_FLUSH
//   }
// }));
app.use(helmet());
app.use(koaBody({
	multipart: true,
	uploadDir: "."
}));

// adds csrf project to routes
app.use((session({}, app)));
//app.use(new CSRF());
app.use(favicon(__dirname + "/public/images/favicon.ico"));
app.use(serve(path.join(__dirname, "./public")));
app.use(serve(path.join(__dirname, "./toupload")));//not used
app.use(serve(path.join(__dirname, "./jsonitems")));

//todo fix this bread crumb stuff
// add array of breadcrumb links
app.use(async (ctx, next) => {
	ctx.state.urlArr = ejsHelpers.splitUrl(ctx.url);
	// need to move this out?
	ctx.state.urlParams = ctx.state.urlArr.length > 0 ? ["titleParam", "verseTitle"] : [];
	await next();
});

// this defaults to a layout.ejs
// we can probably change this
render(app, {
	root: path.join(__dirname, "views"),
	viewExt: "ejs",
	cache: false,
	debug: false,
	rmWhitespace: true
});


app.use(async (ctx, next) => {
	const { formData } = ctx.session;
	if (formData && ctx.originalUrl !== '/contact') {
		ctx.session = cleanContactSessionData(ctx.session)
	}

	await next();
})

// generic error
// not sure if this is used
// is used but needs investigation
//todo fix error error handling
// seems to be on all routes
app.use(async (ctx, next) => {
	// this first bit of the
	try {
		if (ctx.type && ctx.type.length > 0) {
			console.log(ctx.type);
		}
		if (ctx.status === 404) {
			await ctx.render("errPage", {
				pageTitle: "An error has occurred",
				titleParam: "Error",
				seoDescription: "Jaywalking error page"
			});
		}
		// console.log(ctx.status)
		//
		// if (ctx.status > 400) {
		// 	throw new Error({
		// 		status: ctx.status,
		// 		message: 'An error has occurred'
		// 	});
		// } else {
		//
		// }
		await next();
	} catch (err) {
		console.log(`ERROR: ${err.message}`);
		const accessLogStream = fs.createWriteStream(__dirname + '/error.log', { flags: 'a' });
		accessLogStream.write(JSON.stringify(err) + ' app use errors ' + ctx.status + '\n');

		ctx.status = err.status || 500;
		ctx.body = err.message;
		ctx.app.emit("error", err, ctx);
	}
});

app.on("error", (err, ctx) => {
	if (err) {
		const accessLogStream = fs.createWriteStream(__dirname + '/error.log', { flags: 'a' });
		accessLogStream.write(JSON.stringify(err) + ' app on error ' + ctx.status + '\n');

		if (err.status === 404) {
			ctx.status = 302;
			ctx.redirect("/notfound");
		}

		if (err.status === 302) {
			ctx.status = 302;
			ctx.redirect("/302");
		}

		if (err.status === 500) {
			ctx.status = 302;
			ctx.redirect("/500");
		}
	}
});

app.use(staticRouter.routes()).use(staticRouter.allowedMethods());
app.use(adminRouter.routes()).use(adminRouter.allowedMethods());
app.use(imagesRouter.routes()).use(imagesRouter.allowedMethods());
app.use(router.routes()).use(router.allowedMethods());
const server = http.createServer(app.callback());

export {
	server
};
