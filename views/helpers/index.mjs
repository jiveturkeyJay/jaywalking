import { dateOptionsLong, dateOptionsNoDate } from "../../constants/time.mjs";


const dateFormatter = new Intl.DateTimeFormat('en-GB', dateOptionsLong);
const dateFormatterNoDay = new Intl.DateTimeFormat('en-GB', dateOptionsNoDate);

const distanceRatio = 0.62137;
const createDate = (dateStr) => {
	const date = new Date(dateStr);

	return dateFormatter.format(date);
};

const createShortDate = (dateStr) => {
	const date = new Date(dateStr);

	return dateFormatterNoDay.format(date);
};

const distance = (distance) => {
	const ratioed = distance * distanceRatio;
	return `${distance} km or ${ratioed.toFixed(2)} miles`;
};

// this should be moved
/**
 *
 * @param url
 * @return {*[]|*}
 */
const splitUrl = (url) => {
	if (url === "/") {
		return [];
	}

	const breadcrumbArr = url.split("/");
	if (url[0] === "/") {
		breadcrumbArr.shift();
	}
	return breadcrumbArr;
};

/**
 *
 * @param {object} - link
 * @return {string}
 */
const createLink = (link) => {
	const {
		text,
		path,
		classNames
	} = link;
	return classNames ? `<a class="${classNames}" href="${path}">${text}</a>` : `<a href="${path}">${text}</a>`;
};

/**
 * - This actually in the ejs
 * - can use like this <p><%= helpers.getLinks(data) %></p>
 * takes a string and adds html elements based on the data
 * add links round text
 * add abbr
 * @param data
 * @return {*}
 */
const createInlineTags = (data) => {
	const parser = value => JSON.parse(JSON.stringify(value));
	const isArray = arr => Array.isArray(arr);
	let str = parser(data.value);
	const {
		link,
		abbr
	} = data;

	if (link) {
		let newStr = parser(str);
		if (isArray(link)) {

			link.forEach((l) => {
				newStr = newStr.replace(l.text, `<a href="${l.path}" class="${l.className}">${l.text}</a>`);
			});
			str = newStr;
		} else {
			str = newStr.replace(link.text, `<a href="${data.link.path}" class="${link.className}">${link.text}</a>`);
		}
	}

	if (abbr) {
		if (isArray(abbr)) {
			let newStr = parser(str);
			abbr.forEach((a) => {
				newStr = newStr.replace(a.text, `<abbr title="${a.title}">${a.text}</abbr>`);
			});
			str = newStr;
		} else {
			let newStr = parser(str);
			str = newStr.replace(abbr.text, `<abbr title="${abbr.title}">${abbr.text}</abbr>`);
		}
	}

	// if (quote) {
	//     if (isArray(link)) {
	//         let newStr = parser(str);
	//         quote.forEach((q) => {
	//             newStr = newStr.replace(q.text, `<q class="${q.className}">${q.text}</q>`);
	//         });
	//         str = newStr;
	//     }
	//     else {
	//         str =  newStr.replace(quote.text, `<q class="${quote.className}">${quote.text}</q>`);
	//     }
	// }

	return str;
};

const ejsHelpers = {
	createDate,
	createShortDate,
	splitUrl,
	createLink,
	createInlineTags,
	distance
};

export default ejsHelpers;
