import jsHelpers from "./index.mjs";

const {
	createLink,
	createInlineTags
} = jsHelpers;

describe("createLink", () => {
	it("should create a link string", () => {
		const link = createLink({
			path: "/path",
			text: "text"
		});
		expect(typeof link).to.equal("string");
		expect(link).to.equal("<a href=\"/path\">text</a>");
	});

	it("should create a link string with classes", () => {
		const link = createLink({
			path: "/path",
			text: "text",
			classNames: "blah blah2"
		});
		expect(typeof link).to.equal("string");
		expect(link).to.equal("<a class=\"blah blah2\" href=\"/path\">text</a>");
	});
});

describe("createInlineTags", () => {
	let data;
	let abbrData;

	beforeEach(() => {
		data = {
			type: "text",
			value: "Capital Ring and the London Loop",
			link: [{
				text: "Capital Ring",
				path: "https://tfl.gov.uk/modes/walking/capital-ring"
			}, {
				text: "London Loop",
				path: "https://tfl.gov.uk/modes/walking/loop-walk"
			}]
		};
		abbrData = {
			type: "text",
			value: "HTML and CSS are cool",
			abbr: [{
				text: "HTML",
				title: "HyperText Markup Language"
			}, {
				text: "CSS",
				title: "Cascading Style Sheets"
			}]
		};
	});

	it("should wrap text with links where data has an array of link objects", () => {
		const result = createInlineTags(data);
		const expected = '<a href="https://tfl.gov.uk/modes/walking/capital-ring" class="undefined">Capital Ring</a> and the <a href="https://tfl.gov.uk/modes/walking/loop-walk" class="undefined">London Loop</a>';
		expect(result).to.equal(expected);
	});

	it("should wrap text with links where data has a link object", () => {
		const first = data.link[0];
		data.link = first;
		const result = createInlineTags(data);
		const expected = '<a href="https://tfl.gov.uk/modes/walking/capital-ring" class="undefined">Capital Ring</a> and the London Loop';
		expect(result).to.equal(expected);
	});

	it("should wrap text with abbr where data has abbr", () => {
		const result = createInlineTags(abbrData);
		const expected = '<abbr title="HyperText Markup Language">HTML</abbr> and <abbr title="Cascading Style Sheets">CSS</abbr> are cool';
		expect(result).to.equal(expected);
	});
});
