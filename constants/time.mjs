const seconds = 60;
const hour = seconds * 60;
const day = hour * 24;
const week = day * 7;

const dateOptionsLong = {
	weekday: 'long',
	day: 'numeric',
	month: 'long',
	year: 'numeric'
};

const dateOptionsNoDate = {
	day: 'numeric',
	month: 'long',
	year: 'numeric'
};

export {
	seconds,
	hour,
	day,
	week,
	dateOptionsLong,
	dateOptionsNoDate
};
