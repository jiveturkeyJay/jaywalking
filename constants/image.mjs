const imageSizes = {
	large: [1024, 768],
	medium: [640, 480],
	small: [320, 240]
};

const imageSizesName = Object.keys(imageSizes);

const holdingFolder = "public/images/holding";
const createdImagesFolder = "public/images";
const jsonItems = "jsonItems";

export {
	imageSizes,
	imageSizesName,
	createdImagesFolder,
	holdingFolder,
	jsonItems
};
