const walks = {
	pageTitle: "Walks",
	titleParam: "Walks",
	introPara: [
		{
			type: "text",
			value: "Hill walking along with mountaineering has long been a hobby of mine, rarely venture out to do any actually mountaineering or climbing these days, but I still get out for some hill walking.  My first long distance walk was the Everest Base Camp trek, many years ago, though I had done some walking before that, since then I have done a fair bit of walking around south Wales, the Lakes and things like the North Downs Way. During the 2020/21 Covid pandemic I walked both the Capital Ring and the London Loop and really got back into walking again.",
			link: [{
				text: "Capital Ring",
				path: "https://tfl.gov.uk/modes/walking/capital-ring"
			}, {
				text: "London Loop",
				path: "https://tfl.gov.uk/modes/walking/loop-walk"
			}]
		}
	]
};
const equipment = {
	pageTitle: "Equipment",
	titleParam: "Equipment",
	introPara: [
		{
			type: "text",
			value: "Equipment or gear can make or break your experience of either hill walking or climbing. These are my thoughts on buying and selecting equipment and also links to shops or blogs that I have found useful.  I tend to focus on light weight gear but it is always a comprise between cost and weight, personal preference comes into things a lot as well, as does for some ethical considerations such as not using animal products"
		},
		{
			type: "text",
			value: "You will find various sub-sections focusing of differing types of gear, such as clothing, camping equipment"
		}
	]
};

export {
	walks, equipment
};
