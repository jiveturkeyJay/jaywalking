// map case

const cookingAndEating = {
  items: {
    1: {
      item: "Water bottles",
      weight: "40",
      notes: "2 x 1 litre plastic bottles"
    },
    2: {
      item: "Water bladder 3 litre",
      weight: "86",
      notes: "Hydrapak Seeker"
    },
    3: {
      item: "MSR Trailshot water filter",
      weight: "140",
      notes: "Light weight water filter"
    },
    5: {
      item: "Burner",
      weight: "124",
      notes: "Koro Alpkit"
    },
    6: {
      item: "Wind shield",
      weight: "10",
      notes: "Home made from aluminium baking tray"
    },
    7: {
      item: "Gas canister",
      weight: "380",
      notes: "None"
    },
    8: {
      item: "Titanium pot",
      weight: "120",
      notes: "900ml with lid (Alpkit)"
    },
    9: {
      item: "Titanium cup",
      weight: "62",
      notes: "Alpkit"
    },
    10: {
      item: "SnapWire Foon",
      weight: "17",
      notes: "A combined titanium spoon and fork"
    },
    11: {
      item: "Cosy (wraps round cooking pot)",
      weight: "31",
      notes: "Home made"
    },
    12: {
      item: "Pot holder",
      weight: "28",
      notes: "None"
    },
    13: {
      item: "Lighter/striker",
      weight: "27",
      notes: "Jasper from Alpkit"
    },
    14: {
      item: "Penknife/multi-tool",
      weight: "64",
      notes: "Swiss army knife Compact/Huntsman"
    },
    15: {
      item: "Plastic bottles, Nalgene Travel Kit",
      weight: "64",
      notes: "Used for salt, pepper etc"
    }
  },
  heading: "Cooking and Eating",
  type: "cooking_and_eating"
};

const sleepSystem = {
  items: {
    17: {
      item: "Sleeping bag",
      weight: "636",
      notes: "Marmot Micron 40"
    },
    18: {
      item: "Tent",
      weight: "854",
      notes: "Terra Nova Laser Competition 1"
    },
    19: {
      item: "Pegs",
      weight: "120",
      notes: "Probably too many (Check Weight)"
    },
    20: {
      item: "Sleeping mat",
      weight: "280",
      notes: "Nordisk Ven 2.5 3/4 length"
    },
    21: {
      item: "Base mat",
      weight: "104",
      notes: "Granite Gear XL Featherweight Foam Mat"
    },
    22: {
      item: "Pillow",
      weight: "48",
      notes: "Exped (check weight)"
    }
  },
  heading: "Tent and Sleep System",
  type: "tent_and_sleep_system"
};

const carrying = {
  items: {
    25: {
      item: "Backpack",
      weight: "524",
      notes: "Granite Gear"
    },
    27: {
      item: "Rain cover",
      weight: "80",
      notes: "Osprey Ultralight Raincover"
    },
    // 271: {
    //   item: "Liner",
    //   weight: "102",
    //   notes: "Osprey water proof liner"
    // },
    28: {
      item: "Chest pouch",
      weight: "95",
      notes: "OMM "
    },
    29: {
      item: "Zippity 2 x 1 litre",
      weight: "17",
      notes: "Zippity Granite Gear (check weight)"
    },
    30: {
      item: "Zippity 2 x 2.4 litre",
      weight: "24",
      notes: "Zippity Granite Gear (check weight) "
    },
    31: {
      item: "Exped Fold Drybags x 3",
      weight: "80",
      notes: "Small, medium and large (check weight)"
    },
    32: {
      item: "Sea to Summit Neck Wallet",
      weight: "42",
      notes: "None",
      isWorn: true
    }
  },
  heading: "Backpack and storage",
  type: "backpack_and_storage"
};

const clothing = {
  items: {
    1: {
      item: "Sun glasses",
      weight: "24",
      notes: "Camino Spectron 3 CF Sunglasses",
      isWorn: true
    },
    2: {
      item: "Sun hat",
      weight: "200",
      notes: "Existing one with neck cover",
      isWorn: true
    },
    3: {
      item: "Mid layer",
      weight: "180",
      notes: "Montane Allez Micro Hoodie"
    },
    4: {
      item: "Shirt",
      weight: "210",
      notes: "None",
      isWorn: true
    },
    5: {
      item: "Base Layer",
      weight: "208",
      notes: "Icebreaker 200 Oasis Long Sleeve Crewe Top"
    },
    6: {
      item: "Belt",
      weight: "42",
      notes: "Rohan belt",
      isWork: true
    },
    7: {
      item: "Trousers",
      weight: "260",
      notes: "OM",
      isWorn: true
    },
    8: {
      item: "Merino Pants x 2",
      weight: "96",
      notes: "Icebreaker Anatomica Cool-Lite Trunks",
      isWorn: true,
      isFraction: 2
    },
    9: {
      item: "Socks x 2",
      weight: "168",
      notes: "Injinji Trail Midweight Socks x 2",
      isWorn: true,
      isFraction: 2
    },
    10: {
      item: "Boots",
      weight: "395",
      notes: "Salomen X Ultra 4 Wide Gore-Tex",
      isWorn: true
    },
    11: {
      item: "Beanie",
      weight: "18",
      notes: "OM beanie"
    },
    12: {
      item: "Waterproof trousers",
      weight: "185",
      notes: "Berghuas Paclite"
    },
    13: {
      item: "Jacket waterproof",
      weight: "212",
      notes: "Haglofs Lims jacket"
    },
    14: {
      item: "Merino leggings",
      weight: "199",
      notes: "Icebreaker 200"
    },
    15: {
      item: "Down pullover",
      weight: "235",
      notes: "Crux Turbo Top"
    },
    16: {
      item: "Gloves",
      weight: "100",
      notes: "Odd pair of gloves that I had CHECK WEIGHT"
    }
  },
  heading: "Clothing",
  type: "clothing"
};

const medicalAndEmergency = {
  items: {
    57: {
      item: "Medical pack",
      weight: "146",
      notes: "Plasters, pills etc"
    },
    62: {
      item: "Tick remover",
      weight: "4",
      notes: "None"
    },
    63: {
      item: "Midge mask",
      weight: "22",
      notes: "For use in Scotland"
    },
    65: {
      item: "Sewing kit and spares",
      weight: "72",
      notes: "spare laces, gore tex repair, duct tape, guy ropes"
    },
    69: {
      item: "Small mirror",
      weight: "31",
      notes: "Useful if you have a tick on the face"
    },
    67: {
      item: "Emergency whistle",
      weight: "10",
      notes: "Attached to compass"
    }
  },
  heading: "Medical and repairs",
  type: "medical_and_repairs"
};

const washingAndCleaning = {
  items: {
    1: {
      item: "Travel towel",
      weight: "28",
      notes: "None"
    },
    2: {
      item: "Small towel",
      weight: "30",
      notes: "None: CHECK WEIGHT"
    },
    3: {
      item: "Toothbrush",
      weight: "10",
      notes: "Wooden vegan toothbrush"
    },
    4: {
      item: "Tooth paste",
      weight: "116",
      notes: "75ml"
    },
    5: {
      item: "Dr Bonners 18 in 1",
      weight: "163",
      notes: "Organic liquid soap with 18 uses, washing, cleaning, greasing axles etc"
    },
    6: {
      item: "Sun cream, lip salve, vaseline and (liquid) soap",
      weight: "163",
      notes: "None"
    }
  },
  heading: "Washing and cleaning",
  type: "washing_and_cleaning"
};

const electronics = {
  items: {
    1: {
      item: "Phone",
      weight: "0",
      notes: "None",
      isWorn: true
    },
    2: {
      item: "Samsung Tablet 7 inch",
      weight: "714",
      notes: "Tough waterproof used for photos, mapping",
      isWorn: true
    },
    3: {
      item: "Charger",
      weight: "345",
      notes: "Anker Power Core 20000"
    },
    4: {
      item: "Fast charger plug",
      weight: "68",
      notes: "Can charge 3 items at once"
    },
    5: {
      item: "Various cables x 2",
      weight: "45",
      notes: "For use with charger"
    },
    6: {
      item: "Astro 300 Headlamp",
      weight: "75",
      notes: "Weight with 3 batteries"
    }
  },
  heading: "Electronics",
  type: "electronics"
};

const other= {
  items: {
    58: {
      item: "Trowel",
      weight: "52",
      notes: "Do what a bear does"
    },
    59: {
      item: "Watch",
      weight: "49",
      notes: "Worn",
      isWorn: true
    },
    66: {
      item: "Compass",
      weight: "36",
      notes: "Silva Expedition 4 Compass",
      isWorn: true
    },
    68: {
      item: "Map case",
      weight: "80",
      notes: "Orlieb water proof"
    }
  },
  heading: "Odds and sods",
  type: "odds_and sods"
};

const gearListData = [
  cookingAndEating,
  sleepSystem,
  carrying,
  clothing,
  medicalAndEmergency,
  washingAndCleaning,
  electronics,
  other
];

export {
  gearListData
};
