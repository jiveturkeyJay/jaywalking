const adminArticleKeys = [
	"articles_id",
	"verseIndex",
	"sub_type_param",
	"articleParam",
	"title",
	"article_title",
	"articlesJson"
];


const newArticleKeys = [
	'verseIndex',
	'sub_type_param',
	'articleParam',
	'title',
	'articleTitle',
	'articlesJson'
];

const updateHomePageShowAsText = ["sub_type_id", "type_id", "sub_type_param", "type_name"];
const adminArticlesShowAsText = ["articles_id", "verseIndex", "sub_type_param", "title"];
const adminUpdateArticleChecks = ["articleParam", "article_title", "articlesJson"];

const adminUpdateHomepageChecks = ["title", "jsonData"];

export {
	adminArticleKeys,
	adminArticlesShowAsText,
	adminUpdateArticleChecks,
	newArticleKeys,
	updateHomePageShowAsText,
	adminUpdateHomepageChecks
};
