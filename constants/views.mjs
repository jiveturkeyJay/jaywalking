/**
 * expectAdditional is used in verse page to create the data for the aside,
 * different ones for walks/equipment
 */
const expectedAdditional = {
	dateWalked: {
		text: 'Date walked',
		helper: 'createDate'
	},
	distanceWalked: {
		text: 'Distance walked',
		helper: 'distance'
	},
	cumulativeDistance: {
		text: 'Cumulative distance',
		helper: 'distance'
	},
	weather: {
		text: 'Weather',
		helper: null
	},
	accommodation: {
		text: 'Accommodation',
		helper: null
	}
};

const expectedAdditionalKeys = Object.keys(expectedAdditional);


/**
 * admin stuff
 */
export {
	expectedAdditional,
	expectedAdditionalKeys
};
