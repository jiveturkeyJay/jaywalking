export const sortTypes = {
	dateAsc: true,
	dateDesc: true,
	titleAsc: true,
	titleDesc: true
};
