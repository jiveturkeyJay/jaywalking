const urls = {
	adminError: "admin/error"
};

/**
 * used to update or add new articles or home pages
 * @type {{admin_update_type: string, admin_new_article: string, admin_new_type: string, admin_update_article: string}}
 */
const adminItemtoRender = {
	admin_new_article: "admin/new_article",
	admin_update_article: "admin/update_article",
	admin_new_type: "admin/new_type",
	admin_update_type: "admin/update_type"

};

export {
	urls,
	adminItemtoRender
};
