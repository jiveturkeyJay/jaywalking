const subTypes = { walks: true, equipment: true };
const checkSubTypes = (subType) => Reflect.has(subTypes, subType);

export {
	subTypes,
	checkSubTypes
};
