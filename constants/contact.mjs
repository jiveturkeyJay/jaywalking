const unallowedWords = [
	'Ndewo,',
	'marketing',
	'ad',
	'analyze',
	'sex',
	'SEX',
	'behavior',
	'customers',
	'customer',
	'SEO',
	'metric',
	'metrics',
	'keywords',
	'optimized',
	'investment',
	'income',
	'brand',
	'click',
	'domain',
	'domains',
	'equity',
	'PPV',
	'PBN',
	'advertising',
	'dollars',
	'USD',
	'rank',
	'ranking',
	'rankings',
	'revenue',
	'sales',
	'search engine',
	'skype',
	'sites',
	'jaywalking.co.uk',
	'prices',
	'strategy',
	'naked',
	'indecent',
	'Viagra',
	'discount',
	'buy'
];

const unallowedNames = ['RobertReott', 'zardoz']

const unallowedEmailBits = ['reply', 'info'];

export {
	unallowedWords,
	unallowedEmailBits,
	unallowedNames
}
