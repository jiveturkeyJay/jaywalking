// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })
Cypress.Commands.add('goToAdminUpdatePage', () => {
  cy.get("input[name='updateOrAddNew']:first-of-type").check();
  // walks
  cy.get("input[name='type']:first-of-type").check();
  cy.get('button[type="button"]:visible').click();
  cy.wait(500);

  cy.get('select:visible').select('fakewalk');
  cy.get('button[data-button="get-by-sub-type"]').click();
  cy.wait(500);

  cy.get('select[name="articleParam"]').select('fakewalkparam');
  cy.get('form').submit();
});

Cypress.Commands.add('goToAdminUpdateHomePage', () => {
  cy.get("input[name='updateOrAddNew']:first-of-type").check();
  // walks
  cy.get("input[name='type']:first-of-type").check();
  cy.get('button[type="button"]:visible').click();
  cy.wait(500);

  cy.get('select:visible').select('fakewalk');
  cy.get('form').submit();
});

/**
 *
 * goes from /images/preppedImages to
 * /images/selectImages and selects an images
 * then goes to
 * /images/preppedImages
 */

Cypress.Commands.add('goToImageSelectPage', (image) => {
  cy.get('[aria-labelledby="images-nav"] li').eq(1).click();
  cy.get('h1').should('have.text', 'Select images');
  cy.selectImageFile(image);
  cy.get('form').submit();
});

Cypress.Commands.add('selectImageFile', (image) => {
  cy.get('input[type=file]').selectFile([`public/test_images/${image}`]);
});

Cypress.Commands.add('checkImageLengthAndName', (image, length = 1) => {
  cy.get('.form-grid__image')
    .should('have.length', length)
    .should('have.attr', 'src')
    .and('eq', `/images/holding/${image}`);
});

Cypress.Commands.add('assertValueCopiedToClipboard', (value) => {
  cy.window().then((win) => {
    win.navigator.clipboard.readText().then((text) => {
      expect(text).not.to.be.null;
    });
  });
});

Cypress.Commands.add('checkImagesExist', (baseName) => {
  cy.get(`img[src*="${baseName}_large.jpg"]`).should('have.length', 1);
  cy.get(`img[src*="${baseName}_medium.jpg"]`).should('have.length', 1);
  cy.get(`img[src*="${baseName}_small.jpg"]`).should('have.length', 1);
});
