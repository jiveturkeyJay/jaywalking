describe('Static pages', () => {
  it('should have an accessibility page', () => {
    cy.visit('/accessibility');
    cy.get('h1').should('have.text', 'Accessibility Statement');
  });

  it('should have a /privacy page', () => {
    cy.visit('/privacy');
    cy.get('h1').should('have.text', 'Privacy Policy');
  });

  describe('Lejob', () => {
    it('should have a gear list page', () => {
      cy.visit('/static/gearlist');
      cy.get('h1').should('have.text', 'Lejog: Gear list');
    });

    it('should have a Why? page', () => {
      cy.visit('/static/why');
      cy.get('h1').should('have.text', 'Lejog: Why walk it');
    });

    it('should have a Route page', () => {
      cy.visit('/static/route');
      cy.get('h1').should('have.text', 'Lejog: My route');
    });

    it('should have a Rules page', () => {
      cy.visit('/static/rules');
      cy.get('h1').should('have.text', 'Lejog: Walking Rules');
    });

    it('should have a Doing page', () => {
      cy.visit('/static/doing');
      cy.get('h1').should('have.text', 'Lejog: Doing the walk');
    });
  });
});
