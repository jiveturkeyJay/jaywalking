import { originalArticleJSON, updatedArticleJSON } from '../data/adminData.mjs';

describe('Admin: Update article', () => {
  beforeEach(() => {
    cy.visit('/admin');
  });

  // need to fix some of these so that we are checking for the visible ones only
  it('should have the correct title and DOM elements', () => {
    cy.get('h1').should('have.text', 'Update Admin: Home');
    cy.get('input[type="radio"]').should('have.length', 4);
    cy.get('fieldset:visible').should('have.length', 2);
    cy.get('button[type="button"]:visible').should('have.length', 1);
  });

  // todo add error handling?
  it('should show additional elements when items selected and button pressed', () => {
    // update
    cy.get("input[name='updateOrAddNew']:first-of-type").check();
    // walks
    cy.get("input[name='type']:first-of-type").check();
    cy.get('button[type="button"]:visible').click();

    cy.wait(500);
    // shows select for walks
    cy.get('fieldset:visible').should('have.length', 3);
    cy.get('select:visible').should('have.length', 1);
    cy.get('button[type="button"]:visible').should('have.length', 2);
    cy.get('button[data-button="get-by-sub-type"]').click();

    cy.wait(500);
    // shows selects for walk sections eg Farmham to Gomshall
    cy.get('fieldset:visible').should('have.length', 4);
    cy.get('select:visible').should('have.length', 2);
  });

  it('should select an article to update via the drop down selects', () => {
    // get to the fake page
    // update
    cy.get("input[name='updateOrAddNew']:first-of-type").check();
    // walks
    cy.get("input[name='type']:first-of-type").check();
    cy.get('button[type="button"]:visible').click();
    cy.wait(500);

    cy.get('select:visible').select('fakewalk');
    cy.get('button[data-button="get-by-sub-type"]').click();
    cy.wait(500);

    cy.get('select[name="articleParam"]').select('fakewalkparam');
    cy.get('form').submit();

    // we are on the page to update the article
    cy.location().should((location) => {
      expect(location.pathname).to.eql(
        '/admin/update/walks/fakewalk/fakewalkparam'
      );
    });
  });

  it('should update Admin Page', () => {
    cy.goToAdminUpdatePage();

    // check DOM
    cy.get('input[type="text"]').should('have.length', 2);
    cy.get('textarea').should('have.length', 1);

    cy.get('#articleParam').should('have.value', 'fakewalkparam');
    cy.get('#article_title').should('have.value', 'somewhere to somewhere');
    cy.get('#articlesJson').should('have.value', originalArticleJSON);

    cy.get('#article_title').type(' else');
    // clear the text as editing it is too much
    cy.get('#articlesJson').clear();
    cy.get('#articlesJson').type(updatedArticleJSON, {
      parseSpecialCharSequences: false
    });
    cy.get('form').submit();

    cy.wait(500);
    ///admin/update/walks/fakewalk/fakewalkparam
    // this should be different
    cy.location().should((location) => {
      expect(location.pathname).to.eql(
        '/admin/update/walks/fakewalk/fakewalkparam'
      );
    });

    cy.get('h1').should('have.text', 'Update article success');
    cy.get('main > a').click();
    cy.wait(500);
    cy.get('h1').should('have.text', 'Fake walk: Fake walk section');
    cy.get('#article-cont p').should(
      'have.text',
      "Text should be here UPDATED and should deal with it's and stuff's"
    );
  });

  it('should reset the database', () => {
    cy.goToAdminUpdatePage();

    cy.get('#article_title').clear();
    cy.get('#article_title').type('somewhere to somewhere');
    // clear the text as editing it is too much
    cy.get('#articlesJson').clear();
    cy.get('#articlesJson').type(originalArticleJSON, {
      parseSpecialCharSequences: false
    });

    cy.get('form').submit();

    cy.get('h1').should('have.text', 'Update article success');
    cy.get('main > a').click();
    cy.wait(500);
    cy.get('#article-cont p').should('have.text', 'Text should be here');
  });
});
