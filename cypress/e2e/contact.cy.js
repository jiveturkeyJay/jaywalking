import { unallowedWords, unallowedNames } from '../../constants/contact.mjs';

describe('Contact', () => {
  beforeEach(() => {
    cy.visit('/contact');
  });

  describe('Form elements', () => {
    it('should have the correct heading on the page', () => {
      cy.get('h1').should('have.text', 'Contact me');
    });

    it('should have the correct fieldsets and legends', () => {
      cy.get('fieldset').should('have.length', 2);
      cy.get('legend').should('have.length', 2);
    });

    it('should have correct form elements', () => {
      cy.get('#yourname').should('have.length', 1);
      cy.get('input[type="email"]').should('have.length', 1);
      cy.get('textarea').should('have.length', 1);
      cy.get('button[type="submit"]').should('have.length', 1);
    });

    it('should have the correct amount of label elements', () => {
      cy.get('label').should('have.length', 4);
    });
  });

  describe('Form submits with all errors ', () => {
    it('should have the corrects errors if form is submitted with no values', () => {
      cy.get('.error-holder').should('have.length', 0);
      cy.get('form').submit();
      cy.get('.error-holder').should('have.length', 1);
      cy.get('.error-holder').find('a.text-error').should('have.length', 3);
    });

    it('should error holder with correct messages and hrefs', () => {
      cy.get('form').submit();
      cy.get('a.text-error').as('formErrors');

      cy.get('@formErrors')
        .eq(0)
        .should('have.text', 'Your name cannot be empty')
        .and('have.attr', 'href')
        .and('eq', '#yourname');

      cy.get('@formErrors')
        .eq(1)
        .should('have.text', 'Your email address must be valid')
        .and('have.attr', 'href')
        .and('eq', '#emailaddress');

      cy.get('@formErrors')
        .eq(2)
        .should('have.text', 'Your message cannot be empty')
        .and('have.attr', 'href')
        .and('eq', '#yourmessage');
    });

    it('should focus on correct input when an error message is clicked', () => {
      cy.get('form').submit();
      cy.get('a.text-error').as('formErrors');
      cy.get('@formErrors').eq(0).click();
      cy.focused().should('have.attr', 'id', 'yourname');

      cy.get('@formErrors').eq(1).click();
      cy.focused().should('have.attr', 'id', 'emailaddress');

      cy.get('@formErrors').eq(2).click();
      cy.focused().should('have.attr', 'id', 'yourmessage');
    });
  });

  describe('Form with single errors', () => {
    it('should have correct error when no name', () => {
      cy.get('input[type="email"]').type('someemail@email.com');
      cy.get('textarea').type('some message');
      cy.get('form').submit();

      cy.get('a.text-error').as('formErrors').should('have.length', 1);
      cy.get('@formErrors').eq(0).click();
      cy.focused().should('have.attr', 'id', 'yourname');
    });

    it('should have correct error when no email', () => {
      cy.get('#yourname').type('some name');
      cy.get('textarea').type('some message');
      cy.get('form').submit();

      cy.get('a.text-error').as('formErrors').should('have.length', 1);
      cy.get('@formErrors').eq(0).click();
      cy.focused().should('have.attr', 'id', 'emailaddress');
    });

    it('should have correct error when incorrect email', () => {
      cy.get('#yourname').type('some name');
      cy.get('input[type="email"]').type('somename');
      cy.get('textarea').type('some message');
      cy.get('form').submit();

      cy.get('a.text-error').as('formErrors').should('have.length', 1);
      cy.get('@formErrors').eq(0).click();
      cy.focused().should('have.attr', 'id', 'emailaddress');
    });

    it('should have correct error when no email', () => {
      cy.get('#yourname').type('sometext');
      cy.get('input[type="email"]').type('somemail@email.com');
      cy.get('form').submit();

      cy.get('a.text-error').as('formErrors').should('have.length', 1);
      cy.get('@formErrors').eq(0).click();
      cy.focused().should('have.attr', 'id', 'yourmessage');
    });
  });

  describe('Forms that contain text or words that are not allowed', () => {
    it('should go to fail page when the name contains a name is not allowed allowed', () => {
      cy.get('#yourname').type(unallowedNames[1]);
      cy.get('#emailaddress').type('correct@correct.com');
      cy.get('#yourmessage').type(`some message`);
      cy.get('form').submit();
      cy.location().should((loc) => {
        expect(loc.pathname).to.eq(
          '/contactfail'
        );
      });
    });

    it('should go to fail page when the email address that contain parts that are not allowed', () => {
      cy.get('#yourname').type('bob');
      cy.get('#emailaddress').type('reply@correct.com');
      cy.get('#yourmessage').type(`some message`);
      cy.get('form').submit();
      cy.location().should((loc) => {
        expect(loc.pathname).to.eq(
          '/contactfail'
        );
      });
    });

    it('should go to fail page when the textarea contains any non allowed words', () => {
      cy.get('#yourname').type('bob');
      cy.get('#emailaddress').type('correct@correct.com');
      cy.get('#yourmessage').type(`some message with a word ${unallowedWords[1]} that is not allowed`);
      cy.get('form').submit();
      cy.location().should((loc) => {
        expect(loc.pathname).to.eq(
          '/contactfail'
        );
      });
    });
  });

  describe('Bot protection [hidden element]', () => {
    it('should go to success page when hidden element is filled', () => {

      cy.get('form .is-accessible-text').within(() => {
        cy.get('#url').type('Pamela', {force: true} )
      })

      cy.get('form').submit();
      cy.wait(400);
      cy.location().should((loc) => {
        expect(loc.pathname).to.eq('/contactsuccess');
      });
    });
  });
});
