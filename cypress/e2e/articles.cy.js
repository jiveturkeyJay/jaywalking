describe('Articles', () => {
  beforeEach(() => {
    cy.visit('/walks/northdownsway/farnhamtogomshall');
  });

  it('should only have a next link when it is the first in a set of articles', () => {
    cy.get('.grid-verse-nav li')
      .should('have.length', 1)
      .find('span')
      .should('have.text', 'Next:');

    cy.get('.verse-nav__link')
      .should('have.length', 1)
      .and('have.text', 'gomshall to merstham');
  });

  it('should only have a previous link when it is the last in a set of articles', () => {
    cy.visit('/walks/northdownsway/canterburytobougtonlees');
    cy.get('.grid-verse-nav li')
      .should('have.length', 1)
      .find('span')
      .should('have.text', 'Previous:');

    cy.get('.verse-nav__link')
      .should('have.length', 1)
      .and('have.text', 'dover to canterbury');
  });

  it('should navigate between articles using the previous and next button', () => {
    // clickiing next
    cy.get('.verse-nav__link').click();
    cy.wait(400);
    cy.location().should((loc) => {
      expect(loc.pathname).to.eq('/walks/northdownsway/gomshalltomerstham');
    });

    // clicking previous
    cy.get('.grid-verse-nav li').should('have.length', 2).eq(1).click();
    cy.wait(400);
    cy.location().should((loc) => {
      expect(loc.pathname).to.eq('/walks/northdownsway/farnhamtogomshall');
    });
  });
});
