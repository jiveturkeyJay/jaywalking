describe('Article gallery', () => {
  beforeEach(() => {
    cy.visit('/walks/northdownsway/farnhamtogomshall');
  });

  it('should have buttons to open the gallery', () => {
    cy.get('.btn__dialog').as('gallerybutton');
    cy.get('@gallerybutton').should('have.length', 16);
  });

  it('should open modal when a gallery button is clicked', () => {
    cy.get('.dialog-outer').should('not.be.visible');
    cy.get('.btn__dialog').eq(3).click();
    cy.get('.dialog-outer').should('be.visible');
  });

  it('should have the correct item is in the modal', () => {
    cy.get('img.dialog-img').should('not.be.visible');
    cy.get('.btn__dialog').eq(3).click();
    cy.get('img.dialog-img')
      .should('be.visible')
      .and('have.attr', 'src')
      .and('equal', '/images/ndw1_path_large.jpg');

    cy.get('img.dialog-img')
      .and('have.attr', 'alt')
      .and('equal', 'Autumn leaves covering a path though some woodland');
  });

  it('should close the modal when the dialog background is clicked', () => {
    cy.get('.btn__dialog').eq(3).click();
    cy.get('.dialog-outer').should('be.visible');
    cy.get('.dialog-outer').click(10, 10);
    cy.get('.dialog-outer').should('have.class', 'is-hidden');
  });

  it('should close the modal when the close button is clicked', () => {
    cy.get('.btn__dialog').eq(3).click();
    cy.get('.dialog-outer').should('be.visible');
    cy.get('.btn__dialog--close').click();
    cy.get('.dialog-outer').should('not.be.visible');
    cy.get('.dialog-outer').should('have.class', 'is-hidden');
  });

  // cant currently tab for some reason
});
