describe('Navigation and breadcrumb', () => {
  beforeEach(() => {
    cy.visit('/');
  });

  it('should have the correct amount of elements in the nav', () => {
    cy.get('.main-nav__list li').should('have.length', 4);
  });

  describe('Homepage', () => {
    it('should have the correct breadcrumb', () => {
      cy.get('.crumb li').should('have.length', 1);
      cy.get('.crumb__item').contains('Home');
    });
  });

  describe('Homepage to Contact page', () => {
    it('should go to contact when contact is clicked and have correct breadcrumb', () => {
      cy.contains('.main-nav__link', 'Contact').click();
      cy.location().should((loc) => {
        expect(loc.pathname).to.eq('/contact');
      });

      cy.get('.crumb li').should('have.length', 2);
      cy.get('.crumb li').eq(1).should('have.text', 'Contact');
      cy.get('.crumb li a')
        .should('have.length', 1)
        .and('have.text', 'Home')
        .and('have.attr', 'href')
        .and('eq', '/');
    });
  });

  describe('Homepage to Walks Home eg /walks', () => {
    it('should go to walks home when main nav link is clicked', () => {
      cy.contains('.main-nav__link', 'walks').click();
      cy.location().should((loc) => {
        expect(loc.pathname).to.eq('/walks');
      });

      cy.get('.crumb li').should('have.length', 2);
      cy.get('.crumb li').eq(1).should('have.text', 'Walks');
      cy.get('.crumb li a')
        .should('have.length', 1)
        .and('have.text', 'Home')
        .and('have.attr', 'href')
        .and('eq', '/');
    });
  });

  describe('Homepage to Walks > Walk Homepage eg /walks/northdownsway', () => {
    it('should go to walk home page when on walks and an item is clicked', () => {
      cy.contains('.main-nav__link', 'walks').click();
      cy.wait(400);
      cy.get('[data-id="northdownsway"] > a').click();
      cy.wait(400);
      cy.location().should((loc) => {
        expect(loc.pathname).to.eq('/walks/northdownsway');
      });

      cy.get('.crumb li').as('crumbs');
      cy.get('@crumbs').should('have.length', 3);
      cy.get('.crumb li a').should('have.length', 2);

      cy.get('@crumbs').eq(0).should('contain.text', 'Home');
      cy.get('@crumbs')
        .eq(0)
        .find('a')
        .and('have.text', 'Home')
        .and('have.attr', 'href')
        .and('eq', '/');

      cy.get('@crumbs').eq(1).should('contain.text', 'walks');
      cy.get('@crumbs')
        .eq(1)
        .find('a')
        .and('contain.text', 'walks')
        .and('have.attr', 'href')
        .and('eq', '/walks');

      cy.get('@crumbs').eq(2).should('contain.text', 'North Downs Way');
    });

    describe('Homepage to Walks > Walk Homepage > Walks Section eg /walks/northdownsway/wyetofolkestone', () => {
      it('should go to walk section when a section link is clicked', () => {
        cy.contains('.main-nav__link', 'walks').click();
        cy.wait(400);
        cy.get('[data-id="northdownsway"] > a').click();
        cy.wait(400);
        cy.contains('.verse-section__link', 'gomshall to merstham').click();
        cy.wait(400);
        cy.location().should((loc) => {
          expect(loc.pathname).to.eq('/walks/northdownsway/gomshalltomerstham');
        });

        cy.get('.crumb li').as('crumbs');
        cy.get('@crumbs').should('have.length', 4);
        cy.get('.crumb li a').should('have.length', 3);

        cy.get('@crumbs').eq(0).should('contain.text', 'Home');
        cy.get('@crumbs')
          .eq(0)
          .find('a')
          .and('have.text', 'Home')
          .and('have.attr', 'href')
          .and('eq', '/');

        cy.get('@crumbs').eq(1).should('contain.text', 'walks');
        cy.get('@crumbs')
          .eq(1)
          .find('a')
          .and('contain.text', 'walks')
          .and('have.attr', 'href')
          .and('eq', '/walks');

        cy.get('@crumbs').eq(2).should('contain.text', 'North Downs Way');
        cy.get('@crumbs')
          .eq(2)
          .find('a')
          .and('contain.text', 'North Downs Way')
          .and('have.attr', 'href')
          .and('eq', '/walks/northdownsway');

        cy.get('@crumbs').eq(3).should('contain.text', 'Gomshall to Merstham');
      });
    });
  });

  describe('Walks article to gallery', () => {
    it('should go to the gallery', () => {
      cy.contains('.main-nav__link', 'walks').click();
      cy.get('[data-id="northdownsway"] > a').click();
      cy.contains('.verse-section__link', 'gomshall to merstham').click();

      // click the gallery link
      cy.contains('View all images of this days walk').click();
      cy.location().should((loc) => {
        expect(loc.pathname).to.eq(
          '/walks/northdownsway/gomshalltomerstham/gallery'
        );
      });

      cy.get('.crumb li').as('crumbs');
      cy.get('@crumbs').should('have.length', 5);
      cy.get('.crumb li a').should('have.length', 4);

      cy.get('@crumbs').eq(0).should('contain.text', 'Home');
      cy.get('@crumbs')
        .eq(0)
        .find('a')
        .and('have.text', 'Home')
        .and('have.attr', 'href')
        .and('eq', '/');

      cy.get('@crumbs').eq(1).should('contain.text', 'walks');
      cy.get('@crumbs')
        .eq(1)
        .find('a')
        .and('contain.text', 'walks')
        .and('have.attr', 'href')
        .and('eq', '/walks');

      cy.get('@crumbs').eq(2).should('contain.text', 'North Downs Way');
      cy.get('@crumbs')
        .eq(2)
        .find('a')
        .and('contain.text', 'North Downs Way')
        .and('have.attr', 'href')
        .and('eq', '/walks/northdownsway');

      cy.get('@crumbs')
        .eq(3)
        .find('a')
        .and('contain.text', 'Gomshall to Merstham')
        .and('have.attr', 'href')
        .and('eq', '/walks/northdownsway/gomshalltomerstham');

      cy.get('@crumbs').eq(4).should('contain.text', 'gallery');

      // goes back correctly
      cy.get('@crumbs').eq(3).find('a').click({ force: true });

      cy.location().should((loc) => {
        expect(loc.pathname).to.eq('/walks/northdownsway/gomshalltomerstham');
      });
    });
  });
});
