import { fakeArticleJson } from '../data/adminData.mjs';

describe('Admin: Update article', () => {
  beforeEach(() => {
    cy.visit('/admin');
  });

  // need to fix some of these so that we are checking for the visible ones only
  it('should have the correct title and DOM elements', () => {
    cy.get('h1').should('have.text', 'Update Admin: Home');
    cy.get('input[type="radio"]').should('have.length', 4);
    cy.get('fieldset:visible').should('have.length', 2);
    cy.get('button[type="button"]:visible').should('have.length', 1);
  });

  // todo add error handling?
  it('should show additional elements when items selected and button pressed', () => {
    // new
    cy.get("input[name='updateOrAddNew']:last-of-type").check();
    // walks
    cy.get("input[name='type']:first-of-type").check();
    cy.get('button[type="button"]:visible').click();

    cy.wait(500);
    cy.get('select:visible').select('fakewalk');
    cy.get('form').submit();

    // we are on the page to create the new article
    cy.location().should((location) => {
      expect(location.pathname).to.eql('/admin/new/walks/fakewalk/article');
    });

    // should have the correct form elements
    cy.get('input[type="text"]').should('have.length', 5);
    cy.get('textarea').should('have.length', 1);

    // inputs should be correct
    cy.get('input[name="verseIndex"]').should('have.value', '');
    cy.get('input[name="sub_type_param"]').should('have.value', 'fakewalk');
    cy.get('input[name="articleParam"]').should('have.value', '');
    cy.get('input[name="title"]').should('have.value', '');
    cy.get('input[name="articleTitle"]').should('have.value', '');
    cy.get('[name="articlesJson"]').should('have.value', '');
  });

  it('should add new article correctly', () => {
    // new
    cy.get("input[name='updateOrAddNew']:last-of-type").check();
    // walks
    cy.get("input[name='type']:first-of-type").check();
    cy.get('button[type="button"]:visible').click();

    cy.wait(500);
    cy.get('select:visible').select('fakewalk');
    cy.get('form').submit();

    cy.get('input[name="verseIndex"]').type('2');
    cy.get('input[name="articleParam"]').type('fakewalkparam2');
    cy.get('input[name="title"]').type('Fake walk title');
    cy.get('input[name="articleTitle"]').type('Fake Article Title');
    cy.get('[name="articlesJson"]').type(fakeArticleJson, {
      parseSpecialCharSequences: false
    });
    cy.get('form').submit();
    cy.wait(500);

    cy.location().should((location) => {
      expect(location.pathname).to.eql('/admin/new/article');
    });

    cy.get('#viewArticleLink').click();

    cy.location().should((location) => {
      expect(location.pathname).to.eql('/walks/fakewalk/fakewalkparam2');
    });

    cy.get('h1').should('have.text', 'Fake Route Way: Fake Route Title');
    cy.get('#article-cont p').should(
      'have.text',
      'Text for fake walk section xxxx xxxx'
    );
  });

  it('should delete fake article', () => {
    cy.visit('/admin/delete/fakearticle');
    cy.wait(500);
    cy.location().should((location) => {
      expect(location.pathname).to.eql('/admin');
    });
  });
});
