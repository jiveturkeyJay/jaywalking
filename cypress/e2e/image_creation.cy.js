describe('Image creation', () => {
  const imageNavData = [
    { text: 'Home', link: '/images' },
    { text: 'Select images', link: '/images/selectImages' },
    { text: 'View images and json', link: '/images/uploaded' }
  ];

  const imagesNames = {
    image1: 'test_image_one.jpg',
    image2: 'test_image_two.jpg'
  };

  const inputs = {
    'newName-': 'New name',
    'altText-': 'Alt text',
    'captionText-': 'Caption text'
  };

  const { image1, image2 } = imagesNames;

  beforeEach(() => {
    cy.visit('/images');
  });
  // aria-labelledby="images-nav"
  it('should have correct nav', () => {
    cy.get('h1').should('have.text', 'Images start');
    cy.get('[aria-labelledby="images-nav"] li').should('have.length', 3);
    cy.get('[aria-labelledby="images-nav"] a').each(($el, index) => {
      const data = imageNavData[index];
      cy.log($el[0].textContent);
      expect($el[0].textContent).to.equal(data.text);
      expect($el[0].href).to.contain(data.link);
    });
  });

  it('should select an image (on the Select image page) and upload and display it on form submit page', () => {
    cy.get('[aria-labelledby="images-nav"] li').eq(1).click();
    cy.get('h1').should('have.text', 'Select images');
    cy.get('input[type=file]').selectFile([
      'public/test_images/test_image_one.jpg'
    ]);
    cy.get('form').submit();
    cy.get('h1').should('have.text', 'Prepped images');
    cy.get('input:not([type="hidden"])').should('have.length', 6);
  });

  it('should uploaded image if user goes back and selects another image', () => {
    cy.goToImageSelectPage(image1);
    cy.checkImageLengthAndName(image1);

    cy.visit('/images/selectImages');
    cy.selectImageFile(image2);
    cy.get('form').submit();
    cy.checkImageLengthAndName(image2);
  });

  it('should chef an image and create a json file', () => {
    cy.goToImageSelectPage(image1);
    cy.get('#prefix').type('test-prefix');
    for (const [key, value] of Object.entries(inputs)) {
      cy.get(`#${key}0`).type(`${value.trim()}-0`);
    }
    cy.get('form').submit();

    //cy.get('.copy').click();
    // cy.assertValueCopiedToClipboard();
  });
});
