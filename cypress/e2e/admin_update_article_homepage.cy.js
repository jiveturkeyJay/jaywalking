import { fakeHomePageJson, fakeHomePageJsonUpdated } from '../data/adminData.mjs';

describe('Admin: Update article homepage', () => {

	beforeEach(() => {
		cy.visit('/admin');
	});

	it('should have the correct title and DOM elements', () => {
		cy.goToAdminUpdateHomePage();
		cy.wait(500);
		cy.get('h1').should('have.text', 'Admin: Update home page');
		cy.get('fieldset').should('have.length', 1);
		cy.get('button[type="submit"]').should('have.length', 1);
	});


	it('should have the correct values at start', () => {
		cy.goToAdminUpdateHomePage();
		cy.wait(500);
		cy.get('#title').should('have.value', 'Fake Walk Way');
		cy.get('#jsonData').should('have.value', fakeHomePageJson);
	});

	it('should have the update page correctly at start', () => {
		cy.goToAdminUpdateHomePage();
		cy.wait(500);

		cy.get('#title').type(' else');
		// clear the text as editing it is too much
		cy.get('#jsonData').clear();
		cy.get('#jsonData').type(fakeHomePageJsonUpdated, {
			parseSpecialCharSequences: false
		});
		cy.get('form').submit();
		cy.wait(300);

		cy.location().should((location) => {
			expect(location.pathname).to.eql(
				'/admin/update/walks/fakewalk/homepage'
			);
		});
		cy.get('main > a').click();
		cy.wait(500);
		cy.get('article > p').eq(0).should('have.text', 'Fake route intro para updated');
	});

	// reset data after tests
	it('should reset after this', () => {
		cy.goToAdminUpdateHomePage();
		cy.wait(500);

		cy.get('#title').clear();
		cy.get('#title').type('Fake Walk Way');
		// clear the text as editing it is too much
		cy.get('#jsonData').clear();
		cy.get('#jsonData').type(fakeHomePageJson, {
			parseSpecialCharSequences: false
		});
		cy.get('form').submit();
		cy.wait(300);
		cy.get('main > a').click();
		cy.wait(300);
		cy.get('article > p').eq(0).should('have.text', 'Fake route intro para');
	});
});
