describe('Error pages', () => {
  it('should go to an error page when the type does not exist ie /walkss', () => {
    cy.visit('/walks/northdownsway/farnhamtogomshall');
    cy.visit('/walkss');
    cy.get('h1').should('have.text', 'An error has occurred');
  });

  it('should go to an error page when the sub-type does not exist ie /walks/northdownsways', () => {
    cy.visit('/walks/northdownsway/farnhamtogomshall');
    cy.visit('/walk/northdownsways');
    cy.get('h1').should('have.text', 'An error has occurred');
  });

  it('should go to an error page when the article does not exist ie /walk/northdownsway/someroute', () => {
    cy.visit('/walks/northdownsway/farnhamtogomshall');
    cy.visit('/walk/northdownsway/someroute');
    cy.get('h1').should('have.text', 'An error has occurred');
  });
});
