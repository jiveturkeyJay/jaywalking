const originalArticleJSON = '{"keys":["key"],"weather":"Fake weather","bodyData":[{"type":"text","value":"Text should be here"}],"pageTitle":"Fake walk","dateWalked":"2021-05-09T00:00:00.000Z","titleParam":"walks","verseIndex":1,"verseParam":"fakewalkparam","verseTitle":"Fake walk section","dateUpdated":"2021-05-17T00:00:00.000Z","dateWritten":"2021-05-17T00:00:00.000Z","chapterParam":"fakewalk","distanceWalked":24,"seoDescription":"Fake SEO"}';

const updatedArticleJSON = '{"keys":["key"],"weather":"Fake weather","bodyData":[{"type":"text","value":"Text should be here UPDATED and should deal with it\'s and stuff\'s"}],"pageTitle":"Fake walk","dateWalked":"2021-05-09T00:00:00.000Z","titleParam":"walks","verseIndex":1,"verseParam":"fakewalkparam","verseTitle":"Fake walk section","dateUpdated":"2021-05-17T00:00:00.000Z","dateWritten":"2021-05-17T00:00:00.000Z","chapterParam":"fakewalk","distanceWalked":24,"seoDescription":"Fake SEO"}';

const fakeArticleJson = '{"dateWalked":"2022-05-22T05:22:13.000Z","dateWritten":"2022-06-06T05:22:13.000Z","dateUpdated":"","verseTitle":"Fake Route Title","pageTitle":"Fake Route Way","seoDescription":"Fake walk seo","titleParam":"walks","chapterParam":"fakewalkparam","verseParam":"fakewalk","verseIndex":2,"distanceWalked":21,"weather":"Sunny and warm, cloudy","keys":["Icknield Way","Dunstable","Ivanhoe Beacon"],"bodyData":[{"type":"text","value":"Text for fake walk section xxxx xxxx"}]}';

const fakeHomePageJson = '{"keys":["Fake Route Way"],"introPara":[{"link":[{"path":"http://icknieldwaytrail.org.uk/","text":"Fake Route Way Trail"}],"type":"text","value":"Fake route intro para"},{"type":"text","value":"Fake walk home page text xxxx yyyy"}],"pageTitle":"Fake Route Way","titleParam":"walks","verseIndex":0,"dateStarted":"2022-06-06T00:00:00.000Z","dateUpdated":"2022-06-06T00:00:00.000Z","dateWritten":"2022-06-06T00:00:00.000Z","chapterParam":"fakewalk","chapterTitle":"Fake Route Way","seoDescription":"An account of a walk along the Fake Route Way from Ivanhoe Beacon to  Knettishall Heath"}';

const fakeHomePageJsonUpdated = '{"keys":["Fake Route Way"],"introPara":[{"link":[{"path":"http://icknieldwaytrail.org.uk/","text":"Fake Route Way Trail Updated"}],"type":"text","value":"Fake route intro para updated"},{"type":"text","value":"Fake walk home page text xxxx yyyy"}],"pageTitle":"Fake Route Way","titleParam":"walks","verseIndex":0,"dateStarted":"2022-06-06T00:00:00.000Z","dateUpdated":"2022-06-06T00:00:00.000Z","dateWritten":"2022-06-06T00:00:00.000Z","chapterParam":"fakewalk","chapterTitle":"Fake Route Way","seoDescription":"An account of a walk along the Fake Route Way from Ivanhoe Beacon to  Knettishall Heath"}'

export {
	updatedArticleJSON,
	originalArticleJSON,
	fakeArticleJson,
	fakeHomePageJson,
	fakeHomePageJsonUpdated
};
