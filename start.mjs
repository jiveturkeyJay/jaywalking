import { server } from './server.mjs';

server.listen(process.env.PORT,  function(err) {
  console.log(process.env.PORT)
  if (err) {
    console.error('HTTP server FAIL: ', err);
  } else {
    console.log(`HTTP server OK`);
  }
});
