import { connection } from "./index.mjs";
import { sortTypes } from "../../constants/index.mjs";

/**
 * gets query from database
 * @param ctx
 * @param next
 * @return {Promise<*>}
 */
const getAllWalks = async (ctx, next) => {
	const { sortby } = ctx.query;
	try {

		let sortType;
		if (sortTypes[sortby]) {
			sortType = sortby;
		} else {
			sortType = 'dateAsc'
		}
		const isDate = sortType.includes('date');
		const isDesc = sortType.includes('Desc');

			let rows;
			if (isDate) {
				[rows] = await connection.query(`SELECT * from sub_types WHERE type_name = "walks" ORDER BY jsonData->>'$.dateStarted'`);
			} else {
				[rows] = await connection.query(`SELECT * from sub_types WHERE type_name = "walks" ORDER BY title`);
			}
			// so we need to sort this out
			if (!rows.length) {
				// probably don't need to throw an error here
				throw new Error("Unable to find type");
			}

			if (isDesc) {
				rows.reverse()
			}

			ctx.state.query = rows;
			return await next();
	} catch (err) {
		// better to se status here or leave earlier
		ctx.throw(404, 'Unable to retrieve data for sub_type Walks');
	}
};

const getSubTypeHomePage = async (ctx, next) => {
	const {
		sub_type_param
	} = ctx.params;
	try {
			const [rows] = await connection.query(`SELECT * from sub_types WHERE type_name = "walks" AND sub_type_param = "${sub_type_param}"`);
			const [articleData] = await connection.query(`SELECT article_title, articleParam from articles WHERE sub_type_param = "${sub_type_param}" ORDER BY verseIndex ASC`);

		ctx.state.query = rows;
			if (!rows.length) {
				ctx.state.articleData = null;
			} else {
				ctx.state.articleData = articleData;
			}
			return await next();

	} catch (err) {
		ctx.throw(404, `Unable to retrieve data for ${sub_type_param}`);
	}
};

const getSubTypeHomePageOnly = async (ctx, next) => {
	try {
		const {
			type,
			subtype
		} = ctx.params;
		const [rows] = await connection.query(`SELECT * from sub_types WHERE type_name = "${type}" AND sub_type_param = "${subtype}"`);

		if (!rows.length) {
			throw new Error("Unable to find type");
		}

		ctx.state.homepage = rows[0];
		ctx.state.params = {
			type,
			subtype
		};
		return await next();

	} catch (err) {
		ctx.throw(404, "/:subtype/:sub_type_param");
	}
};

const getArticleCulmativeDistances = async (ctx, next) => {
	try {
		const {
			sub_type_param
		} = ctx.params;
		const { articleData } = ctx.state;
		const { verseIndex } = articleData;

		if (verseIndex === 1) {
			articleData.articlesJson.cumulativeDistance = articleData.articlesJson.distanceWalked;
		} else {
			const distances = await connection.query(`SELECT articlesJson->>'$.distanceWalked' from articles WHERE sub_type_param = "${sub_type_param}" LIMIT ${parseInt(verseIndex, 10)}`)
			// move to a new function
			const cumulativeDistance =  distances[0].reduce((acc, valueObj) => {
				const value = Object.values(valueObj)
				acc += parseFloat(value);
				return acc
			}, 0)
			articleData.articlesJson.cumulativeDistance = cumulativeDistance;
		}

		return await next();

	} catch (err) {
		return await next();
	}
}

const getArticle = async (ctx, next) => {
	try {
		const {
			sub_type_param,
			articleParam
		} = ctx.params;

		// get the index from row and limit it by that?
		const [row] = await connection.query(`SELECT * from articles WHERE articleParam = "${articleParam}"`);
		if (!row.length) {
			throw new Error("Unable to find article");
		}

		const articleData = row[0];
		const { verseIndex } = articleData;
		const [prevAndNext] = await connection.query(`SELECT articleParam, article_title, verseIndex FROM articles WHERE sub_type_param = "${sub_type_param}" AND verseIndex = ${verseIndex - 1} OR   sub_type_param = "${sub_type_param}" AND verseIndex = ${verseIndex + 1}`);
		ctx.state.articleData = articleData;
		ctx.state.prevAndNext = prevAndNext;

		return await next();

	} catch (err) {
		console.log(err)
		ctx.throw(404, "article");
	}
};

const getArticleOnly = async (ctx, next) => {
	try {
		const {
			articleParam
		} = ctx.params;
		const [row] = await connection.query(`SELECT * from articles WHERE articleParam = "${articleParam}"`);

		if (!row.length) {
			throw new Error("Unable to find article");
		}

		ctx.state.articleData = row[0];
		return await next();

	} catch (err) {
		ctx.throw(404, "article");
	}
};


const getHomePageContent = async (ctx, next) => {
	try {

		const [row] = await connection.query("SELECT * FROM articles ORDER BY RAND() LIMIT 6");

		//todo fix this so we can change depending on env
		const [latest] = await connection.query("SELECT * FROM articles WHERE articleParam <> 'fakewalkparam'  ORDER BY articles_id DESC LIMIT 0,3");
		ctx.state.homePageContent = row;
		ctx.state.homePageLatest = latest;
		return await next();
	} catch (err) {

		ctx.state.homePageContent = null;
		ctx.state.homePageLatest = null;
		return await next();
	}


};

export {
	getAllWalks,
	getSubTypeHomePage,
	getArticle,
	getHomePageContent,
	getSubTypeHomePageOnly,
	getArticleOnly,
	getArticleCulmativeDistances
};
