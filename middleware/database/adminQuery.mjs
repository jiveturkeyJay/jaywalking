import { connection } from "./index.mjs";

// we need to put this into passport
const getLoginDetails = async (password, username) => {
	// todo check if username is in the return
	try {
		const [rows] = await connection.query(`SELECT * FROM users WHERE username = "${username}"`);
		if (!rows.length) {
			return false;
		} else {
			return rows[0];
		}

	} catch (err) {
		return false;
	}
};

const getAdminTypeData = async (ctx, next) => {
	try {
		const [rows] = await connection.query(`SELECT sub_type_id, sub_type_param, title from sub_types`);

		if (!rows.length) {
			throw new Error("Unable to find type data");
		}

		ctx.state.subTypeData = rows;
		return await next();

	} catch (err) {
		ctx.throw(404, "type");
	}
};

const getArticleTypeData = async (ctx, next) => {
	try {
		const [rows] = await connection.query(`select articleParam, title, article_title from articles;`);
		let sortedData;
		if (!rows.length) {
			throw new Error("Unable to find type data");
		} else {
			sortedData = rows.reduce((acc, row) => {

				if (!Reflect.has(acc, row.title)) {
					Reflect.set(acc, row.title, []);
				}

				const arr = Reflect.get(acc, row.title);
				arr.push(row);
				return acc;
			}, {});
		}

		ctx.state.getArticleTypeData = rows;
		ctx.state.sortedArticleData = sortedData;
		return await next();

	} catch (err) {
		//ctx.app.emit("error", new Error("404"));
		// this will go to error handler
		ctx.throw(404, "type");
	}
};

/**
 * calls for fetch
 */
const getSubTypesByType = async (ctx, next) => {
	const { type } = ctx.params;

	try {
		const [rows] = await connection.query(`SELECT sub_type_id, sub_type_param, title FROM sub_types WHERE sub_types.type_name = "${type}"`);
		if (!rows.length) {
			ctx.state.typeData = [];
		}

		ctx.state.typeData = rows;
		return await next();

	} catch (err) {
		ctx.state.typeData = new Error("Unable to collect data");
		return await next();
	}
};

const getAdminSubTypeData = async (ctx, next) => {
	const { articleParamData } = ctx.params;

	try {
		const [rows] = await connection.query(`SELECT articleParam, title, article_title FROM articles WHERE sub_type_param = "${articleParamData}"`);

		if (!rows.length) {
			ctx.state.articleParamData = [];
		}

		ctx.state.articleParamData = rows;
		return await next();

	} catch (err) {
		ctx.state.articleParamData = new Error("Unable to collect data");
		return await next();
	}
};

/**
 *
 * @param ctx
 * @param next
 * https://www.databasestar.com/mysql-json/
 */
const updateAdminArticle = async (ctx, next) => {
	const { adminUpdateArticleArray, articleParam } = ctx.state.adminUpdateArticle;

	try {
		if (adminUpdateArticleArray.length !== 0) {
			adminUpdateArticleArray.forEach((arr) => {
				let value = arr[1];

				let query;
				if (arr[0] === 'articlesJson') {
					value = JSON.stringify(arr[1]);
					query = `UPDATE articles SET ${arr[0]} = ${value} WHERE articleParam = "${articleParam}"`;
				} else {
					query = `UPDATE articles SET ${arr[0]} = "${value}" WHERE articleParam = "${articleParam}"`;
				}

				connection.query(query);
			});

			ctx.state.updateArticleSuccess = {
				error: false,
				update: true
			};
		} else {
			//nothing to update
			ctx.state.updateArticleSuccess = {
				error: false,
				update: null
			};
		}

	} catch (err) {
		ctx.state.updateArticleSuccess = { error: true, update: null };
	}

	return await next();
};

const updateAdminArticleHomePage = async (ctx, next) => {
	const { adminUpdateHomepageArray } = ctx.state;
	const { type, subtype} = ctx.params;

	try {
		if (adminUpdateHomepageArray.length !== 0) {
			adminUpdateHomepageArray.forEach((arr) => {
				let value = arr[1];

				let query;
				if (arr[0] === 'jsonData') {
					value = JSON.stringify(arr[1]);
					//await connection.query(`SELECT * from sub_types WHERE type_name = "walks" ORDER BY jsonData->>'$.dateStarted'`);
					query = `UPDATE sub_types SET ${arr[0]} = ${value} WHERE type_name = "${type}" AND sub_type_param = "${subtype}"`;
				} else {
					query = `UPDATE sub_types SET ${arr[0]} = "${value}" WHERE type_name = "${type}" AND sub_type_param = "${subtype}"`;
				}

				connection.query(query);
			});

			ctx.state.updateArticleHomepageSuccess = {
				error: false,
				update: true
			};
		} else {
			//nothing to update
			ctx.state.updateArticleHomepageSuccess = {
				error: false,
				update: null
			};
		}

	} catch (err) {
		ctx.state.updateArticleHomepageSuccess = { error: true, update: null };
	}

	return await next();
};

const createNewArticle = async (ctx, next) => {
	const { createNewArticle, formData } = ctx.state;
	const verseI = Reflect.get(formData, 'verseIndex');
	const subTypeParam = Reflect.get(formData, 'sub_type_param');
	const articlePar = Reflect.get(formData, 'articleParam');
	const tit = Reflect.get(formData, 'title');
	const articleTit = Reflect.get(formData, 'articleTitle');
	const articleJason = JSON.stringify(Reflect.get(formData, 'articlesJson'));
	if (createNewArticle) {
		try {
			const query = `INSERT into articles(articles_id, verseIndex, sub_type_param, articleParam, title, article_title, articlesJson)VALUES(null, ${parseInt(verseI, 10)},"${subTypeParam}", "${articlePar}", "${tit}", "${articleTit}", ${articleJason})`;
			await connection.query(query);
			ctx.state.newArticleAdded = true;
			ctx.state.articleLink = `/${formData.type}/${subTypeParam}/${articlePar}`;

		} catch (err) {
			ctx.state.newArticleAdded = 'error';
		}

	} else {
		ctx.state.newArticleAdded = 'unable';
	}

	return await next();
};

const deleteFakeArticle = async (ctx, next) => {
	try {
		const query = `DELETE from articles WHERE articleParam = "fakewalkparam2"`;
		await connection.query(query);

	} catch (err) {
		console.log(err);
	}

	return await next();
};


export {
	deleteFakeArticle,
	getAdminTypeData,
	getArticleTypeData,
	getSubTypesByType,
	getAdminSubTypeData,
	updateAdminArticle,
	updateAdminArticleHomePage,
	createNewArticle,
	getLoginDetails
};
