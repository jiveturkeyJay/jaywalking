import mysql from 'mysql2/promise';
import path from 'path';
import { getConfig } from '../../config.mjs';

// need to do this twice
// once in the server file
// seems this module gets called too early
const __dirname = path.resolve(path.dirname(''));
getConfig(__dirname);

const connection = await mysql.createConnection({
  host: process.env.HOST,
  user: process.env.DBUSER,
  password: process.env.PW,
  database: process.env.DB
});

connection.connect(function(err) {
  if (err) {
    throw err;
  }
  console.log('Connected!');
});

export { connection };
