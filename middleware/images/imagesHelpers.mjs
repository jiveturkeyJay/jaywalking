import fs from "fs";
import path from "path";
import { createdImagesFolder, imageSizes, imageSizesName, jsonItems, holdingFolder } from "../../constants/index.mjs";
import Sharp from "sharp";

const __dirname = path.resolve(path.dirname(""));

/**
 * moves image to holding folder
 * @param item
 * @return {Promise<void>}
 */
const copyImage = async (item) => {
	await fs.copyFile(item.filepath, `${holdingFolder}/${item.originalFilename}`, (err) => {
		return !err;
	});
};

/**
 * gets data from json file and puts in page
 * @param ctx
 * @param next
 * @return {Promise<void>}
 */
const getJsonFiles = async (ctx, next) => {
	try {
		const prefix = ctx.query.prefix;
		ctx.state.jsonData = [];
		ctx.state.prefix = prefix;

		const dirName = path.join(__dirname, jsonItems);
		let filenames = fs.readdirSync(dirName);
		filenames.forEach((file) => {
			if (file.indexOf(prefix) !== -1) {
				fs.readFile(`${dirName}/${file}`, "utf8", (error, data) => {
					if (error) {
						return;
					}

					ctx.state.jsonData.push(JSON.parse(data));
				});
			}
		});
		await next();
	} catch (err) {
		await next();
	}
};

const renameAndResizeImage = async (imageName, newName, prefix, folderName) => {
	try {
		// make a directory if it does not exist
		const folderToSavePhotos = path.join(__dirname, folderName);
		if (!fs.existsSync(folderToSavePhotos)) {
			fs.mkdirSync(folderToSavePhotos);
		}

		const imagePath = path.join(__dirname, holdingFolder, imageName);
		// exclude non files
		if (imageName.toLowerCase().indexOf(".jpg") === -1) {
			return;
		}

		if (fs.existsSync(imagePath)) {
			const image = Sharp(imagePath);
			for (let i = 0; i < imageSizesName.length; i++) {
				const size = imageSizesName[i];
				try {
					await image.rotate().resize({
						width: imageSizes[size][0],
						height: imageSizes[size][1]
					}).jpeg({ quality: 85 }).toFile(`${createdImagesFolder}/${prefix}_${newName}_${size}.jpg`);
				} catch (err) {
					throw new Error(err.message);
				}
			}
		}

		return true;
	} catch (err) {
		return new Error("Your mum is fat");
	}
};

// need to make another array that will delete the images
// that do not have their checkbox selected
// so maybe create an or object that sets a flag
const createParsedData = (formData) => {
	return Object.entries(formData).reduce((acc, entry) => {
		const split = entry[0].split("-");
		const index = split[1];
		const isChecked = Reflect.get(formData, `checkbox-${index}`) === "on";

		if (isChecked) {
			// get the number
			// get the name
			const name = split[0].trim();
			// get the value of the prop
			const value = entry[1].trim();
			if (index) {
				if (Reflect.has(acc, index)) {
					const obj = Reflect.get(acc, index);
					Reflect.set(obj, name, value);
					return acc;
				} else {
					// create the object
					const newObj = {};
					Reflect.set(newObj, name, value);
					Reflect.set(acc, index, newObj);
					return acc;
				}
			}
		}

		return acc;
	}, {});
};

/**
 * gets the images to upload
 * @param ctx
 * @param next
 * @return {Promise<void>}
 */
const displayCreatedImages = async (ctx, next) => {
	try {
		const prefix = ctx.query.prefix;
		ctx.state.successData = fs.readdirSync(path.join(__dirname, createdImagesFolder)).filter((n) => n.indexOf(".jpg") !== -1 && n.indexOf(prefix) !== -1);

		await next();
	} catch (err) {
		ctx.state.errorData = true;
	}
};

const deleteFile = async (filePath) => {
	if (fs.existsSync(filePath)) {
		fs.unlink(filePath, (err) => {
			if (err) {
				return new Error(err.message);
			}
		});
	}
};

/**
 * gets images that have been copied
 * and displays them along with input fields to allow
 * names, alt and caption data to be added
 *
 * @param ctx
 * @param next
 * @return {Promise<void>}
 */
const getBaseImagesToTransform = async (ctx, next) => {
	ctx.state.copiedImages = {};
	ctx.state.copiedImages = await fs.readdirSync(path.join(__dirname, holdingFolder));
	await next();
};

/**
 * takes a set of files and associated data and
 * creates a number of resized images and a json.file
 * that can be used in the article data
 *
 * @param ctx
 * @param next
 * @return {Promise<void>}
 */
const createSizedImages = async (ctx, next) => {
	const jsonDataArray = [];
	const formData = ctx.request.body;
	const { prefix } = formData;
	ctx.state.prefix = prefix;
	ctx.state.createdImagesError = false;
	const parsedData = createParsedData(formData);
	// need to check the images that are not checked
	for (const value of Object.values(parsedData)) {
		const {
			fileName,
			name,
			alt,
			caption
		} = value;
		const json = {
			name: `${prefix}_${name.trim()}`,
			alt,
			caption
		};

		jsonDataArray.push(json);
		try {
			await renameAndResizeImage(fileName, name, prefix, createdImagesFolder);
		} catch (err) {
			ctx.state.createdImagesError = true;
		}
	}

	ctx.state.jsonDataArray = jsonDataArray;
	await next();
};

const createJsonFile = async (ctx, next) => {
	const { jsonDataArray, prefix } = ctx.state;
	const jsonPath = path.join(__dirname, jsonItems);
	let jsonArray = [];
	while (jsonDataArray.length) {
		jsonArray.push({
			type: "picture",
			value: jsonDataArray.splice(0, 2)
		});

	}
	if (!fs.existsSync(jsonPath)) {
		fs.mkdirSync(jsonPath);
	}

	const jsonFile = `${jsonPath}/${prefix}.json`;
	await deleteFile(jsonFile);

	fs.appendFile(jsonFile, JSON.stringify(jsonArray, null, 4), "utf8", function(err) {
		if (err) {
			return err;
		}
	});

	await next();
};

/**
 * when we get the /selectImages we remove any existing files
 * that have been uploaded
 * @param ctx
 * @param next
 * @return {Promise<void>}
 */
const removeUploadedImages = async (ctx, next) => {
	const existingImages = fs.readdirSync(path.join(__dirname, holdingFolder));
	if (existingImages.length) {
		await existingImages.forEach((file) => {
			const deletePath = path.join(__dirname, holdingFolder, file);
			deleteFile(deletePath);
		});
	}
	await next();
};

const uploadSelectedImages = async (ctx, next) => {
	try {
		let files = ctx.request.files.file;
		console.log('This are files ', files);
		ctx.state.imageState = {
			errors: []
		};
		if (!(files instanceof Array)) {
			files = new Array(files);
		}
		// moves images to /public/holding ready for prep
		const imageUploadData = [];
		ctx.state.imageState.uploaded = imageUploadData;
		await files.forEach(async (file) => {
			console.log(file)
			try {
				await copyImage(file);
				imageUploadData.push(`Success upload: ${file.name}`);
			} catch (e) {
				imageUploadData.push(`Fail upload: ${file.name}`);
			}
		});

		await next();
	} catch (err) {
		ctx.state.imageState.errors.push({ upload: `Upload of selected images errors: ${err}` });
	}
};

export {
	copyImage,
	removeUploadedImages,
	getBaseImagesToTransform,
	displayCreatedImages,
	renameAndResizeImage,
	createParsedData,
	getJsonFiles,
	createSizedImages,
	createJsonFile,
	uploadSelectedImages
};
