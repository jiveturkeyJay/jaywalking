import { newArticleKeys } from '../../constants/index.mjs';

// check that all the data is there but don't go overboard
const checkAdminAddNewArticle = async (ctx, next) => {
	const formData = ctx.request.body;
	const every = newArticleKeys.every((key) => {
		const data = Reflect.get(formData, key);
		return data && data.length > 0;
	});

	ctx.state.formData = formData;
	ctx.state.createNewArticle = !!every;
	await next();
};

export {
	checkAdminAddNewArticle
};
