import { adminUpdateArticleChecks } from "../../constants/index.mjs";

/**
 * takes formData and checks what has changed and creates a object
 * of table columns that need to be updated
 * @param ctx
 * @param next
 * @return {Promise<void>}
 *
 * UPDATE table_name SET column1 = value1, column2 = value2 WHERE condition;
 *
 */
const createUpdateArticleData = async (ctx, next) => {
	const formData = ctx.request.body;

	const adminUpdateArticleArray = adminUpdateArticleChecks.reduce((acc, prop) => {
		const original = Reflect.get(formData, `original_${prop}`);
		const updated = Reflect.get(formData, prop);
		if (original !== updated) {
			acc.push([prop, updated]);
		}
		return acc;
	}, []);

	ctx.state.adminUpdateArticle = {
		adminUpdateArticleArray,
		articleParam: formData.articleParam
	};

	await next();
};
export { createUpdateArticleData };
