// todo test
import { adminUpdateHomepageChecks } from "../../constants/index.mjs";

const createUpdateHomeData = async (ctx, next) => {

		const formData = ctx.request.body;
		ctx.state.adminUpdateHomepageArray = adminUpdateHomepageChecks.reduce((acc, prop) => {
			const original = Reflect.get(formData, `original_${prop}`);
			const updated = Reflect.get(formData, prop);
			if (original !== updated) {
				acc.push([prop, updated]);
			}
			return acc;
		}, []);

		await next();
};
export { createUpdateHomeData };
