import { urls, adminItemtoRender } from "../../constants/index.mjs";

/**
 * middleware to determine which data to fetch
 * and whether it is a new item or an existing one to
 * update
 * we can update - homepage eg walks/northdownsway
 * or create a new one - we go to (redirect) admin/new/walks
 *
 * or we can update an article page eg walks/northdownsway/blahtoblah
 * or create a new one - we go to (redirect) admin/new/walks/northdownsway
 *
 * fetch the data (if any) and return it, plus the template to render
 *
 * @param ctx
 * @param next
 * @return {Promise<void>}
 *
 * this is tested by a little hacking but it's hard to test middleware
 */
const checkAdminUpdateBody = async (ctx, next) => {

	const formData = ctx.request.body;
	const {
		updateOrAddNew,
		type,
		subTypeParam,
		articleParam
	} = formData;

	const returnData = {
		toRender: null,
		toRedirect: null
	};

	try {
		if (!type || !subTypeParam) {
			returnData.toRender = Reflect.get(urls, "adminError");
		} else {
			let redirectStr = `/admin/${updateOrAddNew}/${type}/${subTypeParam}`;
			// if new
			if (updateOrAddNew !== "new") {

				if (articleParam) {
					// done
					Reflect.set(returnData, "toRender", adminItemtoRender.admin_update_article);
					Reflect.set(returnData, "toRedirect", `${redirectStr}/${articleParam}`);
				} else {
					Reflect.set(returnData, "toRender", adminItemtoRender.admin_update_type);
					Reflect.set(returnData, "toRedirect", `${redirectStr}/homepage`);
				}
			} else {
				// we dont have article param
				if (subTypeParam) {
					Reflect.set(returnData, "toRender", adminItemtoRender.admin_new_article);
					Reflect.set(returnData, "toRedirect", `${redirectStr}/article`);
				} else {
					Reflect.set(returnData, "toRender", adminItemtoRender.admin_new_type);
					Reflect.set(returnData, "toRedirect", `/admin/${updateOrAddNew}/${type}/homepage`);
				}
			}
		}

		// else update
		ctx.state.returnData = returnData;
		return await next();
	} catch (err) {

		ctx.state.returnData = "fat";
		await next();
	}
};


export {
	checkAdminUpdateBody
};
