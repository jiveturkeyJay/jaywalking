const gearlistuploadValidate = async (ctx) => {
	const formData = ctx.request.body;
	if (!formData.gearlist) {
		return await ctx.redirect('/gearlistupload?hasGearListError=true');
	} else {
		return await ctx.redirect(`/gearlistrender?gearlist=${formData.gearlist}`);
	}

}

export { gearlistuploadValidate }
