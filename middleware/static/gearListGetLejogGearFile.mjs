import { gearListFolder } from '../../constants/index.mjs';
import path from "path";
import fs from "fs";

const __dirname = path.resolve(path.dirname(""));

/**
 * put this into another piece of mw or just one to get the query
 * @param ctx
 * @param next
 * @returns {Promise<*>}
 */
const gearListLejogGetGearFile = async (ctx, next) => {
	const fileName = 'lejog.json';
	const dirName = path.join(__dirname, gearListFolder);

	try {
		ctx.state.gearListData = fs.readFileSync(`${dirName}/${fileName}`, 'utf8');
		return await next();
	} catch (err) {
		console.log(err);
	}
}

export {
	gearListLejogGetGearFile
}
