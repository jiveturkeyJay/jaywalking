import { gearListFolder } from '../../constants/index.mjs';
import path from "path";
import fs from "fs";

const __dirname = path.resolve(path.dirname(""));
const gearListGetGearFile = async (ctx, next) => {
	const fileName = ctx.query.gearlist;
	const dirName = path.join(__dirname, gearListFolder);

	try {
		const gearListData = fs.readFileSync(`${dirName}/${fileName}`, 'utf8');
		ctx.state.gearListData = gearListData;
		return await next();
	} catch (err) {
		console.log(err);
	}
}

export {
	gearListGetGearFile
}
