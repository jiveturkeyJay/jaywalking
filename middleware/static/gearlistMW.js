const gearlistMW = async (ctx, next) => {

	//todo add test
	// move this to the ejs file
	const totals= {
		heading: "Total weight carried and worn",
		type: 'totals',
		items: {
			ruckSackWeight: {
				item: "Weight in rucksack",
				weight: "",
				notes: "Weight in the rucksack"
			},
			wornWeight: {
				item: "Worn weight",
				weight: "",
				notes: "Tablet, clothes,boots etc"
			},
			totalWeight: {
				item: "Total Weight",
				weight: "",
				notes: "Everything (excluding food and water)"
			}
		}
	};

	let overAllWeight = 0;
	let wornWeightTotal = 0;

	// todo fix
	const parsedgearListData = JSON.parse(ctx.state.gearListData)
	parsedgearListData.forEach((gearType) => {
		const { items } = gearType;
		const keys = Reflect.ownKeys(items);
		let total = 0;

		keys.forEach((key) => {
			const gearItem = Reflect.get(items, key);
			const {weight, isWorn, isFraction = 1} = gearItem;
			total = total + parseInt(weight, 10);
			if (isWorn) {
				wornWeightTotal = wornWeightTotal + parseInt(weight, 10)/isFraction;
			}
		});

		gearType.totalweight = total;
		overAllWeight = overAllWeight + total;
	});

	const { totalWeight, wornWeight, ruckSackWeight } = totals.items;

	Reflect.set(totalWeight, 'weight', overAllWeight);
	Reflect.set(wornWeight, 'weight', wornWeightTotal);
	Reflect.set(ruckSackWeight, 'weight', overAllWeight - wornWeightTotal);
	parsedgearListData.push(totals);

	ctx.state.gearListData = parsedgearListData;
	await next();
};

export {
	gearlistMW
}
