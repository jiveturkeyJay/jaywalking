import { checkAdminUpdateBody } from "../admin/checkAdminUpdateBody.mjs";

describe("checkAdminUpdateBody", () => {
	let next;
	let ctx;
	let body;
	beforeEach(() => {
		body = {};
		ctx = {
			state: {},
			request: {
				body
			}
		};

		next = sinon.spy();
	});

	afterEach(() => {
		next = null;
		ctx = {};
		body = {};
	});

	it("should do call next", () => {
		ctx = {
			state: {},
			request: {
				body: {
					updateOrAddNew: "new",
					type: "walks",
					subTypeParam: "1066countrywalk",
					articleParam: "battletopevensey"
				}
			}
		};
		checkAdminUpdateBody(ctx, next);
		expect(next.callCount).to.equal(1);
	});

	it("should set state.toRender to an error page when there is no type param", () => {
		ctx.request.body = {};
		checkAdminUpdateBody(ctx, next);
		expect(ctx.state.returnData.toRender).to.equal("admin/error");
		expect(next.callCount).to.equal(1);
	});

	it("should set state.toRender to an error page when there is no subTypeParam param", () => {
		ctx.request.body = {
			updateOrAddNew: "new",
			type: "walks",
			subTypeParam: "",
			articleParam: "battletopevensey"
		};

		checkAdminUpdateBody(ctx, next);
		expect(ctx.state.returnData.toRender).to.equal("admin/error");
		expect(next.callCount).to.equal(1);
	});

	describe("No article param", () => {
		it("should pass the correct render when the is a type and subTypeParam params", () => {
			ctx.request.body = {
				updateOrAddNew: "new",
				type: "walks",
				subTypeParam: "northdownsway",
				articleParam: ""
			};

			checkAdminUpdateBody(ctx, next);
			expect(ctx.state.returnData.toRender).to.equal("admin/new_article");
			expect(ctx.state.returnData.toRedirect).to.equal("/admin/new/walks/northdownsway/article");
		});

		it("should pass the correct render when the is a type and subTypeParam params", () => {
			ctx.request.body = {
				updateOrAddNew: "update",
				type: "walks",
				subTypeParam: "northdownsway",
				articleParam: ""
			};

			checkAdminUpdateBody(ctx, next);
			expect(ctx.state.returnData.toRender).to.equal("admin/update_type");
			expect(ctx.state.returnData.toRedirect).to.equal("/admin/update/walks/northdownsway/homepage");
		});
	});

	describe("With article param", () => {
		it("should pass the correct render and redirect", () => {
			ctx.request.body = {
				updateOrAddNew: "new",
				type: "walks",
				subTypeParam: "northdownsway",
				articleParam: "somenewparam"
			};

			checkAdminUpdateBody(ctx, next);
			expect(ctx.state.returnData.toRender).to.equal("admin/new_article");
			expect(ctx.state.returnData.toRedirect).to.equal("/admin/new/walks/northdownsway/article");
		});

		it("should pass the correct render and redirect", () => {
			ctx.request.body = {
				updateOrAddNew: "update",
				type: "walks",
				subTypeParam: "northdownsway",
				articleParam: "somenewparam"
			};

			checkAdminUpdateBody(ctx, next);
			expect(ctx.state.returnData.toRender).to.equal("admin/update_article");
			expect(ctx.state.returnData.toRedirect).to.equal("/admin/update/walks/northdownsway/somenewparam");
		});
	});

});
