import { createUpdateHomeData } from '../admin/createUpdateHomeData.mjs';

describe('createUpdateHomeData', () => {
	let formData;
	let ctx;
	let next;

	beforeEach(() => {
		formData = {
			original_title: 'Original title',
			title: 'Original title',
			original_jsonData: 'json data',
			jsonData: 'json data'
		};

		ctx = {
			state: {},
			request: {
				body: formData
			}
		};

		next = sinon.spy();
	});

	it("should call next", () => {
		createUpdateHomeData(ctx, next);
		expect(next.callCount).to.equal(1);
	});

	it('should have an empty array when no changes to form data', () => {
		createUpdateHomeData(ctx, next);
		expect(ctx.state.adminUpdateHomepageArray).to.eql([]);
	});

	it('should have an array of changes when changes to form data', () => {
		ctx.request.body = {
			...formData,
			title: 'Updated title',
			jsonData: 'Updated Json data'
		};

		createUpdateHomeData(ctx, next);
		expect(ctx.state.adminUpdateHomepageArray).to.eql([
			[
				'title', 'Updated title'
			], [
				'jsonData', 'Updated Json data'
			]]);
	});

});
