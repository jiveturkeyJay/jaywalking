import { createUpdateArticleData } from "../admin/createUpdateArticleData.mjs";

describe("checkAdminUpdateBody", () => {

	let formData;
	let ctx;
	let next;
	beforeEach(() => {
		formData = {
			articleParam: "fakewalkparam",
			original_articleParam: "fakewalkparam",
			article_title: "somewhere to somewhere",
			original_article_title: "somewhere to somewhere",
			articlesJson: "{\"article\": \"some article data changed\"}",
			original_articlesJson: "{\"article\": \"some article data original\"}"
		};

		ctx = {
			state: {},
			request: {
				body: formData
			}
		};

		next = sinon.spy();
	});

	it("should call next", () => {
		createUpdateArticleData(ctx, next);
		expect(next.callCount).to.equal(1);
	});

	it("should have an empty array when no changes", () => {
		ctx.request.body.articlesJson = formData.original_articlesJson;
		createUpdateArticleData(ctx, next);
		expect(ctx.state.adminUpdateArticle.undateQuery).to.equal(undefined);
	});

	// this works

	// https://codingstatus.com/how-to-update-data-using-node-js-and-mysql/
	// https://forum.freecodecamp.org/t/nodejs-mysql-prepared-statements/426216
	// https://stackoverflow.com/questions/61103430/multiple-statements-in-mysql2-nodejs
	// https://stackoverflow.com/questions/25552115/updating-multiple-rows-with-node-mysql-nodejs-and-q

	// UPDATE articles SET articlesJson = '{"article": "some article data changed"}' WHERE articleParam = 'fakewalkparam';
	// or try
	// UPDATE tableName SET jsonData = JSON_SET(jsonData, '$.choices.answers', 20) WHERE data->'$.choices.choice' = 18 AND id = 1;
	it("should create the correct ctx state when one item has changed", () => {
		createUpdateArticleData(ctx, next);

		const { adminUpdateArticleArray, articleParam } = ctx.state.adminUpdateArticle;
		expect(adminUpdateArticleArray[0][0]).to.equal("articlesJson");
		expect(adminUpdateArticleArray[0][1]).to.equal('{"article": "some article data changed"}');
		expect(articleParam).to.equal('fakewalkparam');

	});

	it('should create the correct ctx state when more than one item has changed', () => {
		ctx.request.body.article_title = 'somewhere to somewhere updated';
		createUpdateArticleData(ctx, next);
		const { adminUpdateArticleArray, articleParam } = ctx.state.adminUpdateArticle;

		expect(adminUpdateArticleArray[0][0]).to.equal("article_title");
		expect(adminUpdateArticleArray[0][1]).to.equal('somewhere to somewhere updated');
		expect(adminUpdateArticleArray[1][0]).to.equal("articlesJson");
		expect(adminUpdateArticleArray[1][1]).to.equal('{"article": "some article data changed"}');
		expect(articleParam).to.equal('fakewalkparam');
	});
});
