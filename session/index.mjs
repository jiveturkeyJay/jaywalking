
const clearItemFromSession = (session, prop, value = null) => {
	Reflect.set(session, prop, value);
};

const cleanContactSessionData = (session) => {
	clearItemFromSession(session, 'formData', null);
	clearItemFromSession(session, 'formErrors', null);
	return session
}

const addItemToSession = (session, prop, value) => {
	Reflect.set(session, prop, value);
};

const addQueryToSession = async (ctx, next) => {
	const {query, session} = ctx;
	const queryKeys = Reflect.ownKeys(query)

	if (queryKeys && queryKeys.length > 0) {
		queryKeys.forEach((key) => {
			const value = Reflect.get(query, key)
			addItemToSession(session, key, value)
		});
	}
	return await next()

}

const clearSession = async (ctx, next) => {
	ctx.session = {};
	return await next()
}

export {
	clearItemFromSession,
	clearSession,
	addQueryToSession,
	cleanContactSessionData
}
