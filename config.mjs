import path from 'path';
import dotenv from 'dotenv';
import 'dotenv/config';

// we only have one config now
const getConfig = (__dirname) => {
  dotenv.config({
    path: path.resolve(__dirname, '.env')
  });
};

export { getConfig };



