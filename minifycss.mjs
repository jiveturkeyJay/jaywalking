import fs from 'fs';

const input = './public/styles';
const output = './public/stylemin';

// todo get folder name and use as name
function minimizeData(_content) {
    let content = _content;
    content = content.replace(/\/\*(?:(?!\*\/)[\s\S])*\*\/|[\r\n\t]+/g, '');
    // now all comments, newlines and tabs have been removed
    content = content.replace(/ {2,}/g, ' ');
    // now there are no more than single adjacent spaces left
    // now unnecessary: content = content.replace( /(\s)+\./g, ' .' );
    content = content.replace(/ ([{:}]) /g, '$1');
    content = content.replace(/([;,]) /g, '$1');
    content = content.replace(/ !/g, '!');
    return content;
}

let fileArr = [];

fs.readdirSync(input).forEach(file => {
    if (file.endsWith('.css')) {
        fileArr.push(file);
    }
});

async function getFileAndTrim(file) {
    try {
        const bobble = await fs.readFileSync(input + '/' + file, 'utf-8');
        return bobble.toString().trim();

    } catch (error) {
        console.error('there was an error getting or trimming css file:', error.message);
    }
}

const acc = [];
fileArr.forEach(async (file, i) => {
    const cssString = await getFileAndTrim(file);
    acc.push(cssString);
    if (i === fileArr.length - 1) {
        const newString = minimizeData(acc.join(''));

        fs.writeFile(output + '/style.min.css', newString, (err) => {
            if (err) throw err;
            console.log('The Minified CSS file has been saved!');
        });
    }
});


