import koaRouter from "koa-router";

import {
	getAdminTypeData,
	getArticleTypeData,
	getSubTypesByType,
	getAdminSubTypeData,
	updateAdminArticle,
	createNewArticle,
	deleteFakeArticle,
	updateAdminArticleHomePage
} from "../middleware/database/adminQuery.mjs";
import { getSubTypeHomePageOnly, getArticleOnly } from "../middleware/database/query.mjs";
import { checkAdminUpdateBody, checkAdminAddNewArticle, createUpdateArticleData, createUpdateHomeData } from "../middleware/index.mjs";

import { adminArticlesShowAsText, updateHomePageShowAsText } from "../constants/index.mjs";

const adminRouter = new koaRouter({
	prefix: "/admin"
});

adminRouter.get("/", getAdminTypeData, getArticleTypeData, (ctx) => {
	const { subTypeData, getArticleTypeData, sortedArticleData } = ctx.state;
	return ctx.render("admin/home", {
		seoDescription: "Admin",
		pageTitle: "Admin: Home",
		titleParam: "Admin",
		verseTitle: "Home",
		data: {
			subTypeData,
			getArticleTypeData,
			sortedArticleData,
			sortedArticleDataKeys: Reflect.ownKeys(sortedArticleData)
		},
		scripts: ["admin/fetching"]
	});
});

adminRouter.get("/testdata/:type", getSubTypesByType, (ctx) => {
	const { typeData } = ctx.state;
	ctx.body = JSON.stringify(typeData);
	return ctx.body;
});

adminRouter.get("/articledata/:articleParamData", getAdminSubTypeData, (ctx) => {
	const { articleParamData } = ctx.state;
	ctx.body = JSON.stringify(articleParamData);
	return ctx.body;
});

/**
 * takes a post and sorts out where to redirect it
 * which then renders to either an update [article or type]
 * or new [article or type]
 */
adminRouter.post("/updateOrAddNew", checkAdminUpdateBody, async (ctx) => {
	const { toRedirect } = ctx.state.returnData;
	return await ctx.redirect(toRedirect);
});

/**
 * todo
 * 	go from here and get the data for updating a page such as
 *	/walks/northdownsway
 */
// we can change these and just pass the params in state
// so we could have /admin/new-article
adminRouter.get('/new/:type/:subtype/article', (ctx) => {
	return ctx.render("admin/newArticle", {
		seoDescription: "Admin",
		pageTitle: "Update Article",
		titleParam: "Admin",
		verseTitle: "new article",
		urlParams: ["titleParam", "verseTitle"],
		params: ctx.params
	});
});

// post data for new article
// assume data is there as we have validation (basic)
adminRouter.post('/new/article', checkAdminAddNewArticle, createNewArticle, (ctx) => {
	const { articleLink, newArticleAdded } = ctx.state;

	return ctx.render("admin/newArticleSuccess", {
		seoDescription: "Admin",
		pageTitle: "Update Article",
		titleParam: "Admin",
		verseTitle: "New article success",
		urlParams: ["titleParam", "verseTitle"],
		newArticleAdded,
		articleLink
	});
});

// route to delete the test data for adding new route
adminRouter.get('/delete/fakearticle', deleteFakeArticle, async (ctx) => {
	return await ctx.redirect('/admin');
});

adminRouter.get("/update/:type/:subtype", (ctx) => {
	const { articleData } = ctx.state;

	// we can get the article title from the data
	return ctx.render("admin/updateArticle", {
		seoDescription: "Admin",
		pageTitle: "Update Article",
		titleParam: "Admin",
		verseTitle: articleData.title,
		urlParams: ["titleParam", "pageTitle", "verseTitle"],
		params: ctx.params
	});
});


/**
 * update article homepage start
 */
adminRouter.get("/update/:type/:subtype/homepage", getSubTypeHomePageOnly, (ctx) => {
	const { homepage, params } = ctx.state;
	return ctx.render("admin/updateHomepage", {
		seoDescription: "Admin",
		pageTitle: "Admin: Update home page",
		titleParam: "Admin",
		verseTitle: "Home",
		updateHomePageShowAsText,
		homepage,
		params
	});
});

adminRouter.post("/update/:type/:subtype/homepage", createUpdateHomeData, updateAdminArticleHomePage,(ctx) => {
	const { type, subtype } = ctx.params;

	return ctx.render("admin/updateArticleHomepageSuccess", {
		seoDescription: "Admin",
		pageTitle: "Update Article Homepage",
		titleParam: "Admin",
		verseTitle: "Success or failure",
		urlParams: ["titleParam", "pageTitle", "verseTitle"],
		link: `${type}/${subtype}`
	});
});

/**
 * update article start get and post
 */
// probably don't need the breadcrumb as we really only need to go to Admin / Update Article
adminRouter.get("/update/:type/:subtype/:articleParam", getArticleOnly, (ctx) => {
	const { articleData } = ctx.state;

	// we can get the article title from the data
	return ctx.render("admin/updateArticle", {
		seoDescription: "Admin",
		pageTitle: "Update Article",
		titleParam: "Admin",
		verseTitle: articleData.title,
		urlParams: ["titleParam", "pageTitle", "verseTitle"],
		another: articleData.title,
		articleData,
		adminArticlesShowAsText,
		params: ctx.params
	});
});

/**
 * need to check the data that we have and compare it to the original
 * and create a query based on that.
 */
adminRouter.post("/update/:type/:subtype/:articleParam", createUpdateArticleData, updateAdminArticle, (ctx) => {
	// could add a link to view the page
	// todo fix the path this goes to it is the same as the previous form
	return ctx.render("admin/updateArticleSuccess", {
		seoDescription: "Admin",
		pageTitle: "Update Article",
		titleParam: "Admin",
		verseTitle: "Success or failure",
		urlParams: ["titleParam", "pageTitle", "verseTitle"],
		link: `${ctx.params.type}/${ctx.params.subtype}/${ctx.params.articleParam}`
	});
});

export {
	adminRouter
};
