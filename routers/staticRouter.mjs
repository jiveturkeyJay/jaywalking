import koaRouter from "koa-router";
import { gearlistMW } from "../middleware/static/gearlistMW.js";
import { gearlistuploadValidate } from "../middleware/static/gearlistuploadValidate.mjs";
import { gearListGetGearFile } from "../middleware/static/gearListGetGearFile.mjs";
import { gearListLejogGetGearFile } from "../middleware/static/gearListGetLejogGearFile.mjs";

const staticRouter = new koaRouter();

staticRouter.get("/static/doing", (ctx) => {
	return ctx.render("lejog/doing", {
		seoDescription: "Doing the walk from Lands End to John O'Grouts",
		pageTitle: "Lejog: Doing the walk",
		titleParam: "Lejog",
		verseTitle: "Doing the walk",
		subtype: "walks",
		urlParams: ["subtype", "titleParam", "verseTitle"],
		urlArr: ["walks", "lejog", "verseTitle"]
	});
});

staticRouter.get("/static/rules", (ctx) => {
	return ctx.render("lejog/rules", {
		seoDescription: "My rules for walking from Lands End to John O'Grouts",
		pageTitle: "Lejog: Walking Rules",
		titleParam: "Lejog",
		verseTitle: "Walking rules",
		subtype: "walks",
		urlParams: ["subtype", "titleParam", "verseTitle"],
		urlArr: ["walks", "lejog", "verseTitle"]
	});
});

// okay this isn't a static piece of data anymore
staticRouter.get("/static/gearlist", gearListLejogGetGearFile, gearlistMW, (ctx) => {
	const { gearListData } = ctx.state;
	return ctx.render("lejog/gearlist", {
		seoDescription: "My gear lost for my walk from Lands End to John O'Grouts",
		pageTitle: "Lejog: Gear list",
		titleParam: "Lejog",
		verseTitle: "Gear list",
		data: gearListData,
		subtype: "walks",
		urlParams: ["subtype", "titleParam", "verseTitle"],
		urlArr: ["walks", "lejog", "verseTitle"]
	});
});

staticRouter.get('/gearlistupload', (ctx) => {
	let hasGearListError = false;
	if (ctx.query.hasGearListError) {
		hasGearListError = true;
	}

	return ctx.render("staticpages/gearlistupload", {
		seoDescription: "",
		pageTitle: "Gear list",
		titleParam: "Upload",
		verseTitle: "Gear list",
		subtype: "walks",
		urlParams: ["subtype", "titleParam", "verseTitle"],
		urlArr: ["walks"],
		hasGearListError
	});
})

staticRouter.get('/gearlistrender', gearListGetGearFile, gearlistMW, (ctx) => {
	const { gearListData } = ctx.state;
	return ctx.render("lejog/gearlist", {
		seoDescription: "My gear lost for my walk from Lands End to John O'Grouts",
		pageTitle: "Lejog: Gear list",
		titleParam: "Lejog",
		verseTitle: "Gear list",
		data: gearListData,
		subtype: "walks",
		urlParams: ["subtype", "titleParam", "verseTitle"],
		urlArr: ["walks", "lejog", "verseTitle"]
	});
})

staticRouter.post('/gearlistupload',  gearlistuploadValidate, (ctx) => {
	return ctx.render("staticpages/gearlistupload", {
		seoDescription: "",
		pageTitle: "Gear list",
		titleParam: "Upload",
		verseTitle: "Gear list",
		subtype: "walks",
		urlParams: ["subtype", "titleParam", "verseTitle"],
		urlArr: ["walks"]
	});
})

staticRouter.get("/static/why", (ctx) => {
	return ctx.render("lejog/why", {
		seoDescription: "Why am I walking from Lands End to John O'Grouts",
		pageTitle: "Lejog: Why walk it",
		titleParam: "Lejog",
		verseTitle: "Why Lejog",
		subtype: "walks",
		urlParams: ["subtype", "titleParam", "verseTitle"],
		urlArr: ["walks", "lejog", "verseTitle"]
	});
});

staticRouter.get("/static/route", (ctx) => {
	return ctx.render("lejog/route", {
		seoDescription: "Details of my intended route from Lands End to John O'Grouts",
		pageTitle: "Lejog: My route",
		titleParam: "Lejog",
		verseTitle: "Lejog Route",
		subtype: "walks",
		urlParams: ["subtype", "titleParam", "verseTitle"],
		urlArr: ["walks", "lejog", "verseTitle"]
	});
});

staticRouter.get("/accessibility", (ctx) => {
	return ctx.render("staticpages/accessibility", {
		seoDescription: "Jay Walking accessibility statement",
		pageTitle: "Accessibility Statement",
		titleParam: "Accessibility Statement",
		urlParams: ["titleParam"]
	});
});

staticRouter.get("/privacy", (ctx) => {
	return ctx.render("staticpages/privacy", {
		seoDescription: "Some Seo",
		pageTitle: "Privacy Policy",
		titleParam: "Privacy Policy",
		urlParams: ["titleParam"]
	});
});

/**
 * static pages need to be registered first
 */
staticRouter.get("/contactsuccess", (ctx) => {
	return ctx.render("staticpages/contactsuccess", {
		seoDescription: "Jaywalking contact details",
		pageTitle: "Contact form successfully sent",
		titleParam: "Contact success",
		urlParams: ["titleParam"]
	});
});

staticRouter.get("/contactfail", (ctx) => {
	return ctx.render("staticpages/contactfail", {
		seoDescription: "Jaywalking contact details",
		pageTitle: "Contact failure",
		titleParam: "Contact failure",
		urlParams: ["titleParam"]
	});
});

staticRouter.get("/contact", async (ctx) => {

	const { formData, formErrors } = ctx.session;

	await ctx.render("staticpages/contact", {
		seoDescription: "Jaywalking contact details",
		pageTitle: "Contact me",
		titleParam: "Contact",
		urlParams: ["titleParam"],
		formData: formData ? formData : null,
		formErrors: formErrors ? formErrors : null
	});
});

export {
	staticRouter
};
