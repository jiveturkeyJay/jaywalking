import { adminRouter } from "./adminRouter.mjs";
import { imagesRouter } from "./imagesRouter.mjs";
import { staticRouter } from "./staticRouter.mjs";
import { router } from './router.mjs';

export { adminRouter, imagesRouter, staticRouter, router };
