import koaRouter from "koa-router";
import {
	createSizedImages,
	getBaseImagesToTransform,
	displayCreatedImages,
	getJsonFiles,
	removeUploadedImages,
	createJsonFile,
	uploadSelectedImages
} from "../middleware/index.mjs";


// todo add some validation
const imagesRouter = new koaRouter({
	prefix: "/images"
});

/**
 * ####### images start #######
 */
imagesRouter.get("/", async (ctx) => {
	await ctx.render("images/index", {
		seoDescription: "Some seo",
		pageTitle: "Images start",
		titleParam: "Images",
		urlParams: ["titleParam"],
		urlArr: ["images"]
	});
});

/**
 * Images start::
 * puts in [ '20220528_105532.jpg', '20220528_114538.jpg' ] as something
 */
imagesRouter.get("/selectImages", removeUploadedImages, async (ctx) => {
	await ctx.render("images/selectImages", {
		seoDescription: "Some se0",
		pageTitle: "Select images",
		titleParam: 'Images',
		urlParams: ["titleParam", "pageTitle"],
		urlArr: ["images", "pageTitle"]
	});
});

/**
 * moves images to the folder
 */
imagesRouter.post("/selectImages", uploadSelectedImages, async (ctx) => {
	// can probably move the redirect or need to check if errs here
	await ctx.redirect(`./preppedImages`);
});

/**
 * allows us to view the images we want to upload
 * and create versions of
 * also then add captions, alt text, new names
 */
imagesRouter.get("/preppedImages", getBaseImagesToTransform, (ctx) => {
	const { copiedImages } = ctx.state;

	return ctx.render("images/prepped", {
		seoDescription: "Some se0",
		pageTitle: "Prepped images",
		titleParam: 'Images',
		verseTitle: 'Prepped images',
		urlParams: ["titleParam", "pageTitle"],
		urlArr: ["images", "pageTitle"],
		copiedImages
	});
});

/**
 * post once we have selected images to chef and added in their data
 * we process them and create a json file which is put into the folder
 * "jsonItems". The images are moved to the folder "public/images"
 */
imagesRouter.post("/chefImages", createSizedImages, createJsonFile, removeUploadedImages, async (ctx) => {
	if (!ctx.state.createdImagesError) {
		await ctx.redirect(`/images/uploaded?prefix=${ctx.state.prefix}`);
	}
});

/**
 * displays images that have been created and
 * also the json file associated with those images
 * allows click and copy
 */
imagesRouter.get("/uploaded", displayCreatedImages, getJsonFiles, async (ctx) => {
	const {
		successData,
		errorData,
		jsonData
	} = ctx.state;

	await ctx.render("images/uploaded", {
		successData,
		jsonData,
		errorData,
		seoDescription: "Some se0",
		pageTitle: "Created images",
		titleParam: "Images",
		urlParams: ["titleParam", "pageTitle"],
		urlArr: ["images", "pageTitle"],
		scripts: ["json"]
	});
});

export {
	imagesRouter
};
