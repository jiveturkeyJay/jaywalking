import koaRouter from "koa-router";
import {
	getAllWalks,
	getArticle,
	getArticleCulmativeDistances,
	getHomePageContent,
	getSubTypeHomePage
} from "../middleware/database/query.mjs";
import ejsHelpers from "../views/helpers/index.mjs";
import sgMail from "@sendgrid/mail";
import { expectedAdditional, expectedAdditionalKeys } from "../constants/index.mjs";
import * as additional from "../constants/data/additional/additional.mjs";
import {
	hasExternalLinks,
	contactFormErrors,
	checkMessage,
	checkEmails,
	checkMessageBits,
	checkNames
} from "../utils/index.mjs";

const router = new koaRouter();
sgMail.setApiKey(process.env.SENDGRID_API_KEY);
router.get("/", getHomePageContent, (ctx) => {
	return ctx.render("staticpages/home", {
		seoDescription: "Jay Walking Home page",
		pageTitle: "Home",
		titleParam: "Home",
		homePageContent: ctx.state.homePageContent,
		homePageLatest: ctx.state.homePageLatest,
		helpers: ejsHelpers,
		cssOptions: {
			container: 'container__fill-width'
		}
	});
});
// an generic page displayed when we cant get data
router.get("/notfound", async (ctx) => {
	await ctx.render("errPage", {
		pageTitle: "An error has occurred",
		titleParam: "Error",
		urlParams: ["titleParam"],
		seoDescription: "Jaywalking error page"
	});
});
router.get("/302", async (ctx) => {
	await ctx.render("errPage", {
		pageTitle: "302 An error has occurred",
		titleParam: "302 Error",
		seoDescription: "Jaywalking error page"
	});
});
router.get("/500", async (ctx) => {
	await ctx.render("errPage", {
		pageTitle: "500 An error has occurred",
		titleParam: "500 Error",
		seoDescription: "Jaywalking error page"
	});
});
router.post("/sendmessage", async (ctx) => {
	Reflect.deleteProperty(ctx.session, 'formData');
	Reflect.deleteProperty(ctx.session, 'formErrors');

	const formData = ctx.request.body;
	const hasUrlData = formData.url && formData.url.length > 0

	if (hasUrlData) {
		return await ctx.redirect('./contactsuccess');
	}

	const formErrors = contactFormErrors(formData);
	if (formErrors.length || !formData || formData.emailaddress === undefined) {
		// remove so we don't expose it
		Reflect.deleteProperty(formData, '_csrf');
		ctx.session.formErrors = formErrors;
		ctx.session.formData = formData;

		return await ctx.redirect('/contact');
	} else {

		const { yourname, yourmessage, emailaddress } = formData;
		const hasUnallowedEmail = checkEmails(emailaddress);
		const hasUnallowedWords = checkMessage(yourmessage);
		const hasUnallowedBits = checkMessageBits(yourmessage);
		const hasUnallowedNames = checkNames(yourname);

		// we are fed up with spam emails from SEO companies etc
		// so have some simple checks and let them think its worked
		if (hasUnallowedEmail || hasUnallowedWords || hasUnallowedBits || hasUnallowedNames) {
			return await ctx.redirect('./contactfail');
		}

		const msg = {
			to: "jasoncowlam@gmail.com", // Change to your recipient
			from: "jasoncowlam@gmail.com", // Change to your verified sender
			subject: `Message from Jaywalking:: ${yourname}`,
			text: `Name:: ${yourname} :::  Message:: ${yourmessage}  :::  Email: ${emailaddress}`, // plain text body
			html: `<p>Name ${yourname}</p><p>Message:: ${yourmessage}</p><p>Email: ${emailaddress}</p>` // html body
		};

		try {
			await sgMail.send(msg);
			await ctx.redirect('./contactsuccess');
		} catch (err) {
			await ctx.redirect('./contactfail');
		}
	}
});

/**
 * article page
 */
router.get("/walks/:sub_type_param/:articleParam", getArticle, getArticleCulmativeDistances, (ctx) => {
	const {
		articleData,
		prevAndNext
	} = ctx.state;
	const jsonData = articleData.articlesJson;
	const data = { ...jsonData };

	data.helpers = ejsHelpers;
	data.expectedAdditional = expectedAdditional;
	data.expectedAdditionalKeys = expectedAdditionalKeys;
	data.urlParams = ["titleParam", "pageTitle", "verseTitle"];
	data.prevAndNext = prevAndNext;
	data.scripts = ["articles"];
	data.cssOptions = {
		container: 'container__fill-width'
	}

	data.hasExternalLinks = hasExternalLinks(data.bodyData);
	return ctx.render("article", data);
});

// back to article link
router.get("/walks/:sub_type_param/:articleParam/gallery", getArticle, (ctx) => {
	const {
		articleData,
		prevAndNext
	} = ctx.state;
	const jsonData = articleData.articlesJson;
	const data = { ...jsonData };

	data.helpers = ejsHelpers;
	data.expectedAdditional = expectedAdditional;
	data.expectedAdditionalKeys = expectedAdditionalKeys;
	data.urlParams = ["titleParam", "pageTitle", "verseTitle", "gallery"];
	data.gallery = "gallery";
	data.prevAndNext = prevAndNext;
	data.scripts = ["articles"];

	data.cssOptions = {
		container: 'container__fill-width'
	}

	return ctx.render("gallery", data);
});
/**
 * home page for a walk or an item equipment
 * eg /walks/northdownsway
 */
router.get("/walks/:sub_type_param", getSubTypeHomePage, (ctx) => {
	const {
		query,
		articleData
	} = ctx.state;

	let data = {};

	if (query && query[0]) {
		const jsonData = query[0].jsonData;
		data = { ...jsonData };
		data.additional = jsonData.additional ? jsonData.additional : [];
	}

	let hasExternals = hasExternalLinks(data.introPara);
	if (!hasExternals) {
		hasExternals = hasExternalLinks(data.additional, 'values', 'href');
	}

	data.expectedAdditional = null;
	data.expectedAdditionalKeys = null;
	data.helpers = ejsHelpers;
	data.urlParams = ["titleParam", "chapterTitle"];
	data.section = articleData;
	data.nonsection = null;
	data.hasExternalLinks = hasExternals;
	// if no data then throw a wobbly
	return ctx.render("homepages", data);
});

/**
 * /walks
 */
router.get("/walks", getAllWalks, (ctx) => {
	const rows = ctx.state.query;
	const {
		pageTitle,
		introPara
	} = additional["walks"];
	const sortedByData = {
		dateAsc: 'Date walked (asc)',
		dateDesc: 'Date walked (desc)',
		titleAsc: 'Title asc',
		titleDesc: 'Title desc'
	};

	const { sortby } = ctx.query;
	return ctx.render("type_homepage", {
		seoDescription: "Jay Walking Home page",
		pageTitle,
		titleParam: "Walks",
		rows,
		sortedByData,
		sortbyValue: sortby ? sortby : '',
		helpers: ejsHelpers,
		introPara,
		sub_type: "walks",
		urlParams: ['titleParam'],
		cssOptions: {
			container: 'container__fill-width'
		}
	});
});


export { router };
