import { defineConfig } from 'cypress';

export default defineConfig({
  e2e: {
    watchForFileChanges: false,
    trashAssetsBeforeRuns: true,
    failOnStatusCode: false,
    video: false,
    baseUrl: 'http://localhost:3030'
  }
});
