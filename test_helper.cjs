const chai = require('chai');
const chaiPromise = require('chai-as-promised');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');
const jsdom = require('jsdom');
const {JSDOM} = jsdom;

//https://github.com/yahoo/react-intl/wiki/Testing-with-React-Intl#dom-rendering
// get the window object out of the document
const dom = new JSDOM('<!doctype html><html><body><p>dfjdsfkdsfds</p></body></html>');
let win = dom.defaultView;
// set globals for mocha that make access to document and window feel
// natural in the e2e environment
global.window = dom.window;
global.document = dom.window.document;

let assert = chai.assert;
let expect = chai.expect;
chai.should();
chai.use(sinonChai);
chai.use(chaiPromise);


global.expect = expect;
global.sinon = sinon;

// from mocha-jsdom https://github.com/rstacruz/mocha-jsdom/blob/master/index.js#L80
function propagateToGlobal(window) {
    for (let key in window) {
        if (!window.hasOwnProperty(key)) {
            continue;
        }
        if (key in global) {
            continue;
        }

        global[key] = window[key];
    }
}

// take all properties of the window object and also attach it to the
// mocha global object
propagateToGlobal(win);

//The original function can be restored by calling object.method.restore(); (or stub.restore();).
//Resets the history of the stub, like calling spy.reset();

const restoreStubOrSpy = stub => stub.restore();
const resetStubOrSpy = spy => spy.resetHistory();

module.exports = {
    chai,
    chaiPromise,
    sinon,
    sinonChai,
    assert,
    expect,
    restoreStubOrSpy,
    resetStubOrSpy
};
